﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_2_CSP
{
    class Person
    {
        public enum Professions { Boss, Secretary, ItSupport, Worker}

        public string Id { get; set; }
        public Professions Profession { get; set; }
        public bool GarlicLover { get; set; }
        public int MaxVisitors { get; set; }
        public bool Assigned { get; set; }
        public List<Room> PossibleRooms { get; set; }

        public Person()
        {
            PossibleRooms = new List<Room>();
        }

        public Person(string id, Professions profession, bool garlicLover, int maxVisitors) : this()
        {
            Id = id;
            Profession = profession;
            GarlicLover = garlicLover;
            MaxVisitors = maxVisitors;
        }
    }
}
