﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_2_CSP
{
    class Room
    {
        public string Id { get; set; }
        public int WorkPlaces { get; set; }
        public int VisitorPlaces { get; set; }
        public List<Room> Near { get; set; }
        public List<Person> Occupants { get; set; }
        public Room()
        {
            Near = new List<Room>();
            Occupants = new List<Person>();
        }

        public Room(string id, int workPlaces, int visitorPlaces) : this()
        {
            Id = id;
            WorkPlaces = workPlaces;
            VisitorPlaces = visitorPlaces;
        }
    }
}
