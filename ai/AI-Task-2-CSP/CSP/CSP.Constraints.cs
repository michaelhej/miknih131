﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_2_CSP
{
    partial class CSP
    {
        /// <summary>
        /// Check all contraints on given person in given room
        /// </summary>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool CheckContraints(Person person, Room room)
        {
            _tests++;

            return
                CheckBossAlone(person, room) &&
                CheckNearBoss(person, room) &&
                CheckGarlicLovers(person, room) &&
                CheckWorkPlaces(person, room) &&
                CheckVisitorsFit(person, room);

        }

        /// <summary>
        /// Boss needs an own office
        /// </summary>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool CheckBossAlone(Person person, Room room)
        {
            if (person.Profession == Person.Professions.Boss)
            {
                if (room.Occupants.Count > 1)
                    return false;
            }
            else
            {
                foreach (Person p in room.Occupants)
                {
                    if (p.Profession == Person.Professions.Boss)
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Return wether person has specified profession, thus needing an office next to a boss
        /// </summary>
        /// <param name="profession"></param>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool CheckNearBoss(Person person, Room room)
        {
            if (person.Profession != Person.Professions.ItSupport && person.Profession != Person.Professions.Secretary)
                return true;

            foreach (Room r in room.Near)
            {
                foreach (Person p in r.Occupants)
                {
                    if (p.Profession == Person.Professions.Boss)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Garlic lovers bunk together
        /// </summary>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool CheckGarlicLovers(Person person, Room room)
        {
            foreach (Person p in room.Occupants)
            {
                if (person.GarlicLover != p.GarlicLover)
                    return false;
            }
            return true;
        }


        private bool CheckWorkPlaces(Person person, Room room)
        {
            return room.WorkPlaces >= room.Occupants.Count;
        }

        /// <summary>
        /// All persons in a room should have room for all their visitors
        /// </summary>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool CheckVisitorsFit(Person person, Room room)
        {
            int requiredVisitorPlaces = 0;//person.MaxVisitors;
            foreach (Person p in room.Occupants)
            {
                requiredVisitorPlaces += p.MaxVisitors;
            }
            return room.VisitorPlaces >= requiredVisitorPlaces;
        }
    }
}
