﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_2_CSP
{
    partial class CSP
    {
        private enum LogType
        {
            Assigning,
            Fail,
            Backtrack,
            PossibleRooms,
            TestAssignment
        };

        void Log(Person person, Room room, LogType logType, int data = 0)
        {
            switch (logType)
            {
                case LogType.Assigning:
                    Console.WriteLine("Testing " + person.Id + " (" + person.Profession + ") in " + room.Id);
                    break;
                case LogType.Fail:
                    Console.WriteLine("! Assignment failed: " + person.Id + " (" + person.Profession + ") in " + room.Id);
                    break;
                case LogType.Backtrack:
                    Console.WriteLine("<- Backtracking. Removing " + person.Id + " (" + person.Profession + ") from " + room.Id);
                    break;
                case LogType.PossibleRooms:
                    Console.WriteLine(person.Id + " (" + person.Profession + ") has " + person.PossibleRooms.Count + " possible rooms");
                    break;
                case LogType.TestAssignment:
                    Console.WriteLine(person.Id + " (" + person.Profession + ") has " + data + " possible assignments after assignment to room " + room.Id);
                    break;
            }
        }

        private void PrintAllocation()
        {
            Console.WriteLine("### ROOM ASSIGNMENTS ###");
            foreach (Room r in Rooms)
            {
                Console.Write("In room " + r.Id);

                Console.Write(", Workplaces: " + r.WorkPlaces);
                Console.Write(", MaxVisitors: " + r.VisitorPlaces);

                Console.Write(" (Near: ");
                foreach (Room near in r.Near)
                {
                    Console.Write(near.Id);
                    if (near != r.Near.Last())
                        Console.Write(", ");
                }
                Console.WriteLine(")");

                foreach (Person p in r.Occupants)
                {
                    Console.WriteLine("\t" + p.Id + "\t" + p.Profession + "\t" + p.MaxVisitors + " visitors");
                }
                Console.WriteLine("-------");
            }
            Console.WriteLine("Total tests: " + _tests);
            Console.WriteLine("Backtracking steps: " + _backtracks);
        }
    }
}
