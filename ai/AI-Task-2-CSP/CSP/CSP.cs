﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_2_CSP
{
    partial class CSP
    {
        public enum SearchType
        {
            Simple,
            Heuristic
        };

        public List<Person> Persons { get; set; }
        public List<Room> Rooms { get; set; }
        
        private int _tests;
        private int _backtracks;

        public CSP()
        {
            Persons = new List<Person>();
            Rooms = new List<Room>();
        }

        public void SetNearRooms(string roomId, string[] nearRooms)
        {
            Room room = Rooms.Find(r => r.Id == roomId);
            for (int i = 0; i < nearRooms.Length; i++)
            {
                Room nearRoom = Rooms.Find(r => r.Id == nearRooms[i]);
                if (!room.Near.Contains(nearRoom))
                    room.Near.Add(nearRoom);
                if (!nearRoom.Near.Contains(room))
                    nearRoom.Near.Add(room);
            }
        }

        public void Allocate(SearchType searchType)
        {
            ClearAssignments();

            switch (searchType)
            {
                case SearchType.Simple:
                    Allocate();
                    break;
                case SearchType.Heuristic:
                    AllocateHeuristic();
                    break;
            }
            
            PrintAllocation();
        }

        private void ClearAssignments()
        {
            _tests = _backtracks = 0;
            foreach (Room room in Rooms)
            {
                room.Occupants.Clear();
            }
            foreach (Person person in Persons)
            {
                person.Assigned = false;
            }
        }

        /// <summary>
        /// Shuffles list, to avoid getting same results every time.
        /// Also, make sure to but a boss on top, otherwise it won't work
        /// </summary>
        public void Shuffle()
        {
            Random rand = new Random();
            int n = Persons.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                Person tmp = Persons[k];
                Persons[k] = Persons[n];
                Persons[n] = tmp;
            }

            for (int i = 0; i < Persons.Count; i++)
            {
                if (Persons[i].Profession == Person.Professions.Boss)
                {
                    Person tmp = Persons[i];
                    Persons[i] = Persons[0];
                    Persons[0] = tmp;
                    break;
                }
            }

            Console.WriteLine("### List shuffled ###");
            foreach (Person p in Persons)
            {
                Console.WriteLine(p.Id + ", " + p.Profession);
            }
            Console.WriteLine("-----");
        }

        #region SIMPLE SEARCH
        /// <summary>
        /// Use a simple trial and error function to place persons in rooms
        /// </summary>
        /// <returns></returns>
        private bool Allocate()
        {
            Person person = SelectNextUnassignedPerson();
            if (person == null)
                return true;

            foreach (Room room in Rooms)
            {
                Log(person, room, LogType.TestAssignment);

                room.Occupants.Add(person);
                person.Assigned = true;

                if (!CheckContraints(person, room))
                {
                    Log(person, room, LogType.Fail);
                    room.Occupants.Remove(person);
                    person.Assigned = false;
                }
                else if (Allocate())
                {
                    return true;
                }
                else
                {
                    Log(person, room, LogType.Backtrack);
                    room.Occupants.Remove(person);
                    person.Assigned = false;
                    _backtracks++;
                }
            }
            return false;
        }

        private Person SelectNextUnassignedPerson()
        {
            foreach (Person p in Persons)
            {
                if (!p.Assigned)
                {
                    return p;
                }
            }
            return null;
        }
        #endregion

        #region HEURISTIC SEARCH
        /// <summary>
        /// Use heuristic functions to allocate persons in room
        /// </summary>
        /// <returns></returns>
        private bool AllocateHeuristic()
        {
            UpdatePossibleRooms();
            Person person = SelectPersonWithLowestRooms();
            if (person == null)
                return true;

            foreach (Room room in RoomAllocationList(person))
            {
                Log(person, room, LogType.TestAssignment);

                room.Occupants.Add(person);
                person.Assigned = true;

                if (!CheckContraints(person, room))
                {
                    Log(person, room, LogType.Fail);
                    room.Occupants.Remove(person);
                    person.Assigned = false;
                }
                else if (AllocateHeuristic())
                {
                    return true;
                }
                else
                {
                    Log(person, room, LogType.Backtrack);
                    room.Occupants.Remove(person);
                    person.Assigned = false;
                    _backtracks++;
                }
            }
            return false;
        }

        /// <summary>
        /// Select the person with the lowest number of possible rooms lest
        /// </summary>
        /// <returns></returns>
        private Person SelectPersonWithLowestRooms()
        {
            Person person = null;
            int lowestRooms = int.MaxValue;
            foreach (Person p in Persons)
            {
                int count = p.PossibleRooms.Count;
                if (!p.Assigned)
                {
                    Log(p, null, LogType.PossibleRooms);
                    if (count < lowestRooms)
                    { 
                        person = p;
                        lowestRooms = count;
                    }
                }
                
            }
            //Console.WriteLine("Returning " + person.Profession + " (lowest) ");
            return person;
        }

        /// <summary>
        /// Returns a list of rooms sorted by least contraining
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        private List<Room> RoomAllocationList(Person person)
        {
            List<Tuple<Room, int>> possibleRooms = new List<Tuple<Room, int>>();
            foreach (Room room in person.PossibleRooms)
            {
                //Test assigning person to room and calculate the impact of the assignment
                room.Occupants.Add(person);
                person.Assigned = true;
                UpdatePossibleRooms();

                int possibleAfterAssignment = 0;
                foreach (Person p in Persons)
                {
                    if (!p.Assigned)
                    {
                        //Too contraining, not valid assignment
                        if (p.PossibleRooms.Count == 0)
                        {
                            possibleAfterAssignment = -1;
                            break;
                        }
                        possibleAfterAssignment += p.PossibleRooms.Count;
                    }
                }
                Log(person, room, LogType.TestAssignment, possibleAfterAssignment);

                //If room is valid assignment
                if (possibleAfterAssignment >= 0)
                {
                    possibleRooms.Add(Tuple.Create(room, possibleAfterAssignment));
                }

                //Remove test-assignment
                room.Occupants.Remove(person);
                person.Assigned = false;
            }

            //Return room-list sorted by contrainability
            possibleRooms.OrderBy(r => r.Item2);

            return possibleRooms.Select(r => r.Item1).ToList();
        }
        #endregion

        #region POSSIBLE ROOMS
        /// <summary>
        /// Update the set of possible rooms for all unassigned persons
        /// </summary>
        private void UpdatePossibleRooms()
        {
            foreach (Person person in Persons)
            {
                if (!person.Assigned)
                {
                    person.PossibleRooms.Clear();

                    switch (person.Profession)
                    {
                        case Person.Professions.Boss:
                            BossPossibleRooms(person);
                            break;

                        case Person.Professions.ItSupport:
                        case Person.Professions.Secretary:
                            NearBossPossibleRooms(person);
                            break;

                        case Person.Professions.Worker:
                            WorkerPossibleRooms(person);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Bosses need an own room big enought for all their visitors
        /// </summary>
        /// <param name="person"></param>
        private void BossPossibleRooms(Person person)
        {
            foreach (Room room in Rooms)
            {
                if (room.Occupants.Count == 0 && room.VisitorPlaces >= person.MaxVisitors)
                    person.PossibleRooms.Add(room);
            }
        }

        /// <summary>
        /// Check which rooms are near bosses and, if basic constraints are satisfied, add to list of possible rooms
        /// </summary>
        /// <param name="person"></param>
        private void NearBossPossibleRooms(Person person)
        {
            //First check if all bosses are assigned
            //If not, possible rooms are based on same contraints as workders
            foreach (Person p in Persons)
            {
                if (!p.Assigned && p.Profession == Person.Professions.Boss)
                {
                    WorkerPossibleRooms(person);
                    return;
                }
            }

            //If all bosses are assigned rooms, check all rooms near bosses
            foreach (Room room in Rooms)
            {
                //If there is a boss-person in this room, ass near rooms to possible rooms
                if (room.Occupants.Count == 1 && room.Occupants[0].Profession == Person.Professions.Boss)
                {
                    foreach (Room near in room.Near)
                    {
                        if (AllWorkersContraints(person, near))
                            person.PossibleRooms.Add(near);
                    }
                }
            }
        }

        private void WorkerPossibleRooms(Person person)
        {
            foreach (Room room in Rooms)
            {
                if (AllWorkersContraints(person, room))
                    person.PossibleRooms.Add(room);
            }
        }

        /// <summary>
        /// Evaluate basic worker contrains for room
        /// </summary>
        /// <param name="person"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool AllWorkersContraints(Person person, Room room)
        {
            int visitorPlacesTaken = 0;
            foreach (Person p in room.Occupants)
            {
                if (p.GarlicLover != person.GarlicLover)
                    return false;

                if (p.Profession == Person.Professions.Boss)
                    return false;

                visitorPlacesTaken += p.MaxVisitors;
            }

            if (room.WorkPlaces > room.Occupants.Count &&
                room.VisitorPlaces >= visitorPlacesTaken + person.MaxVisitors)
                return true;

            return false;
        }

        #endregion
    }
}
