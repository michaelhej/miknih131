﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AI_Task_2_CSP
{
    public partial class Form1 : Form
    {
        private CSP _csp;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _csp = new CSP();
            InitData();
        }


        private void InitData()
        {
            //Add persons
            _csp.Persons.Add(new Person("A", Person.Professions.Boss, true, 5));
            _csp.Persons.Add(new Person("B", Person.Professions.Secretary, false, 1));
            _csp.Persons.Add(new Person("C", Person.Professions.ItSupport, false, 2));
            _csp.Persons.Add(new Person("D", Person.Professions.Boss, true, 5));
            _csp.Persons.Add(new Person("E", Person.Professions.Secretary, false, 1));
            _csp.Persons.Add(new Person("F", Person.Professions.Worker, false, 3));
            _csp.Persons.Add(new Person("G", Person.Professions.Worker, false, 2));
            _csp.Persons.Add(new Person("H", Person.Professions.Worker, true, 0));
            _csp.Persons.Add(new Person("I", Person.Professions.Worker, true, 1));


            //Add rooms
            _csp.Rooms.Add(new Room("T1", 2, 5));
            _csp.Rooms.Add(new Room("T2", 3, 2));
            _csp.Rooms.Add(new Room("T3", 1, 3));
            _csp.Rooms.Add(new Room("T4", 1, 3));
            _csp.Rooms.Add(new Room("T5", 2, 5));
            _csp.Rooms.Add(new Room("T6", 2, 5));
            _csp.Rooms.Add(new Room("T7", 2, 5));

            //Near rooms
            _csp.SetNearRooms("T1", new[] { "T2", "T7" });
            _csp.SetNearRooms("T2", new[] { "T1", "T3", "T6" });
            _csp.SetNearRooms("T3", new[] { "T2", "T4", "T5" });
            _csp.SetNearRooms("T4", new[] { "T3", "T5" });
            _csp.SetNearRooms("T5", new[] { "T6", "T4", "T3" });
            _csp.SetNearRooms("T6", new[] { "T5", "T7", "T2" });
            _csp.SetNearRooms("T7", new[] { "T6", "T1" });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _csp.Allocate(CSP.SearchType.Simple);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _csp.Allocate(CSP.SearchType.Heuristic);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _csp.Shuffle();
        }
    }
}
