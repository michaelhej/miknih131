﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_3_DTL
{
    public class DecisionFork
    {
        public List<Car> Cars { get; set; }
        public Dictionary<Feature, DecisionFork> Branches { get; set; }
        public bool IsLeaf { get; set; }

        public DecisionFork()
        {
            Branches = new Dictionary<Feature, DecisionFork>();
        }
        public string Output()
        {
            if (Cars.Count == 0)
                return "";
            else
            {
                int distinctOutputs = Cars.Select(c => c.Output).Distinct().Count();
                if (distinctOutputs == 1)
                    return Cars.First().Output;

                return distinctOutputs + " outputs";
            }
        }
        
        /// <summary>
        /// Traverse down the tree to return an output decided by the features on the given car
        /// </summary>
        /// <param name="car"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public string Decision(Car car, int depth = 0)
        {
            for (int i = 0; i < car.Features.Count; i++)
            {
                if (Branches.ContainsKey(car.Features[i]))
                {
                    Log.Print(car.Features[i], Log.LogType.Decision, depth);
                    DecisionFork matchingBranch = Branches[car.Features[i]];
                    car.Features.RemoveAt(i);
                    return matchingBranch.Decision(car, depth + 1);
                }
            }
            return Output();
        }

        /// <summary>
        /// Branch this tree on best feature and branch branches
        /// </summary>
        public void BranchBest(string forcedFeature = "")
        {
            string splitFeature = BestSplitFeature();

            if (forcedFeature != "")
                splitFeature = forcedFeature;

            //If no splitting feature, make leaf
            if (splitFeature == "")
            {
                IsLeaf = true;
                return;
            }
            BranchFeatureName(splitFeature);

            foreach (DecisionFork branch in Branches.Values)
            {
                branch.BranchBest();
            }
        }

        /// <summary>
        /// Select the best (highest information gain) feature for splitting
        /// </summary>
        /// <returns></returns>
        public string BestSplitFeature()
        {
            double entropy = Entropy();

            double bestGain = 0;
            string bestFeatureData = "";

            Log.Print("Calculating best split feature", Log.LogType.Entropy);

            foreach (string featureName in UniqueFeatures())
            {
                double gain = entropy - EntropyForBranch(featureName);
                Log.Print(featureName, Log.LogType.Entropy, 0, entropy, gain);
                if (gain > bestGain)
                {
                    bestGain = gain;
                    bestFeatureData = featureName;
                }
            }

            return bestFeatureData;
        }

        /// <summary>
        /// Returns expected entropy for split on given feature
        /// </summary>
        /// <param name="featureName"></param>
        /// <returns></returns>
        public double EntropyForBranch(string featureName)
        {
            double branchEntropy = 0;
            foreach (Feature feature in UniqueFeatureValues(featureName))
            {
                DecisionFork branch = BranchFeatureValue(feature);
                double proportion = ((double)branch.Cars.Count / Cars.Count);
                branchEntropy += branch.Entropy() * proportion;
            }
            return branchEntropy;
        }

        /// <summary>
        /// Gets the entropy for this DecisionFork
        /// </summary>
        /// <returns></returns>
        public double Entropy()
        {
            double entropy = 0;

            //Iterate through distinct outputs
            foreach (string output in Cars.Select(c => c.Output).Distinct())
            {
                double outputCount = Cars.Count(c => c.Output == output);
                double ratio = outputCount / Cars.Count;
                entropy += ratio * Math.Log(ratio, 2);
            }

            return -entropy;
        }

        /// <summary>
        /// Branch this decision tree on featureName
        /// </summary>
        public void BranchFeatureName(string featureName)
        {
            Console.Write("\nBranching on {0}: ", featureName.PadRight(10));

            foreach (Feature feature in UniqueFeatureValues(featureName))
            {
                DecisionFork forkOfThis = BranchFeatureValue(feature);
                Console.Write("{0}", feature.Value.PadLeft(10));
                Branches.Add(feature, forkOfThis);
            }
        }

        /// <summary>
        /// Branch this decision tree on given feature
        /// </summary>
        /// <param name="splitFeature"></param>
        /// <returns></returns>
        public DecisionFork BranchFeatureValue(Feature splitFeature)
        {
            DecisionFork fork = new DecisionFork();
            fork.Cars = new List<Car>();

            foreach (Car car in Cars)
            {
                Car splittedCar = car.Split(splitFeature);
                if (splittedCar != null)
                    fork.Cars.Add(splittedCar);
            }
            return fork;
        }


        /// <summary>
        /// Returns a list of unique features of the cars in this DecisionFork
        /// </summary>
        /// <returns></returns>
        public List<string> UniqueFeatures()
        {
            IEnumerable<Feature> allFeatures = Cars.SelectMany(c => c.Features);
            IEnumerable<string> uniqueFeatures = allFeatures.Select(a => a.Name).Distinct();
            return uniqueFeatures.ToList();
        }

        /// <summary>
        /// Gets a list of unique feature values for given feature
        /// </summary>
        /// <param name="featureName"></param>
        /// <returns></returns>
        public List<Feature> UniqueFeatureValues(string featureName)
        {
            List<Feature> uniqueValues = new List<Feature>();

            //Select all features from all cars
            foreach (Feature carFeature in Cars.SelectMany(c => c.Features).Distinct())
            {
                if (carFeature.Name == featureName)
                {
                    if (uniqueValues.All(u => u.Value != carFeature.Value))
                    {
                        uniqueValues.Add(carFeature);
                    }
                }
            }

            return uniqueValues;
        }

        /// <summary>
        /// Prints out all branches
        /// </summary>
        /// <param name="depth"></param>
        public void Print(int depth = 0)
        {
            Log.Print("Cars (features) in data set: " + Cars.Count + " (" + Cars.SelectMany(c => c.Features).Count() + ")", depth);

            foreach (Feature feature in Branches.Keys)
            {
                Log.Print(feature, Log.LogType.PrintTree, depth);
                Branches[feature].Print(depth + 1);
            }
        }
    }
}
