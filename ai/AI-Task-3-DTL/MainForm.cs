﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AI_Task_3_DTL
{
    public partial class MainForm : Form
    {
        private DTL _dtl;

        public MainForm()
        {
            InitializeComponent();

            _dtl = new DTL();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            _dtl.ReadFile(@"..\..\car-small.data");
            _dtl.MainFork.BranchBest();
            TestDecision();
        }

        /// <summary>
        /// Test the output from the decision tree for a car with some attributes
        /// </summary>
        private void TestDecision()
        {
            List<Feature> attributes = new List<Feature>();
            attributes.Add(new Feature("buying", "low"));
            attributes.Add(new Feature("maint", "low"));
            attributes.Add(new Feature("doors", "4"));
            attributes.Add(new Feature("persons", "4"));
            attributes.Add(new Feature("lug_boot", "med"));
            attributes.Add(new Feature("safety", "high"));

            Car car = new Car(attributes, "");
            Console.WriteLine("Testing decision");
            string decision = _dtl.MainFork.Decision(car);

            Console.WriteLine("Output: {0}", decision);
        }



        private void btnOpenTreeView_Click(object sender, EventArgs e)
        {
            TreeViewForm treeView = new TreeViewForm();
            treeView.Show();
            treeView.PopulateTreeView(_dtl.MainFork);
        }

        private void btnDrawTree_Click(object sender, EventArgs e)
        {
            TreeDrawer treeDrawer = new TreeDrawer();
            treeDrawer.Show();
            treeDrawer.DrawTree(_dtl.MainFork);
        }
    }
}
