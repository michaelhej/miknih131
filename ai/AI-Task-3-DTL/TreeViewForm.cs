﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AI_Task_3_DTL
{
    public partial class TreeViewForm : Form
    {
        public TreeViewForm()
        {
            InitializeComponent();
        }

        private void TreeViewForm_Load(object sender, EventArgs e)
        {

        }

        public void PopulateTreeView(DecisionFork fork)
        {
            if (fork.Branches.Count == 0)
                return;

            AddNode(fork, null, null);
        }

        private void AddNode(DecisionFork fork, Feature splitAttribute, TreeNode parent)
        {
            TreeNode node = new TreeNode();
            string branchName = "root";
            if (splitAttribute != null)
                branchName = splitAttribute.Name + " = " + splitAttribute.Value;

            string info = branchName.PadRight(25);
            info += ("Entropy " + Math.Round(fork.Entropy(), 2)).PadRight(25);
            info += (fork.Cars.Count + " cars").PadRight(25);
            info += (fork.Cars.SelectMany(c => c.Features).Count() + " attr").PadRight(25);
            info += ("Output: " + fork.Output()).PadRight(25);

            node.Text = info;

            if (parent == null)
                treeView.Nodes.Add(node);
            else
                parent.Nodes.Add(node);

            node.Tag = fork;
            if (fork.IsLeaf)
                node.BackColor = Color.GreenYellow;

            foreach (KeyValuePair<Feature, DecisionFork> branch in fork.Branches)
            {
                AddNode(branch.Value, branch.Key, node);
            }
        }

        private void ShowCars()
        {
            if (treeView.SelectedNode.Tag == null)
                return;

            DecisionFork fork = (DecisionFork)treeView.SelectedNode.Tag;

            carListView.Items.Clear();
            foreach (Car car in fork.Cars)
            {
                ListViewItem firstItem = new ListViewItem(car.Output);
                foreach (Feature attribute in car.Features)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = attribute.Name + " = " + attribute.Value;
                    firstItem.SubItems.Add(item.Text);
                }
                carListView.Items.Add(firstItem);
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ShowCars();
        }

        private void btnExpandAll_Click(object sender, EventArgs e)
        {
            treeView.ExpandAll();
        }
    }
}
