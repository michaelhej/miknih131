﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_3_DTL
{
    public static class Log
    {
        public enum LogType
        {
            Split,
            PrintTree,
            Entropy,
            InfoGain,
            Decision,
            Branch
        };
        public static void Print(string str, int depth = 0)
        {
            Console.WriteLine("".PadLeft(depth * 5, '-') + str);
        }

        public static void Print(Feature attribute, LogType logType, int depth = 0, double entropy = 0, double gain = 0)
        {
            Print(attribute.Name + " = " + attribute.Value, logType, depth, entropy, gain);
        }
        public static void Print(string str, LogType logType, int depth = 0, double entropy = 0, double gain = 0)
        {
            if (logType == LogType.Branch)
            {
                str = "Branching on " + str;
                Print(str, depth);
            }

            if (logType == LogType.PrintTree)
            {
                str = " # Branch: " + str;
                Print(str, depth);
            }

            if (logType == LogType.Split)
            {
                str = " -> Splitting on " + str;
                Print(str, depth);
            }

            if (logType == LogType.Decision)
            {
                str = " Taking route " + str;
                Print(str, depth);
            }

            if (logType == LogType.Entropy)
            {
                str = str + ", Entropy: " + entropy + ", Gain: " + gain;
                //Print(str, depth);
            }
        }
    }
}
