﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AI_Task_3_DTL;

namespace AI_Task_3_DTL
{
    class DTL
    {
        public DecisionFork MainFork { get; set; }
        /// <summary>
        /// Read learning data set from given file
        /// </summary>
        /// <param name="filename"></param>
        public void ReadFile(string filename)
        {
            DecisionFork tree = new DecisionFork();
            List<Car> cars = new List<Car>();
            string[] headers = new[] {""};

            bool firstLine = true;
            using (StreamReader reader = new StreamReader(filename))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string[] attr = line.Split(',');
                    
                    if (attr.Length < 1)
                        continue;

                    List<Feature> attributes = new List<Feature>();
                    attributes.Add(new Feature("buying", attr[0]));
                    attributes.Add(new Feature("maint", attr[1]));
                    attributes.Add(new Feature("doors", attr[2]));
                    attributes.Add(new Feature("persons", attr[3]));
                    attributes.Add(new Feature("lug_boot", attr[4]));
                    attributes.Add(new Feature("safety", attr[5]));

                    Car car = new Car(attributes, attr[6]);
                    cars.Add(car);
                }
            }

            tree.Cars = cars;
            MainFork = tree;
        }
    }
}
