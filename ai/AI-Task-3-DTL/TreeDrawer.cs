﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AI_Task_3_DTL
{
    public partial class TreeDrawer : Form
    {
        private Dictionary<DecisionFork, Point> _forkLocation;
        private List<Node> _nodes;

        private int maxDepth = 6;
        private int width = 80;
        private int height = 70;
        private int spacingX = 25;
        private int spacingY = 25;

        public TreeDrawer()
        {
            InitializeComponent();

            _forkLocation = new Dictionary<DecisionFork, Point>();
            _nodes = new List<Node>();
        }

        private void TreeDrawer_Paint(object sender, PaintEventArgs e)
        {
            DrawNodes();
        }

        /// <summary>
        /// Draw a tree. Takes the root node as argument
        /// </summary>
        /// <param name="fork"></param>
        public void DrawTree(DecisionFork fork)
        {
            if (fork.Branches.Count == 0)
                return;

            AddFork(null, null, fork, 0, 1);
            DrawNodes();
        }

        /// <summary>
        /// Recursive function traversing the tree adding all branches to the list of nodes
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="splitAttribute"></param>
        /// <param name="fork"></param>
        /// <param name="depth"></param>
        /// <param name="index"></param>
        private void AddFork(Node parent, Feature splitAttribute, DecisionFork fork, int depth, int index)
        {
            if (depth > maxDepth)
                return;

            string branchName = "root";
            if (splitAttribute != null)
                branchName = splitAttribute.Name + " = " + splitAttribute.Value;

            string info = branchName;
            info += "\n";
            info += "Entropy " + Math.Round(fork.Entropy(), 2);
            info += "\n";
            info += fork.Cars.Count + " cars";
            info += "\n";
            info += fork.Cars.SelectMany(c => c.Features).Count() + " attr";
            info += "\n";
            info += "= " + fork.Output();

            Node node = new Node();
            node.Depth = depth;
            node.Index = index;
            node.Fork = fork;
            node.Parent = parent;
            node.Info = info;
            node.AllNodes = _nodes;
            _nodes.Add(node);

            int i = 0;
            foreach (KeyValuePair<Feature, DecisionFork> branch in fork.Branches)
            {
                i++;
                AddFork(node, branch.Key, branch.Value, depth + 1, i);
            }
        }

        /// <summary>
        /// Draw the list of nodes
        /// </summary>
        private void DrawNodes()
        {
            Graphics graphics = CreateGraphics();
            graphics.Clear(Color.FromArgb(64, 64, 64));

            Pen pen = new Pen(Color.AntiqueWhite, 1);

            //Draw lines
            foreach (Node node in _nodes)
            {
                if (node.Parent != null)
                {
                    graphics.DrawLine(pen, NodeLocation(node.Parent, 1), NodeLocation(node, 2));
                }
            }

            foreach (Node node in _nodes)
            {
                PointF location = NodeLocation(node);
                PointF textLocation = NodeLocation(node, 3);
                textLocation.X -= graphics.MeasureString(node.Info, DefaultFont).Width / 2;
                textLocation.Y -= graphics.MeasureString(node.Info, DefaultFont).Height / 2;

                Rectangle rectangle = new Rectangle((int)location.X, (int)location.Y, width, height);

                LinearGradientBrush brush = new LinearGradientBrush(rectangle, Color.Orange, Color.Chocolate, LinearGradientMode.Vertical);
                if (node.Fork.IsLeaf)
                    brush = new LinearGradientBrush(rectangle, Color.GreenYellow, Color.MediumSeaGreen, LinearGradientMode.Vertical);
                Brush textBrush = new SolidBrush(Color.Black);

                graphics.FillRectangle(brush, rectangle);
                graphics.DrawRectangle(pen, rectangle);
                graphics.DrawString(node.Info, Font, textBrush, textLocation);
            }
        }

        /// <summary>
        /// Return a location of given node.
        /// Location X value is the center
        /// Location Y value is either top / mid / bottom
        /// </summary>
        /// <param name="node"></param>
        /// <param name="center">1 = bottom, 2 = top, 3 = middle</param>
        /// <returns></returns>
        private PointF NodeLocation(Node node, int center = 0)
        {
            PointF location = node.Location;
            location.X *= this.Width - width * ((maxDepth + 1) - node.Depth);
            location.X += width * (maxDepth - node.Depth) / 2;
            location.Y *= (Height / (maxDepth + 1));

            if (center == 1)
            {
                location.X += width / 2;
                location.Y += height;
            }
            if (center == 2)
            {
                location.X += width / 2;
            }
            if (center == 3)
            {
                location.X += width / 2;
                location.Y += height / 2;
            }

            return location;
        }
    }
}
