﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_3_DTL
{
    class Node
    {
        public List<Node> AllNodes { get; set; }
        public int Depth { get; set; }
        public int Index { get; set; }
        public DecisionFork Fork { get; set; }
        public Node Parent { get; set; }
        public string Info { get; set; }
        
        public PointF Location
        {
            get
            {
                if (Parent == null)
                {
                    return new PointF(0.5f, 0);
                }

                List<Node> nodesOnRow = AllNodes.Where(n => n.Depth == Depth).ToList();
                float rowCount = nodesOnRow.Count;
                if (rowCount == 1)
                    rowCount = 2;
                float positionOnRow = nodesOnRow.FindIndex(x => x.Equals(this));

                return new PointF(positionOnRow / (rowCount - 1), Depth);
            }
        }
    }
}
