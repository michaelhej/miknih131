﻿namespace AI_Task_3_DTL
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGo = new System.Windows.Forms.Button();
            this.btnOpenTreeView = new System.Windows.Forms.Button();
            this.btnDrawTree = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(22, 24);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(177, 23);
            this.btnGo.TabIndex = 0;
            this.btnGo.Text = "Load data";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // btnOpenTreeView
            // 
            this.btnOpenTreeView.Location = new System.Drawing.Point(22, 129);
            this.btnOpenTreeView.Name = "btnOpenTreeView";
            this.btnOpenTreeView.Size = new System.Drawing.Size(177, 23);
            this.btnOpenTreeView.TabIndex = 1;
            this.btnOpenTreeView.Text = "Open tree view";
            this.btnOpenTreeView.UseVisualStyleBackColor = true;
            this.btnOpenTreeView.Click += new System.EventHandler(this.btnOpenTreeView_Click);
            // 
            // btnDrawTree
            // 
            this.btnDrawTree.Location = new System.Drawing.Point(22, 100);
            this.btnDrawTree.Name = "btnDrawTree";
            this.btnDrawTree.Size = new System.Drawing.Size(177, 23);
            this.btnDrawTree.TabIndex = 2;
            this.btnDrawTree.Text = "Draw tree";
            this.btnDrawTree.UseVisualStyleBackColor = true;
            this.btnDrawTree.Click += new System.EventHandler(this.btnDrawTree_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(214, 173);
            this.Controls.Add(this.btnDrawTree);
            this.Controls.Add(this.btnOpenTreeView);
            this.Controls.Add(this.btnGo);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Button btnOpenTreeView;
        private System.Windows.Forms.Button btnDrawTree;
    }
}

