﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_3_DTL
{
    public class Car
    {
        public List<Feature> Features { get; set; }
        public string Output { get; set; }

        public Car()
        {
            Features = new List<Feature>();
        }

        public Car(List<Feature> attributes, string output)
        {
            Features = attributes;
            Output = output;
        }

        /// <summary>
        /// Split car on specified attribute
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public Car Split(Feature feature)
        {
            List<Feature> splitAttributes = Features.Where(a => a != feature).ToList();

            if (splitAttributes.Count == Features.Count)
                return null;

            return new Car(splitAttributes, Output);
        }
    }
}
