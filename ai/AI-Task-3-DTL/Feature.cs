﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_3_DTL
{
    public class Feature
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Feature(string data, string value)
        {
            Name = data;
            Value = value;
        }

        public static bool operator ==(Feature a, Feature b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return a.Name.Equals(b.Name) && a.Value.Equals(b.Value);
        }
        public static bool operator !=(Feature a, Feature b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            Feature other = obj as Feature;

            if (other == null)
                return false;

            return Name.Equals(other.Name) && Value.Equals(other.Value);
        }

        /// <summary>
        /// Used for branching-dictionary
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            string str = Name + "=" + Value;
            return str.GetHashCode();
        }
    }
}
