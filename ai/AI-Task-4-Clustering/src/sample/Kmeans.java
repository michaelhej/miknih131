package sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by michael on 22/02/16.
 */
public class Kmeans
{
    private ArrayList<Instance> dataSet =  new ArrayList<>();
    public ArrayList<Cluster> clusters =  new ArrayList<>();
    private final int numClusters = 3;
    public Kmeans()
    {

    }

    public void loadData()
    {
        dataSet.clear();
        clusters.clear();
        Cluster.numClusters = 0;
        Instance.attributesMax.clear();
        Instance.attributesDeviation.clear();
        Instance.attributesMean.clear();
        Instance.attributesMin.clear();

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("wine.data"));
            for (String line = reader.readLine(); line != null; line = reader.readLine())
            {
                Instance instance = new Instance();
                for (String attr : line.split(","))
                {
                    instance.attributes.add(Double.parseDouble(attr));
                }
                dataSet.add(instance);
            }
            reader.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void createClusters()
    {
        for (int i = 0; i < numClusters; i++)
            clusters.add(new Cluster(dataSet.get(new Random().nextInt(dataSet.size()))));
    }

    public void analyzeAttributes()
    {
        int attributeCount = dataSet.get(0).attributes.size();
        for (int i = 0; i < attributeCount; i++)
        {
            double minValue = Double.MAX_VALUE;
            double maxValue = Double.MIN_VALUE;
            double sum = 0;
            for (Instance instance : dataSet)
            {
                double value = instance.attributes.get(i);
                sum += value;

                if (value > maxValue)
                    maxValue = value;
                if (value < minValue)
                    minValue = value;
            }
            Instance.attributesMin.add(minValue);
            Instance.attributesMax.add(maxValue);
            Instance.attributesMean.add(sum/dataSet.size());
        }

        //Scale values
        for (int i = 1; i < attributeCount; i++)
        {
            double sum = 0;
            for (Instance instance : dataSet)
            {
                Double min = Instance.attributesMin.get(i);
                Double max = Instance.attributesMax.get(i);
                Double val = instance.attributes.get(i);
                Double scaleValue = ((val - min) / (max - min));

                instance.attributes.set(i, scaleValue);
                sum += scaleValue;

            }
            Instance.attributesMean.set(i, sum/dataSet.size());
        }

        //Standard deviation
        for (int i = 0; i < attributeCount; i++)
        {
            double sumDeviation = 0;
            for (Instance instance : dataSet)
            {
                sumDeviation += Math.pow(instance.attributes.get(i) - Instance.attributesMean.get(i), 2);
            }
            Instance.attributesDeviation.add(Math.sqrt(sumDeviation/dataSet.size()));
            System.out.println(i + "mean: " + Instance.attributesMean.get(i) + ", deviation: " + Instance.attributesDeviation.get(i));
        }

        //Set axis attributes
        Instance.setAxisAttributes();
    }

    public void populateClusters()
    {
        for (Cluster cluster : clusters)
        {
            cluster.computeCentroid();
            cluster.clear();
        }

        for (Instance instance : dataSet)
        {
            double closestDistance = Double.MAX_VALUE;
            Cluster closestCluster = null;

            for (Cluster cluster : clusters)
            {
                double d = instance.distanceTo(cluster.centroid);
                if (d < closestDistance)
                {
                    closestDistance = d;
                    closestCluster = cluster;
                }
            }
            closestCluster.add(instance);
        }
    }
}
