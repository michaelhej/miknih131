package sample;

import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Created by michael on 22/02/16.
 */
public class Cluster
{
    public static int numClusters = 0;
    public Point point;
    public Color color;
    public Instance centroid;
    public ArrayList<Instance> instances = new ArrayList<>();

    public Cluster(Instance centroid)
    {
        this.centroid = centroid;
        switch (numClusters)
        {
            case 0:
                color = Color.ORANGE;
                break;
            case 1:
                color = Color.LIME;
                break;
            case 2:
                color = Color.CORNFLOWERBLUE;
                break;
            case 3:
                color = Color.RED;
                break;
            case 4:
                color = Color.YELLOW;
                break;
        }

        numClusters++;
    }

    public void add(Instance instance)
    {
        instances.add(instance);
    }

    public void clear()
    {
        instances.clear();
    }

    public void computeCentroid()
    {
        if (instances.size() == 0)
            return;

        int attributeCount = centroid.attributes.size();
        centroid = new Instance();
        for (int i = 0; i < attributeCount; i++)
        {
            double sum = 0;
            for (Instance instance : instances)
            {
                sum += instance.attributes.get(i);
            }

            centroid.attributes.add(sum/instances.size());
        }
    }
}
