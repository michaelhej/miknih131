package sample;

import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1CFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.*;

/**
 * Created by michael on 01/03/16.
 */
public class PDFMaker
{
    public static void Make() throws Exception
    {
        PDDocument doc = new PDDocument();
        PDPage page = new PDPage();

        doc.addPage(page);

        PDFont font = PDType1Font.HELVETICA_BOLD;
        PDPageContentStream content = new PDPageContentStream(doc, page);

        content.beginText();
        content.setFont(font, 14);
        content.newLineAtOffset(100, 100);
        content.showText("Vapou");
        content.newLineAtOffset(100, 120);
        content.showText("X X X");
        content.endText();
        content.endMarkedContent();

        content.close();

        File outFile = new File("mega.pdf");
        doc.save(outFile.getAbsolutePath());
        doc.close();
    }

}
