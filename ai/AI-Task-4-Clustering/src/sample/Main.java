package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

public class Main extends Application
{
    Kmeans k = new Kmeans();
    Scene scene;
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        scene = new Scene(root, 700, 700);
        primaryStage.setTitle("K-means clustering");
        primaryStage.setScene(scene);
        primaryStage.show();


        //Map buttons
        Button btn = (Button)scene.lookup("#btnGo");
        btn.setOnAction(this::assign);

        btn = (Button)scene.lookup("#btnFormula");
        btn.setOnAction((event) -> {
            Instance.formula++;
            assign(null);
        });

        btn = (Button)scene.lookup("#btnAttr");
        btn.setOnAction((event) -> {
            Instance.axisAttributeCnt+=2;
            Instance.setAxisAttributes();
            assign(null);
        });

        btn = (Button)scene.lookup("#btnReload");
        btn.setOnAction((event) -> {
            loadData();
            assign(null);
        });

        loadData();
    }

    private void loadData()
    {
        k.loadData();
        k.analyzeAttributes();
        k.createClusters();
    }

    @FXML
    private void assign(ActionEvent event)
    {
        for (int i = 0; i < 10; i++)
            k.populateClusters();

        Canvas canvas = new Canvas(900, 900);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawClusters(gc);

        Pane plotter = (Pane)scene.lookup("#panePlotter");
        plotter.getChildren().clear();
        plotter.getChildren().add(canvas);

    }

    private void drawClusters(GraphicsContext gc)
    {
        gc.fill();

        //Draw cluster instances
        for (Cluster cluster : k.clusters)
        {
            gc.setFill(cluster.color);
            for (Instance instance : cluster.instances)
            {
                gc.setStroke(Color.web(cluster.color.toString(), 1 - Math.min(0, cluster.centroid.distanceTo(instance))));
                gc.setFill(instance.typeColor());
                gc.fillOval(instance.getPoint().x, instance.getPoint().y, 7, 7);

                gc.setLineWidth(1);
                gc.strokeOval(instance.getPoint().x - 2, instance.getPoint().y - 2, 11, 11);
            }
        }

        //Draw centroids
        for (Cluster cluster : k.clusters)
        {
            gc.setStroke(Color.BLACK);
            gc.setLineWidth(6);
            gc.strokeOval(cluster.centroid.getPoint().x, cluster.centroid.getPoint().y, 10, 10);

            gc.setStroke(cluster.color);
            gc.setLineWidth(2);
            gc.strokeOval(cluster.centroid.getPoint().x, cluster.centroid.getPoint().y, 10, 10);
        }

        //Print current config
        System.out.println("\nAttributes:");
        for (Integer x : Instance.xAxisAttributes)
            System.out.println("X: " + x);

        for (Integer y : Instance.yAxisAttributes)
            System.out.println("Y: " + y);

        System.out.println("Axis attributes: " + Instance.axisAttributeCnt);
        System.out.println("Formula: " + Instance.formula % 5);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
