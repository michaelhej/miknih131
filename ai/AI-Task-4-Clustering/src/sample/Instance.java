package sample;

import javafx.scene.paint.Color;
import java.util.ArrayList;

public class Instance
{
    ArrayList<Double> attributes = new ArrayList<>();
    static ArrayList<Double> attributesMax = new ArrayList<>();
    static ArrayList<Double> attributesMin = new ArrayList<>();
    static ArrayList<Double> attributesMean = new ArrayList<>();
    static ArrayList<Double> attributesDeviation = new ArrayList<>();
    static ArrayList<Integer> xAxisAttributes = new ArrayList<>();
    static ArrayList<Integer> yAxisAttributes = new ArrayList<>();

    public static int formula = 0;
    public static int axisAttributeCnt = 2;

    public Point getPoint()
    {
        Point p = new Point();
        for (Integer i : xAxisAttributes) {
            p.x += diff(i);
        }

        for (Integer i : yAxisAttributes) {
            p.y += diff(i);
        }

        return p;
    }

    public Color typeColor()
    {
        switch (attributes.get(0).intValue())
        {
            case 1:
                return Color.LIME;
            case 2:
                return Color.RED;
            case 3:
                return Color.ORANGE;

        }
        return Color.BLACK;
    }

    private int diff(int index)
    {
        Double val = attributes.get(index);
        Double stdDeviation = attributesDeviation.get(index);

        Double diffValue = val;

        switch (formula % 5) {
            case 0:
                diffValue = val * 200;
                break;
            case 1:
                diffValue = val * val * 200;
                break;
            case 2:
                diffValue = val * val * stdDeviation * 700;
                break;
            case 3:
                diffValue = val * val / stdDeviation * 80;
                break;
            case 4:
                diffValue = val * stdDeviation * 1000;
                break;
        }

        return diffValue.intValue();
    }

    public double distanceTo(Instance other)
    {
        return eucledeanDistance(other);
    }

    public Double eucledeanDistance(Instance other)
    {
        Double d = 0.0;

        for (int i = 1; i < attributes.size(); i++)
            d += Math.pow(attributes.get(i) - other.attributes.get(i), 2);

        return Math.sqrt(d);
    }

    public Double minkowskiDistance(Instance other)
    {
        int order = 3;
        Double sum = 0.0;

        for (int i = 1; i < attributes.size(); i++)
        {
            double d = Math.abs(attributes.get(i) - other.attributes.get(i));
            sum += Math.pow(d, order);
        }

        return Math.pow(sum, 1/order);
    }

    public static void setAxisAttributes()
    {
        Instance.xAxisAttributes.clear();
        Instance.yAxisAttributes.clear();

        if (axisAttributeCnt > attributesDeviation.size())
            axisAttributeCnt = 2;

        int attrCnt = 0;
        while (attrCnt <= axisAttributeCnt)
        {
            Double maxDeviation = Double.MIN_VALUE;
            int maxIndex = 0;

            for (int i = 1; i < attributesDeviation.size(); i++)
            {
                Double val = attributesDeviation.get(i);
                if (val > maxDeviation && !xAxisAttributes.contains(i) && !yAxisAttributes.contains(i))
                {
                    maxDeviation = val;
                    maxIndex = i;
                }
            }

            if (maxIndex > 0)
            {
                if (attrCnt % 2 == 0)
                    xAxisAttributes.add(maxIndex);
                else
                    yAxisAttributes.add(maxIndex);
            }

            attrCnt++;
        }

        //Even number of attributes
        if (xAxisAttributes.size() > yAxisAttributes.size())
            xAxisAttributes.remove(xAxisAttributes.size() - 1);
        if (yAxisAttributes.size() > xAxisAttributes.size())
            yAxisAttributes.remove(yAxisAttributes.size() - 1);
    }
}
