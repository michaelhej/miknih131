﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_1_Search
{
    partial class Nav
    {
        private const int PointSize = 10;

        /// <summary>
        /// Draw all locations and connecting roads
        /// </summary>
        /// <param name="g"></param>
        /// <param name="clear"></param>
        public void DrawMap(Graphics g, bool clear = false)
        {
            if (clear)
                g.Clear(Color.FromArgb(64,64,64));

            Pen pen = new Pen(Color.LightSlateGray, 3);
            Font font = new Font("Verdana", 8, FontStyle.Bold);
            Brush nodeBrush = new SolidBrush(Color.DarkGray);
            Brush fontBrush = new SolidBrush(Color.Coral);

            //Draw roads
            foreach (Link link in _links)
            {
                Node start = _nodes.Find(node => node.Name == link.Node1);
                Node end = _nodes.Find(node => node.Name == link.Node2);
                g.DrawLine(pen, start.X, start.Y, end.X, end.Y);
            }

            //Draw locations
            foreach (Node node in _nodes)
            {
                SizeF stringLength = g.MeasureString(node.Name, font);
                g.FillEllipse(nodeBrush, node.X - PointSize / 2, node.Y - PointSize / 2, PointSize, PointSize);
                g.DrawString(node.Name, font, fontBrush, node.X - stringLength.Width / 2, node.Y - 25);

            }
        }

        /// <summary>
        /// Finds node att given point
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Node FindAt(int x, int y)
        {
            foreach (Node node in _nodes)
            {
                if (x > node.X - PointSize / 2 && x < node.X + PointSize / 2)
                {
                    if (y > node.Y - PointSize / 2 && y < node.Y + PointSize / 2)
                    {
                        Console.WriteLine("Node clicked: " + node.Name);
                        Adjacent(node);
                        return node;
                    }
                }
            }
            return null;
        }
    }
}
