﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AI_Task_1_Search
{

    partial class Nav
    {
        private const int MaxDepth = 15;

        public Node Start;
        public Node Goal;

        private List<Node> _nodes = new List<Node>();
        private List<Link> _links = new List<Link>();

        public int VisualSpeed { get; set; }

        private Dictionary<Node, double> _distance = new Dictionary<Node, double>();
        private Dictionary<Node, Node> _parent = new Dictionary<Node, Node>();
        private int _expanded = 0;
        private bool _slow = false;

        public enum SearchAlgorithm
        {
            IDDFS,
            BeFS,
            AStar
        };

        public bool Search(SearchAlgorithm algorithm, bool slow = false)
        {
            if (Start == null || Goal == null)
                return false;

            _slow = slow;
            Console.WriteLine(_slow);
            foreach (Node node in _nodes)
            {
                node.Expanded = false;
            }
            Graphics.Clear(Color.FromArgb(64, 64, 64));
            DrawMap(Graphics);
            _distance.Clear();
            _parent.Clear();
            _expanded = 0;

            if (algorithm == SearchAlgorithm.IDDFS)
            {
                PrintPath(IDDFS());
            }
            if (algorithm == SearchAlgorithm.BeFS)
            {
                BFS();
                ReconstructPath();
            }
            if (algorithm == SearchAlgorithm.AStar)
            {
                AStar();
                ReconstructPath();
            }

            return true;
        }

        /// <summary>
        /// Find adjecent nodes of given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<Node> Adjacent(Node node)
        {
            List<Node> edges = new List<Node>();
            foreach (Link link in _links)
            {
                Node connectedNode;
                if (Equals(link.Node1, node.Name))
                    connectedNode = _nodes.Find(n => n.Name == link.Node2);
                else if (Equals(link.Node2, node.Name))
                    connectedNode = _nodes.Find(n => n.Name == link.Node1);
                else
                    continue;

                if (!edges.Contains(connectedNode))
                {
                    edges.Add(connectedNode);
                }
            }

            return edges;
        }

        /// <summary>
        /// Reconstructs the path from startnode to the goal node
        /// </summary>
        /// <param name="node"></param>
        private void ReconstructPath()
        {
            List<Node> path = new List<Node>();
            Node nextNode = Goal;

            while (nextNode != Start)
            {
                path.Add(nextNode);
                nextNode = _parent[nextNode];
            }
            path.Add(nextNode);

            path.Reverse();
            PrintPath(path);
        }

        public void PrintPath(List<Node> path)
        {
            Console.WriteLine("");
            Console.Write("Path: ");
            if (path == null)
            {
                Console.Write("no path");
                return;
            }
            Node prevNode = path.First();
            foreach (Node node in path)
            {
                Console.Write(node.Name);
                if (node != Goal)
                {
                    Console.Write(" -> ");
                }
                if (node != path.First())
                {
                    DrawPath(prevNode, node, Color.Lime);
                }
                prevNode = node;
            }
            Console.WriteLine("");
            Console.WriteLine(_expanded + " nodes expanded.");
        }

        #region Iterative Deepening Depth First Search (recursive)
        /// <summary>
        /// Iterative deepening depth first search
        /// </summary>
        /// <returns></returns>
        private List<Node> IDDFS()
        {
            Log("##### IDDFS #####", 5);

            List<Node> path = new List<Node>();
            for (int i = 0; i < MaxDepth; i++)
            {
                DrawMap(Graphics);
                foreach (Node node in _nodes)
                {
                    node.Expanded = false;
                }

                Log("Depth: " + i, 4);
                path = DLS(Start, i, new List<Node>());
                if (path != null)
                    return path;
            }

            return null;
        }
        /// <summary>
        /// Depth limited search
        /// </summary>
        /// <param name="node"></param>
        /// <param name="depth"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<Node> DLS(Node node, int depth, List<Node> path)
        {
            node.Expanded = true;
            Log(node, LogMode.Visiting, depth);
            if (node == Goal)
            {
                Log(node, LogMode.GoalNode, depth);

                path.Add(node);
                return path;
            }

            if (depth > 0)
            {
                Log(node, LogMode.Expanding, depth);
                _expanded++;

                //Expand current node and add adjecent to frontier
                foreach (Node adjNode in Adjacent(node))
                {
                    if (adjNode.Expanded)
                        continue;
                    DrawPath(node, adjNode, Color.Blue);

                    //Duplicates the path and adding node
                    List<Node> goalPath = path.ToList();
                    goalPath.Add(node);

                    goalPath = DLS(adjNode, depth - 1, goalPath);

                    if (goalPath != null)
                    {
                        return goalPath;
                    }
                    else
                    {
                        Log(adjNode, LogMode.Backtrack, depth - 1);
                    }
                }
            }
            return null;
        }
        #endregion

        #region BFS
        /// <summary>
        /// Best First Search (Greedy search)
        /// </summary>
        /// <returns></returns>
        private void BFS()
        {
            List<Node> frontier = new List<Node>();
            frontier.Add(Start);

            while (frontier.Count > 0)
            {

                Node current = BFSNextNode(frontier);
                frontier.Remove(current);
                Log(current, LogMode.Visiting);


                if (current == Goal)
                {
                    Log(current, LogMode.GoalNode);
                    return;
                }

                Log(current, LogMode.Expanding);
                _expanded++;

                foreach (Node node in Adjacent(current))
                {
                    if (!node.Expanded)
                    {
                        node.Expanded = true;

                        Log(node, LogMode.Adding);
                        frontier.Add(node);
                        DrawPath(current, node, Color.Blue);
                        _parent.Add(node, current);
                    }
                }
            }
        }

        /// <summary>
        /// Get best (greedy) node to expand from frontier
        /// </summary>
        /// <param name="frontier"></param>
        /// <returns></returns>
        private Node BFSNextNode(List<Node> frontier)
        {
            Node bestNode = frontier.First();
            double shortestDistance = bestNode.Distance(Goal);
            foreach (Node node in frontier)
            {
                double nodeDistance = node.Distance(Goal);
                if (nodeDistance < shortestDistance)
                {
                    shortestDistance = nodeDistance;
                    bestNode = node;
                }
                Console.WriteLine("Distance from " + node.Name + ": " + nodeDistance);
            }
            return bestNode;
        }
        #endregion

        #region A*

        private void AStar()
        {
            List<Node> frontier = new List<Node>();
            frontier.Add(Start);
            Start.Expanded = true;
            _distance.Clear();
            _distance.Add(Start, 0);


            while (frontier.Count > 0)
            {
                Node current = AStarNextNode(frontier);
                Log(current, LogMode.Visiting);

                frontier.Remove(current);

                if (current == Goal)
                {
                    //Check if the next-best node is closer, thus having a potentially shorter path
                    Node nextBest = AStarNextNode(frontier);
                    if (_distance[nextBest] + nextBest.Distance(Goal) < _distance[current])
                    {
                        frontier.Add(current);
                        current = nextBest;
                    }
                    else
                    {
                        Log(current, LogMode.GoalNode);
                        return;
                    }
                }

                frontier.Remove(current);
                Log(current, LogMode.Expanding);
                _expanded++;

                foreach (Node node in Adjacent(current))
                {
                    double nodeDistance = _distance[current] + current.Distance(node);

                    if (node.Expanded)
                    {
                        if (nodeDistance < _distance[node])
                        {
                            frontier.Add(node); //Follow the new path
                            _parent[node] = current;
                            _distance[node] = nodeDistance;
                        }
                    }
                    else if (frontier.Contains(node))
                    {
                        if (nodeDistance < _distance[node])
                        {
                            _parent[node] = current;
                            _distance[node] = nodeDistance;
                        }
                    }
                    else
                    {
                        node.Expanded = true;
                        frontier.Add(node);

                        //Track
                        _distance.Add(node, nodeDistance);
                        _parent.Add(node, current);

                        //Log
                        Log(node, LogMode.Adding);
                        DrawPath(current, node, Color.Blue);
                    }
                }
            }
        }


        /// <summary>
        /// Get best (greedy) node to expand from frontier
        /// </summary>
        /// <param name="frontier"></param>
        /// <param name="ignoreNode"></param>
        /// <returns></returns>
        private Node AStarNextNode(List<Node> frontier)
        {
            Node bestNode = frontier.First();
            double shortestDistance = double.MaxValue;
            foreach (Node node in frontier)
            {
                double nodeDistance = _distance[node] + node.Distance(Goal);
                if (nodeDistance < shortestDistance)
                {
                    shortestDistance = nodeDistance;
                    bestNode = node;
                }
                Console.WriteLine("Distance from " + node.Name + ": " + nodeDistance);
            }
            return bestNode;
        }

        #endregion
    }
}
