﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AI_Task_1_Search
{
    partial class Nav
    {
        public Graphics Graphics { get; set; }
        /// <summary>
        /// Read nodes from file
        /// </summary>
        /// <param name="fileName"></param>
        public void ReadNodes(string fileName)
        {
            _nodes.Clear();
            StreamReader reader;
            using (reader = new StreamReader(fileName))
            {
                string line;
                bool skipLine = true;

                while ((line = reader.ReadLine()) != null)
                {
                    //Skip first line in file
                    if (skipLine)
                    {
                        skipLine = false;
                        continue;
                    }

                    string[] lineValues = line.Split(';');

                    //Skip last/invalid lines
                    if (lineValues.Length < 3)
                        continue;

                    Node node = new Node();
                    node.Name = lineValues[0];
                    node.X = Int32.Parse(lineValues[1]);
                    node.Y = Int32.Parse(lineValues[2]);

                    _nodes.Add(node);
                }
            }
        }

        /// <summary>
        /// Read links from file
        /// </summary>
        /// <param name="fileName"></param>
        public void ReadLinks(string fileName)
        {
            _links.Clear();
            StreamReader reader;
            using (reader = new StreamReader(fileName))
            {
                string line;
                bool skipLine = true;

                while ((line = reader.ReadLine()) != null)
                {
                    //Skip first line in file
                    if (skipLine)
                    {
                        skipLine = false;
                        continue;
                    }

                    string[] lineValues = line.Split(';');

                    //Skip last/invalid lines
                    if (lineValues.Length < 2)
                        continue;

                    Link link = new Link();
                    link.Node1 = lineValues[0];
                    link.Node2 = lineValues[1];

                    _links.Add(link);
                }
            }
        }

        private enum LogMode
        {
            Visiting,
            Expanding,
            GoalNode,
            Backtrack,
            Distance,
            Adding
        };

        private void Log(Node node, LogMode mode, int depth = MaxDepth)
        {

            string str = "";

            switch (mode)
            {
                case LogMode.Visiting:
                    str += "  -> Visiting ";
                    DrawNode(node, Color.DeepPink);
                    break;
                case LogMode.Expanding:

                    if (_slow)
                        Thread.Sleep(VisualSpeed);

                    DrawNode(node, Color.Lime);
                    str += "    -> Expanding ";
                    break;
                case LogMode.Adding:
                    DrawNode(node, Color.Wheat);
                    str += "Adding ";
                    break;
                case LogMode.Backtrack:
                    str += "<- Backtracking ";
                    return;
                    break;
                case LogMode.GoalNode:
                    DrawNode(node, Color.Gold);
                    str += " # Found goal node ";
                    break;
            }

            str += node.Name;
            Log(str, depth);
        }

        private void DrawNode(Node node, Color color)
        {
            Brush brush = new SolidBrush(color);
            Graphics.FillEllipse(brush, node.X - PointSize / 2, node.Y - PointSize / 2, PointSize, PointSize);
        }

        private void DrawPath(Node node1, Node node2, Color color)
        {
            Pen pen = new Pen(color, 2);
            Graphics.DrawLine(pen, node1.X, node1.Y, node2.X, node2.Y);

            if (_slow)
                Thread.Sleep(VisualSpeed);
        }

        private void Log(string str, int depth = MaxDepth)
        {
            for (int i = 0; i < MaxDepth - depth; i++)
            {
                str = "    " + str;
            }
            Console.WriteLine(str);
        }
    }
}
