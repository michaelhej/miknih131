﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_1_Search
{
    class Link
    {
        public string Node1 { get; set; }
        public string Node2 { get; set; }

        public Link()
        {
            Node1 = Node2 = "";
        }

        public Link(string start, string end)
        {
            Node1 = start;
            Node2 = end;
        }
    }
}
