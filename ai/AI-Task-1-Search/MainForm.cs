﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.ComponentModel.Com2Interop;

namespace AI_Task_1_Search
{
    public partial class MainForm : Form
    {
        private Nav _nav = new Nav();
        public MainForm()
        {
            InitializeComponent();
        }

        private void FormLoad(object sender, EventArgs e)
        {
            _nav.VisualSpeed = 100;
            _nav.Graphics = this.CreateGraphics();
        }

        private void FormPaint(object sender, PaintEventArgs e)
        {
            _nav.DrawMap(e.Graphics);
        }

        private void FormMouseClick(object sender, MouseEventArgs e)
        {
            Node node = _nav.FindAt(e.X, e.Y);
            if (node != null)
            {
                if (e.Button == MouseButtons.Left)
                    _nav.Start = node;
                else
                    _nav.Goal = node;
            }
        }

        private void btnIDDFS_Click(object sender, EventArgs e)
        {
            _nav.Search(Nav.SearchAlgorithm.IDDFS, Control.ModifierKeys == Keys.Alt);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _nav.Search(Nav.SearchAlgorithm.BeFS, Control.ModifierKeys == Keys.Alt);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _nav.Search(Nav.SearchAlgorithm.AStar, Control.ModifierKeys == Keys.Alt);
        }

        private void btnLoadMap_Click(object sender, EventArgs e)
        {
            _nav.ReadNodes(@"..\..\_locations.csv");
            _nav.ReadLinks(@"..\..\_roads.csv");
            _nav.DrawMap(this.CreateGraphics(), true);
        }

        private void btnLoadTree_Click(object sender, EventArgs e)
        {
            _nav.ReadNodes(@"..\..\locations.csv");
            _nav.ReadLinks(@"..\..\roads.csv");
            _nav.DrawMap(this.CreateGraphics(), true);
        }
    }
}
