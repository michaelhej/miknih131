﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Task_1_Search
{
    class Node
    {
        public string Name { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Expanded { get; set; }

        public Node()
        {
            Name = "";
            X = Y = 0;
        }

        public Node(string name, int x, int y)
        {
            Name = name;
            X = x;
            Y = y;
        }

        public double Distance(Node node)
        {
            return Math.Sqrt(Math.Pow(X - node.X, 2) + Math.Pow(Y - node.Y, 2));
        }

    }
}
