#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#if defined(_MSDOS) || defined(_WIN32)
#include <windows.h>
#endif
#include "sqlext.h"

void printOptions();
void newEmployeeForm();
char* employeeName(int id);
void createNewEmployee(char* name, int salary, int manager);
void showEmployees(int filterId, char* filterName);
int isManager(int id);
void rmEmployee();
void updateSalary();
int getNum(char* prompt);
char* getStr(char* prompt);
void error(char* errstr);
SQLHSTMT allocStatement();