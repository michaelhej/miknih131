/*
Databasteknik 2
Lab 4
ODBC i C
*/

#include "main.h"

SQLHENV environment;
SQLHDBC connection;

/*
Main application entry
Set up environment and allocate handles
*/
int main(void)
{
	int opt = -1;

	if (SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &environment) != SQL_SUCCESS)
		error("Error allocating environment");

	if (SQLSetEnvAttr(environment, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER) != SQL_SUCCESS)
		error("Failed to set environment");

	if (SQLAllocHandle(SQL_HANDLE_DBC, environment, &connection) != SQL_SUCCESS)
		error("Error allocating connection handle");

	if (SQLConnect(connection, (SQLCHAR*)"dbk", SQL_NTS, (SQLCHAR*)"dbtek114", SQL_NTS, (SQLCHAR*)"kanta", SQL_NTS) != SQL_SUCCESS)
		error("Unable to connect");

	//Main loop
	printOptions();
	while (opt != 0)
	{
		opt = getNum("Option");
		switch (opt)
		{
		case 1:
			showEmployees(-1, getStr("Employee name"));
			break;
		case 2:
			showEmployees(getNum("Employee id"), "");
			break;
		case 3:
			newEmployeeForm();
			break;
		case 4:
			rmEmployee();
			break;
		case 5:
			updateSalary();
			break;
		}
		printf("\n_____________\n");
	}

	//Disconnect from server and free handles
	SQLDisconnect(connection);
	SQLFreeHandle(SQL_HANDLE_DBC, connection);
	SQLFreeHandle(SQL_HANDLE_ENV, environment);

	printf("bye!\n");

	return EXIT_SUCCESS;
}

/*
Show all options
*/
void printOptions()
{
	printf("==== ODBC ====\n\n");
	printf("1. Search by namn\n");
	printf("2. Search by id\n");
	printf("3. Add new employee\n");
	printf("4. Remove employee\n");
	printf("5. Change salary\n");
	printf("0. Quit\n\n");
}

/*
Request a number input
*/
int getNum(char* prompt)
{
	int id;
	printf("%s: ", prompt);
	while (scanf_s("%d", &id) != 1)
	{
		printf("%s: ", prompt);
		while (getchar() != '\n');
	}
	while (getchar() != '\n');
	return id;
}

/*
Request a name input
*/
char* getStr(char* prompt)
{
	SQLCHAR name[21];
	printf("%s: ", prompt);
	fgets(name, sizeof name, stdin);
	if (name[strlen(name) - 1] == '\n')
		name[strlen(name) - 1] = '\0';

	return _strdup(name);
}

/*
Show form to create a new employee
*/
void updateSalary()
{
	int id, salary;
	id = getNum("Employee id");

	while (employeeName(id) == 0)
	{
		printf("Enter a valid employee id (0 = abort)\n");
		id = getNum("Employee id");
		if (id == 0)
			return;
	}

	salary = getNum("Enter new salary");

	SQLHSTMT sh = allocStatement();

	SQLINTEGER idSize = sizeof id;
	SQLINTEGER salarySize = sizeof salary;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &salary, sizeof salary, &salarySize);
	SQLBindParameter(sh, 2, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &id, sizeof id, &idSize);

	if (SQLExecDirect(sh, (SQLCHAR*)"UPDATE employee SET salary = ? WHERE number = ?", SQL_NTS) != SQL_SUCCESS)
	{
		printf("\nFailed to update salary!\n");
	}
	else
	{
		printf("\nSalary updated!\n");
	}
	SQLFreeHandle(SQL_HANDLE_STMT, sh);
}

/*
Show form to create a new employee
*/
void newEmployeeForm()
{
	char* name;
	int salary;
	int manager;

	printf("\n--- New employee ---\n");
	name = getStr("Name");
	salary = getNum("Salary");
	manager = getNum("Manager id");

	while (employeeName(manager) == 0)
	{
		printf("\nManager does not exist!\n");
		printf("Enter a valid manager id (0 = abort)\n");
		manager = getNum("Manager id");
		if (manager == 0)
			return;
	}
	createNewEmployee(name, salary, manager);
}

/*
Execute new employee command on the db
*/
void createNewEmployee(char* name, int salary, int manager)
{
	SQLHSTMT sh = allocStatement();

	SQLINTEGER nameSize = SQL_NTS;
	SQLINTEGER salarySize = sizeof salary;
	SQLINTEGER managerSize = sizeof manager;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR, 20, 0, name, sizeof name, &nameSize);
	SQLBindParameter(sh, 2, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &salary, sizeof salary, &salarySize);
	SQLBindParameter(sh, 3, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &manager, sizeof manager, &managerSize);

	if (SQLExecDirect(sh, (SQLCHAR*)"INSERT INTO employee(name, salary, manager) values(?, ?, ?)", SQL_NTS) != SQL_SUCCESS)
	{
		printf("\nFailed to add employee!\n");
	}
	else
	{
		printf("\nNew employee added!\n");
	}
	SQLFreeHandle(SQL_HANDLE_STMT, sh);
}

/*
Remove employee from the database
*/
void rmEmployee()
{
	int id = getNum("Remove employee with id");
	if (isManager(id))
	{
		printf("Employee is a manager and cannot be removed!\n");
		return;
	}

	SQLHSTMT sh = allocStatement();
	SQLINTEGER idSize = sizeof id;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &id, sizeof id, &idSize);
	if (SQLExecDirect(sh, (SQLCHAR*)"DELETE FROM employee WHERE number = ?", SQL_NTS) != SQL_SUCCESS)
		printf("Could not remove employee!\n");

	if (SQLFetch(sh) == SQL_SUCCESS)
		printf("Employee successfully removed!\n");
	else
		printf("Employee not found!\n");

	SQLFreeHandle(SQL_HANDLE_STMT, sh);
}

/*
Checks is an employee is a manager
*/
int isManager(int id)
{
	SQLHSTMT sh = allocStatement();
	SQLINTEGER idSize = sizeof id;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER,
		0, 0, &id, sizeof id, &idSize);

	if (SQLExecDirect(sh, (SQLCHAR*)"SELECT 1 FROM employee WHERE manager = ?", SQL_NTS) != SQL_SUCCESS)
	{
		SQLFreeHandle(SQL_HANDLE_STMT, sh);
		return 0;
	}

	if (SQLFetch(sh) == SQL_SUCCESS)
	{
		SQLFreeHandle(SQL_HANDLE_STMT, sh);
		return 1;
	}

	return 0;
}

/*
Get name of an employee via ID
*/
char* employeeName(int id)
{
	SQLHSTMT sh = allocStatement();
	SQLINTEGER idSize = sizeof id;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER,
		0, 0, &id, sizeof id, &idSize);

	if (SQLExecDirect(sh, (SQLCHAR*)"SELECT name FROM employee WHERE number = ?", SQL_NTS) != SQL_SUCCESS)
	{
		SQLFreeHandle(SQL_HANDLE_STMT, sh);
		return 0;
	}

	if (SQLFetch(sh) == SQL_SUCCESS)
	{
		SQLCHAR name[21];
		SQLGetData(sh, 1, SQL_C_CHAR, name, sizeof name, 0);
		SQLFreeHandle(SQL_HANDLE_STMT, sh);
		return _strdup(name);
	}

	return 0;
}

/*
Search for an employee in the database on number or name
*/
void showEmployees(int filterId, char* filterName)
{
	SQLHSTMT sh = allocStatement();

	int rows = 0;
	SQLINTEGER idSize = sizeof filterId;
	SQLINTEGER nameSize = SQL_NTS;

	SQLBindParameter(sh, 1, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER,
		0, 0, &filterId, sizeof filterId, &idSize);

	SQLBindParameter(sh, 2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_CHAR,
		20, 0, filterName, sizeof filterName, &nameSize);

	if (SQLExecDirect(sh, (SQLCHAR*)"SELECT number, name, salary, manager FROM employee WHERE number = ? OR name LIKE ?", SQL_NTS) != SQL_SUCCESS)
	{
		printf("Could not execute search");
		SQLFreeHandle(SQL_HANDLE_STMT, sh);
		return;
	}

	printf("\nSearch results:\n");

	while (SQLFetch(sh) == SQL_SUCCESS)
	{
		rows++;
		SQLINTEGER id;
		SQLCHAR name[21];
		SQLINTEGER salary;
		SQLINTEGER managerId;

		SQLGetData(sh, 1, SQL_C_SLONG, &id, sizeof id, 0);
		SQLGetData(sh, 2, SQL_C_CHAR, name, sizeof name, 0);
		SQLGetData(sh, 3, SQL_C_SLONG, &salary, sizeof salary, 0);
		SQLGetData(sh, 4, SQL_C_SLONG, &managerId, sizeof managerId, 0);
		char* managerName = employeeName(managerId);

		printf("\n---------------\n");
		printf("Name: %s\n", name);
		printf("Nummer: %d\n", (int)id);
		printf("Salary: %d\n", (int)salary);
		printf("Manager: [%d] %s\n", (int)managerId, managerName);
	}
	printf("---------------\n\n");
	printf("%d employees found\n\n", rows);
	SQLFreeHandle(SQL_HANDLE_STMT, sh);
}

/*
Allocate a new statement handle
*/
SQLHSTMT allocStatement()
{
	SQLHSTMT statementHandle;
	if (SQLAllocHandle(SQL_HANDLE_STMT, connection, &statementHandle) != SQL_SUCCESS)
		error("Error allocating statement handle");
	return statementHandle;
}

/*
Error occured
*/
void error(char* errstr)
{
	printf(errstr);
	getch();
	exit(1);
}