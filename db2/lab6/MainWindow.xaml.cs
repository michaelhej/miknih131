﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Windows;
using System.Windows.Controls;
using MySql.Data.MySqlClient;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Threading;
using System.Windows.Input;

namespace lab6
{
    public partial class MainWindow : Window
    {
        private static string connectionString = "DSN=mysql";
        private static string tableName = "categories";

        private OdbcConnection _connection;
        private OdbcDataAdapter _adapter;
        private DataTable _dataTable;
        private Stack<DataTable> _history;

        public MainWindow()
        {
            InitializeComponent();
            _history = new Stack<DataTable>();
        }

        /// <summary>
        /// Fill datagrid with table data
        /// </summary>
        private void Fill()
        {
            try
            {
                _adapter = new OdbcDataAdapter("SELECT * FROM " + tableName, _connection);
                _dataTable = new DataTable(tableName);
                _adapter.FillSchema(_dataTable, SchemaType.Mapped);

                if (_dataTable.PrimaryKey.Length < 1)
                {
                    labelStatus.Content = "No primary key in table!";
                    return;
                }
                _adapter.Fill(_dataTable);
                myDataGrid.ItemsSource = _dataTable.AsDataView();
                myDataGrid.IsEnabled = true;
            }
            catch (Exception)
            {
                labelStatus.Content = "Could not fetch " + tableName;
            }
        }

        /// <summary>
        /// Connect to the server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Connect(object sender, RoutedEventArgs e)
        {
            try
            {
                _connection = new OdbcConnection(connectionString);
                _connection.Open();
            }
            catch (Exception ex)
            {
                labelStatus.Content = "Connection failed: " + ex.StackTrace;
                return;
            }

            labelStatus.Content = "Connected";
            Fill();
        }

        /// <summary>
        /// Push change of states to the history and change the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            _history.Push(_dataTable.Copy());
            btnUndo.IsEnabled = true;
            Dispatcher.BeginInvoke(new Action(() => SaveDataSet()), DispatcherPriority.Background);
        }

        /// <summary>
        /// Save row deletion to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                RowEditEnding(null, null);
        }

        /// <summary>
        /// Save changes done in the datagrid to the database
        /// </summary>
        private void SaveDataSet()
        {
            try
            {
                OdbcCommandBuilder builder = new OdbcCommandBuilder(_adapter);
                if (_adapter.Update(_dataTable) > 0)
                    labelStatus.Content = "Changes saved";
                else
                    _history.Pop();

                if (_history.Count == 0)
                    btnUndo.IsEnabled = false;
            }
            catch (Exception)
            {
                labelStatus.Content = "Could not save changes";
            }
        }

        /// <summary>
        /// Undo previous save action to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            DataTable undoTable = _history.Pop();
            DataTableReader changeReader = new DataTableReader(undoTable);
            _dataTable.Load(changeReader, LoadOption.Upsert);
            if (_dataTable.Rows.Count > undoTable.Rows.Count)
                _dataTable.Rows[_dataTable.Rows.Count - 1].Delete();

            SaveDataSet();

            if (_history.Count == 0)
                btnUndo.IsEnabled = false;
        }
 
        /// <summary>
        /// Window is closing, disconnect any open connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
                _connection.Close();
        }
    }
}
