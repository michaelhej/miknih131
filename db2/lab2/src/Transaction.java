import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Transaction
{
	public static String dbName = "mega.db";
	public static String logName = "mega.db.log";
	public static void main(String[] args)
	{
		System.out.println("db <name> \t\t set db name (default mega.db)");
		System.out.println("create <qty> <data> \t create and fill new database");
		System.out.println("increment [-interrupt] \t increment database data [simulate interruption]");
		System.out.println("show \t\t\t show database data");
		System.out.println("log \t\t\t show transactions in log file");
		System.out.println("analyze [-fix] \t\t analyze log file [rollback uncommited transactions]");
		System.out.println("rollback <txid> \t rollback a transaction");
		System.out.println("rerun <txid> \t\t re-run a transaction");
		
		while(true)
		{
			try
			{
				parse();
			}
			catch (ArrayIndexOutOfBoundsException | NumberFormatException ex)
			{
				System.out.println("Oops!");
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	private static void parse() throws Exception
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("database> ");
        String line = br.readLine();
        String cmd[] = line.split(" ");
        if (cmd.length == 0)
        	return;
        switch (cmd[0])
        {
        	case "db":
        		if (cmd.length == 1)
        		{
        			System.out.println("Database is " + dbName);
        			return;
        		}
        		dbName = cmd[1];
        		logName = cmd[1] + ".log";
        		System.out.println("Database set to " + dbName);
        		break;
	        case "create":
	        	create(Integer.parseInt(cmd[1]), Integer.parseInt(cmd[2]));
	        	break;
	        case "increment":
	        	increment((cmd.length > 1 && cmd[1].equals("-interrupt")));
	        	break;
	        case "show":
	        	show();
	        	break;
	        case "log":
	        	showLog();
	        	break;
	        case "analyze":
	        	analyzeLog((cmd.length > 1 && cmd[1].equals("-rollback")));
	        	break;
	        case "rollback":
	        	recover((cmd.length > 1 ? cmd[1] : ""), true);
	        	break;
	        case "rerun":
	        	recover((cmd.length > 1 ? cmd[1] : ""), false);
	        	break;
	        case "q":
	        	System.exit(0);
	        	break;
	        default:
	        	System.out.println("Invalid command " + cmd[0]);
        }
	}
	
	/**
	 * Fill a new db-file with given number of ones
	 * @param filename
	 * @param qty
	 * @param simulateInterrupt	simulates an interruption at 50 %
	 * @throws IOException
	 */
	private static void create(int qty, int data) throws IOException
	{
		System.out.print ("### Filling " + dbName + " with " + qty + " integers of value " + data + "... ");
		
		//Delete existing database
		File file = new File(dbName);
		file.delete();
		
		DataOutputStream db = new DataOutputStream(new FileOutputStream(dbName));
		
		for (int i = 0; i < qty; i++)
		{
			db.writeInt(data);
			db.flush();
		}
		
		db.close();
		System.out.println("Done!");
	}
	
	/**
	 * Increament the values in the db-file
	 * @param filename
	 * @throws IOException
	 */
	private static void increment(boolean interrupt) throws IOException
	{
		System.out.print("### Incrementing " + dbName);
		if (interrupt)
			System.out.print(" (simulating interrupt)");
		System.out.print("... ");
		
		String txid = UUID.randomUUID().toString().substring(0, 5).toUpperCase();
		int data = 0;
		int i = 0;
		RandomAccessFile db = new RandomAccessFile(dbName, "rw");
		
		data = db.readInt();
		
		log("start-transaction:" + txid);

		db.seek(0);
		while(db.getFilePointer() < db.length())
		{	
			data = db.readInt();

			log("chg:" + i + ":" + data + ":" + (data + 1));
			
			db.seek(db.getFilePointer() - (Integer.SIZE / Byte.SIZE));
			db.writeInt(data + 1);
			i++;
			
			if (interrupt && db.getFilePointer() > (db.length() / 2))
				break;
		}
		db.close();
		if (!interrupt)
			log("commit:" + txid);
		System.out.println("Done! (txid: " + txid + ")");
	}
	
	/**
	 * Update all values in the database
	 * @param data
	 * @throws IOException
	 */
	private static void update(int i, int data) throws IOException
	{
		RandomAccessFile db = new RandomAccessFile(dbName, "rw");
		long position = i * Integer.SIZE / Byte.SIZE;
		
		if (position < db.length())
		{
			db.seek(position);
			db.writeInt(data);
		}
		db.close();
	}
	
	/**
	 * Show the a summary of the values for the given db-file
	 * @param filename
	 * @throws IOException
	 */
	private static void show() throws IOException
	{
		System.out.println("### Showing " + dbName);
		
		int data = 0;
		int i = 0;
		
		DataInputStream db = new DataInputStream(new FileInputStream(dbName));
		
		while(true)
		{
			i++;
			try
			{
				if (data != (data = db.readInt()))
				{
					System.out.println("Data at " + i + ".. -> " + data);
				}
			}
			catch (EOFException ex)
			{
				--i;
				db.close();
				System.out.println(i + " values in database");
				break;
			}
		}
	}
	
	/**
	 * Log action to log file
	 * @param str
	 * @throws IOException
	 */
	private static void log(String str) throws IOException
	{
		FileWriter writer = new FileWriter(logName, true);
		writer.write(str + ":" + (new Date()) + "\n");
		writer.close();
	}


	/**
	 * Analyze the log file, checking for nucommited transaction
	 * @param recoverAll	recover all uncommited transaction
	 * @throws IOException
	 */
	private static void showLog() throws IOException
	{
		System.out.println("### Transactions in " + logName);
		BufferedReader reader = new BufferedReader(new FileReader(logName));
		for (String line = reader.readLine(); line != null; line = reader.readLine())
		{
			String[] entry = line.split(":");

			if (entry[0].equals("start-transaction"))
			{
				System.out.println(line.substring(entry[0].length() + 1));
			}
		}
		reader.close();
	}

	/**
	 * Analyze the log file, checking for nucommited transaction
	 * @param recoverAll	recover all uncommited transaction
	 * @throws IOException
	 */
	private static void analyzeLog(boolean recoverAll) throws IOException
	{
		ArrayList<String> uncommited = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(logName));
		String curId = "";
		for (String line = reader.readLine(); line != null; line = reader.readLine())
		{
			String[] entry = line.split(":");

			if (entry[0].equals("start-transaction"))
			{
				if (!curId.equals(""))
					uncommited.add(curId);
				curId = entry[1];
			}
			
			if (entry[0].equals("commit") || entry[0].equals("rollback"))
			{
				if (uncommited.contains(entry[1]))
					uncommited.remove(entry[1]);
				
				curId = "";
			}
		}
		if (!curId.equals(""))
			uncommited.add(curId);

		for(int i = uncommited.size() - 1; i >= 0; i--)
		{
			System.out.println("Uncommited transaction: " + uncommited.get(i));
			if (recoverAll)
				recover(uncommited.get(i), true);
		}
		
		if (uncommited.size() == 0)
			System.out.println("No uncommited transactions");
		
		reader.close();
	}
	
	/**
	 * Recover a specified transaction
	 * @param txid
	 * @throws IOException
	 */
	private static void recover(String txid, boolean rollback) throws IOException
	{
		if (txid.equals(""))
			throw new NumberFormatException();
		System.out.println("### Recovering transaction " + txid + ", rollback: " + rollback);
		BufferedReader reader = new BufferedReader(new FileReader(logName));
		String line;
		boolean found = false;
		for (line = reader.readLine(); line != null; line = reader.readLine())
		{
			String[] entry = line.split(":");

			if (entry[0].equals("start-transaction") && entry[1].equalsIgnoreCase(new String(txid)))
			{	
				found = true;
				while((line = reader.readLine()) != null)
				{
					entry = line.split(":");
					if (!entry[0].equals("chg"))
						break;
					if (rollback)
						update(Integer.parseInt(entry[1]), Integer.parseInt(entry[2]));
					else
						update(Integer.parseInt(entry[1]), Integer.parseInt(entry[3]));
				}
				reader.close();
				log("rollback:" + txid);
				break;
			}
		}
		reader.close();
		if (!found)
			System.out.println("Transaction not found!");
		else
			System.out.println("Done!");
	}
}