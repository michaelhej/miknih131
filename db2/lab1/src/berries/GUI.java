package berries;/**
 * Created by michael on 28/05/16.
 */

import db.Berry;
import db.BerryFetcher;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class GUI extends Application {

    private Stage primaryStage;
    Double longMin, longMax, latMin, latMax;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        AnchorPane pane = null;
        try
        {
            pane = FXMLLoader.load(getClass().getResource("MainGui.fxml"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Scene scene = new Scene(pane, 800, 600);

        //Show scene
        primaryStage.setScene(scene);
        primaryStage.show();
        mapSlides();
        setupDataTable();
    }

    private void updateValues() {
        longMin = parseValue("sliderLongMin");
        longMax = parseValue("sliderLongMax");
        latMin = parseValue("sliderLatMin");
        latMax = parseValue("sliderLatMax");

        Label labelLong = getItem("labelLong");
        Label labelLat = getItem("labelLat");

        labelLong.setText(longMin + " - " + longMax);
        labelLat.setText(latMin + " - " + latMax);
    }

    private Double parseValue(String itemId) {
        return new BigDecimal(((Slider)getItem(itemId)).getValue())
                .setScale(3, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }

    private void mapSlides() {

        String[] sliderItems = { "sliderLongMin", "sliderLongMax", "sliderLatMin", "sliderLatMax" };
        for (int i = 0; i < sliderItems.length; i++) {
            Slider slider = getItem(sliderItems[i]);
            slider.valueProperty().addListener(event -> {
                updateValues();
            });
        }

        Button btnShow = getItem("btnShow");
        btnShow.setOnAction(event -> {
                updateValues();
                fetchValues();
            });
    }

    private void fetchValues() {
        BerryFetcher fetcher = new BerryFetcher();
        fetcher.connect();
        List<Berry> berries = fetcher.getBerries(longMin, longMax, latMin, latMax);
        fetcher.close();

        TableView<Berry> tblBerries = getItem("tableBerries");
        ObservableList<Berry> data = FXCollections.observableArrayList(berries);
        tblBerries.setItems(data);
    }

    private void setupDataTable() {
        TableView tblBerries = getItem("tableBerries");

        TableColumn numberCol = new TableColumn("Nummer");
        numberCol.setCellValueFactory(
                new PropertyValueFactory<Berry, String>("number"));

        TableColumn longCol = new TableColumn("Longitud");
        longCol.setCellValueFactory(
                new PropertyValueFactory<Berry, String>("longitud"));

        TableColumn latCol = new TableColumn("Latitud");
        latCol.setCellValueFactory(
                new PropertyValueFactory<Berry, String>("latitud"));

        tblBerries.getColumns().addAll(numberCol, longCol, latCol);
    }

    private <T> T getItem(String itemId) {
        if (primaryStage.getScene().lookup("#" + itemId) == null) {
            System.out.println("Count not find: " + itemId);
            return null;
        }

        return (T)primaryStage.getScene().lookup("#" + itemId);
    }
}
