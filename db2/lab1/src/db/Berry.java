package db;

/**
 * Created by michael on 28/05/16.
 */
public class Berry {
    private String number;
    private String longitud;
    private String latitud;

    public Berry(String number, String longitud, String latitud) {
        this.number = number;
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public String getNumber() {
        return number;
    }

    public String getLongitud() {
        return longitud;
    }

    public String getLatitud() {
        return latitud;
    }
}
