package db;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BerryFetcher
{
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://piq.nu/db_teamb";
    private static final String USERNAME = "teamb";
    private static final String PASSWORD = "teambteamb";

	private Connection conn;

	/**
	 * Connect to the database server
	 * @return
	 */
	public Boolean connect()
	{
		System.out.print("Connecting to database... ");
		try
		{
			Class.forName(DRIVER);
		}
		catch (java.lang.ClassNotFoundException cnf)
		{
			System.out.println("JDBC driver not found: " + DRIVER);
			return false;
		}

		 try
		 {
			 conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		 }
		 catch (Exception ex)
		 {
			 System.out.println("Could not connect to database");
			 return false;
		 }
		 System.out.println("Connected!");
		 return true;
	}

    //Fetch berries from database
	public List<Berry> getBerries(Double longMin, Double longMax, Double latMin, Double latMax)
	{
        try {
            System.out.print("Fetching berries... ");
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement("SELECT * FROM Blabar JOIN Buskar ON Buskar.nummer = Blabar.buske WHERE longitud > (?) AND longitud < (?) AND latitud > (?) AND latitud < (?)");

            //Set geographic parameters
            stmt.setDouble(1, longMin);
            stmt.setDouble(2, longMax);
            stmt.setDouble(3, latMin);
            stmt.setDouble(4, latMax);
            ResultSet rs = stmt.executeQuery();
            List<Berry> berries = new ArrayList<>();

            //Add results to berry list
            while (rs.next()) {
                Berry berry = new Berry(
                        rs.getString("nummer"),
                        rs.getString("longitud"),
                        rs.getString("latitud"));
                berries.add(berry);
            }
            stmt.close();
            System.out.println("ok!");
            return berries;
        } catch (Exception ex) {
            System.out.println("fail!");
            ex.printStackTrace();
        }
        return null;
	}

    public void close() {
        try {
            System.out.print("Disconnecting... ");
            conn.close();
            System.out.println("ok!");
        } catch (Exception ex) {
            System.out.println("fail!");
            /* Ignore */
        }
    }

}