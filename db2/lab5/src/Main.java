import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main
{
	private static  Connection con;
	
	/**
	 * Main entry point
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		if (!connect())
			return;
		printOptions();
		
		String[] cmd;
		loop: while(true)
		{
			cmd = input("jdbc> ");
			switch (cmd[0])
			{
				case "list":
					listAll();
					break;
				case "find": case "fin":
					search(cmd);
					break;
				case "salary":
					updateSalary(cmd);
					break;
				case "add":
					addEmployee();
					break;
				case "rm":
					rmEmployee(cmd);
					break;
				case "?":
					printOptions();
					break;
				case "q": case "exit":
					break loop;
				default:
					System.out.println("Invalid command " + cmd[0]);
			}
			System.out.println("");
		}
		System.out.println("bye!");
		con.close();
	}
	
	/**
	 * Print all available options
	 */
	private static void printOptions()
	{
		System.out.println("=== JDBC ===");
		System.out.println("find <name> \t\t\t Search for employees");
		System.out.println("add \t\t\t\t Add a new employee");
		System.out.println("rm <id> \t\t\t Remove employee by id");
		System.out.println("salary id <id> <salary> \t Update salary for employee by id");
		System.out.println("salary name <name> <salary> \t Update salary for employee by name");
		System.out.println("list \t\t\t\t List all employees");
	}
	
	/**
	 * Connect to the database server
	 * @return
	 */
	private static Boolean connect()
	{
		System.out.print("Connecting to database... ");
		try
		{
			Class.forName("com.mimer.jdbc.Driver");
		}
		catch (java.lang.ClassNotFoundException cnf)
		{
			System.out.println("JDBC driver not found");
			return false;
		}
		
		 String url = "jdbc:mimer://dbtek114:kanta@basen.oru.se/dbk";
		 try
		 {
			 con = DriverManager.getConnection(url);
		 }
		 catch (Exception ex)
		 {
			 System.out.println("Could not connect to database");
			 return false;
		 }
		 System.out.println("Connected!");
		 return true;
	}
	
	/**
	 * Return a string input from the user
	 * @param prompt
	 * @return
	 * @throws Exception
	 */
	private static String[] input(String prompt) throws Exception
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(prompt);
        String line = br.readLine();
        String cmd[] = line.split(" ");
        return cmd;
	}

	/**
	 * Remove an employee from the database
	 * @param cmd
	 * @throws SQLException
	 */
	private static void rmEmployee(String[] cmd) throws SQLException
	{
		if (cmd.length < 2)
		{
			System.out.println("Syntax error!");
			return;
		}
		
		PreparedStatement stmt = null;
		if (isNumeric(cmd[1]))
		{
			stmt = con.prepareStatement("DELETE FROM employee WHERE number = ?");
		}
		else
		{
			System.out.println("Syntax error!");
			return;
		}
		

		if (isManager(cmd[1]))
		{
			System.out.println("Employee is a manager and cannot be removed!");
			return;
		}
		
	    stmt.setString(1, cmd[1]);
	    int inserts = stmt.executeUpdate();
	    if (inserts > 0)
	    	System.out.println("Employee removed!");
	    else
	    	System.out.println("No employee with id " + cmd[1]);
	    
	    stmt.close();
	}
	/**
	 * Add a new employee to the database
	 * @param cmd
	 * @throws Exception
	 */
	private static void addEmployee() throws Exception
	{
		PreparedStatement stmt = null;
		
		String name;
		String salary;
		String manager;

		name = input("Name: ")[0];
		
		salary = input("Salary: ")[0];
		while (!(isNumeric(salary)))
		{
			System.out.println("Salary must be a number");
			salary = input("Salary: ")[0];
		}

		manager = input("Manager id: ")[0];
		while (!(isNumeric(manager)) || getName(manager).equals("N/A"))
		{
			if (!(isNumeric(manager)))
				System.out.println("Manager must be a number");
			else
				System.out.println("Manager ID not valid.");
			
			manager = input("New manager ID or (a)bort: ")[0];
			if (manager.equals("a"))
				return;
		}	
		
		stmt = con.prepareStatement("INSERT INTO employee(name, salary, manager) values(?, ?, ?)");
	    stmt.setString(1, name);
	    stmt.setInt(2, Integer.parseInt(salary));
	    stmt.setInt(3, Integer.parseInt(manager));
	    int inserts = stmt.executeUpdate();
	    if (inserts > 0)
	    	System.out.println(name + " added!");
	    else
	    	System.out.println("Failed to add " + name);
	    
	    stmt.close();
		
	}
	
	/**
	 * Update salary for an employee
	 * @param cmd
	 * @throws SQLException
	 */
	private static void updateSalary(String[] cmd) throws SQLException
	{
		if (cmd.length < 4)
		{
			System.out.println("Syntax error!");
			return;
		}
		
		PreparedStatement stmt = null;
		if (cmd[1].equals("name"))
			stmt = con.prepareStatement("UPDATE employee SET salary = ? WHERE lower(name) = lower(?)");
		else if (cmd[1].equals("id"))
			stmt = con.prepareStatement("UPDATE employee SET salary = ? WHERE number = ?");
		else
		{
			System.out.println("Syntax error!");
			return;
		}
		try
		{
			stmt.setString(1, cmd[3]);
			stmt.setString(2, cmd[2]);
		}
		catch (Exception ex)
		{
			System.out.println("Syntax error!");
			return;
		}
	    int updated = stmt.executeUpdate();
	    System.out.println("Salary updated for " + updated + " employees");
		stmt.close();
	}
	
	/**
	 * Search for an employee
	 * @param cmd
	 * @throws SQLException
	 */
	private static void search(String[] cmd) throws SQLException
	{
		if (cmd.length < 2)
		{
			System.out.println("Syntax error!");
			return;
		}
		
		System.out.println("Searching for employee matching " + cmd[1]);
		PreparedStatement stmt = null;
	    stmt = con.prepareStatement("SELECT * FROM employee WHERE lower(name) LIKE lower(?)");
	    stmt.setString(1, "%" + cmd[1] + "%");
	    ResultSet rs = stmt.executeQuery();
		while (rs.next())
		{
			showProfile(rs);
	    }
		stmt.close();
	}
	
	/**
	 * 
	 * @throws SQLException
	 */
	private static void listAll() throws SQLException
	{
	    Statement stmt = null;
	    try
	    {
	        stmt = con.createStatement();
	        ResultSet rs = stmt.executeQuery("SELECT * FROM employee");
	        while (rs.next())
	        {
				showProfile(rs);
	        }
	    }
	    catch (SQLException e)
	    {
	        e.printStackTrace();
	    }
	    finally
	    {
	        if (stmt != null)
	        {
	        	stmt.close();
	        }
	    }
	}
	
	/**
	 * Show a profile for an employee
	 * @param rs
	 */
	private static void showProfile(ResultSet rs)
	{
		try
		{
			System.out.println("");
			System.out.println("======= " + rs.getString("name") + " =======");
			System.out.println("Number: " + rs.getString("number"));
			System.out.println("Salary: " + rs.getString("salary"));
			System.out.println("Manager [ID]: " + getName(rs.getString("manager")) + " [" + rs.getString("manager") + "]");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	private static String getName(String id) throws SQLException
	{
		String name = "N/A";
		Statement stmt = null;

        stmt = con.createStatement();
		try
		{
	        ResultSet rs = stmt.executeQuery("SELECT name FROM employee WHERE number = " + id);
	        rs.next();
	        name = rs.getString("name");
		}
		catch (Exception ex)
		{
			/* Ignore */
		}
        stmt.close();
        return name;
	}
	
	/**
	 * Checks if a employee is a manager for any other employees
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	private static boolean isManager(String id) throws SQLException
	{
		Statement stmt = null;
		int count = 0;
        stmt = con.createStatement();
		try
		{
	        ResultSet rs = stmt.executeQuery("SELECT count(*) AS cnt FROM employee WHERE manager = " + id);
	        rs.next();
	        count = rs.getInt("cnt");
		}
		catch (Exception ex)
		{
			/* Ignore */
		}
        stmt.close();
        return (count > 0);
	}
	
	/**
	 * Check if a string contains only numbers
	 * @param str
	 * @return
	 */
	private static boolean isNumeric(String str)  
	{  
		try  
		{  
			double d = Integer.parseInt(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}
}