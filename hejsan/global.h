#include <stdio.h> 
#include <cstdlib>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <vector>
#include <map>
#include "type.h"
#include "value.h"
#include "colors.h"
#include "parser.tab.hpp"


extern std::vector<Symbol> symbols;
extern void Error(char* msg, int errLine = 0);