namespace NodeType {
    enum {
        Stop = 1,
        While,
        Statement,
        Print,
        Read,
        Assignment,
        Condition,
        Declaration,
        Color,
        Int,
        String,
        Variable,
        GetKey,
        Coord,
        Random,
        Clear
    };
};