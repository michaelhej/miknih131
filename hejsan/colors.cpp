#include "global.h"
#include <string>
std::map <std::string, char*> colors;

void InitColors()
{
	colors["gul"] = "\033[38;5;226m";
	colors["röd"] = "\033[38;5;196m";
	colors["grön"] = "\033[38;5;46m";
	colors["orange"] = "\033[38;5;212m";
	colors["blå"] = "\033[38;5;21m";
	colors["svart"] = "\033[38;5;232m";
	colors["grå"] = "\033[38;5;246m";
	colors["lila"] = "\033[38;5;129m";
	colors["vit"] = "\033[00m";
}

char* ColorCode(char* color)
{
	std::string colorString = std::string(color);

	if(colors[colorString] != NULL)
	    return colors[colorString];
	
    return "\033[00m"; //White
}