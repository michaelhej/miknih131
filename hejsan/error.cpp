/* error.c */

#include "global.h"

int lineNo = 1;
void Error(char* msg, int errLine)
{
	fprintf(stderr, "line %d: %s\n", errLine, msg);
	exit(EXIT_FAILURE);
}
