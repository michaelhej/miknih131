%{
#include <stdio.h>
#include "global.h"
#include "colors.h"
extern int lineNo;
extern FILE* yyin;
extern char* outFile;
%}

%option noyywrap
%option nounput

%%
[ \t]+    					{	/* Ignore */	}
"§"[^\r\n]*					{	/* Comment */	}
[\n]						{	lineNo += 1;	}

"Slut!!!"					{	return DONE;	}

	/* Data types */
"är ett tal"				{	return DEFINT;	}
"är en text"				{	return DEFSTR;	}

	/* Statements */
"Om"						{	return IF;		}
"Medan"						{	return WHILE;	}
"Annars"					{	return ELSE;	}
"Skriv ut"					{	return PRINT;	}
"Läs in"					{	return READ;	}
"Sätt färg till" 			{	return COLOR;	}
"knapptryck" 				{	return GETKEY;	}
"Gå till" 					{	return COORD;	}
"Rensa"						{	return CLEAR;	}
"slumptal"					{	return RANDOM;	}
"gör"						{	return '{';		}
"då"						{	return '{';		}
"så"						{	return '{';		}
"sluta"						{	return '}';		}
"stop"						{	return '}';		}


"!"							{	return ';';		}
"="							{	return '=';		}
"som blir"					{	return '=';		}
"är"						{	return EQUALS;	}
"är lika med"				{	return EQUALS;	}
"Sätt"						{	return LET;		}
"till"						{	return BE;		}

	/* Numbers */
[0-9]+   					{   yylval.value = atoi(yytext);
								return NUM; 	}
"sant"						{	yylval.value = 1;
								return NUM;	}
"falskt"					{	yylval.value = 0;
								return NUM;	}
"neråtpil"|"nedåtpil"		{	yylval.value = 66;
								return NUM;	}
"uppåtpil"					{	yylval.value = 65;
								return NUM;	}
"högerpil"					{	yylval.value = 67;
								return NUM;	}
"vänsterpil"				{	yylval.value = 68;
								return NUM;	}
	/* Variables */
([a-z|å|ä|ö]|[A-Z]|Å|Ä|Ö)+([a-z|0-9|å|ä|ö]|[A-Z]|Å|Ä|Ö)* 	{
							 	yylval.string = strdup(yytext);
								return ID;		}

	/* Operators */
[\+\-\*/%\^&\|<>]			{	return *yytext; }

	/* Token symbols */
[\(\)\?"{}:]				{	return *yytext; }

	/* Strings */
\"([^\\\"]|\\.)*\"			{ 	yylval.string = (char*)calloc(strlen(yytext)-1, sizeof(char));
								strncpy(yylval.string, &yytext[1], strlen(yytext)-2);
								return STRING;	}
"ny rad" 					{	yylval.string = "\n";
								return STRING;	}
%%

//Arguments are -i <input> -o <output>
//Defaults to prompt
int main(int argc, char** argv)
{
	srand (time(NULL));
	char* inFile;
	for (int i = 0; i < argc; i++)
	{
		if (strcmp(argv[i], "-i") == 0)
			inFile = argv[i+1];
		if (strcmp(argv[i], "-o") == 0)
			outFile = argv[i+1];
	}

	//Single argument. Assume filename.
	if (inFile == NULL && argc == 2)
	{
		inFile = argv[1];
	}

	if (inFile != NULL)
	{
		yyin = fopen(inFile, "r");
		if(!yyin)
		{
			fprintf(stderr, "Can't open file\n");
			return 1;
		}
	}

    InitColors();
    yyparse();
    exit(0);
}