TreeNode* MakeLeaf(int type, Value value);
TreeNode* MakeAssignNode(char* name, TreeNode* assignNode);
TreeNode* MakeNode(int type, TreeNode* arg0 = 0, TreeNode* arg1 = 0, TreeNode* arg2 = 0);
TreeNode* CopyNode(TreeNode* sourceNode);