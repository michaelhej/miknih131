#include <cmath>
#include <iostream>
#include <cstring>

#include "global.h"
#include "tree.h"
#include "symbol.h"
#include "getch.cpp"

extern void Translate(TreeNode* p);
int GetNodeType(TreeNode* p);

void SetColor(char* color)
{
	printf("%s", ColorCode(color));
}

//Interpret syntax tree executing nodes
Value Interpret(TreeNode* p)
{
	if (p == 0)
		return Value(0);

	//Concat strings with +
	if (p->type == '+' && GetNodeType(p->args[0]) == NodeType::String)
	{
		if (GetNodeType(p->args[1]) == NodeType::String)
			return Value(strcat(Interpret(p->args[0]).str, Interpret(p->args[1]).str));
		else
			Error("Mismatching data types for +", p->lineNo);
	}

	//Compare strings with =
	if (p->type == '=' && GetNodeType(p->args[0]) == NodeType::String)
	{
		if (GetNodeType(p->args[1]) == NodeType::String)
			return Value(strcmp(Interpret(p->args[0]).str, Interpret(p->args[1]).str) == 0);
		else
			Error("Mismatching data types for +", p->lineNo);
	}

	//Arithmetic types
	switch (p->type)
	{
		case '+':
			return Value(Interpret(p->args[0]).integer + Interpret(p->args[1]).integer);
		case '-':
			return Value(Interpret(p->args[0]).integer - Interpret(p->args[1]).integer);
		case '*':
			return Value(Interpret(p->args[0]).integer * Interpret(p->args[1]).integer);
		case '/':
			return Value(Interpret(p->args[0]).integer / Interpret(p->args[1]).integer);
		case '%':
			return Value(Interpret(p->args[0]).integer % Interpret(p->args[1]).integer);
		case '^':
			return Value(pow(Interpret(p->args[0]).integer, Interpret(p->args[1]).integer));
		case '&':
			return Value(Interpret(p->args[0]).integer & Interpret(p->args[1]).integer);
		case '|':
			return Value(Interpret(p->args[0]).integer | Interpret(p->args[1]).integer);
		case '>':
			return Value(Interpret(p->args[0]).integer > Interpret(p->args[1]).integer);
		case '<':
			return Value(Interpret(p->args[0]).integer < Interpret(p->args[1]).integer);
		case '=':
			return Value(Interpret(p->args[0]).integer == Interpret(p->args[1]).integer);
	}

	//Variable declaration
	if (p->type == NodeType::Declaration)
	{	
    	DeclareSymbol(p->args[0]->leafValue.str, p->args[0]->type);
    	Interpret(p->args[1]); //Optional assignment statement
    	return 0;
	}
	
	//Assign variable with node argument
	if (p->type == NodeType::Assignment)
	{
		SaveSymbol(p->leafValue, Interpret(p->args[0]));
		return 0;
	}

	//Change color
	if (p->type == NodeType::Color)
	{
		SetColor(Interpret(p->args[0]).str);
		return 0;
	}

	//Read to variable from cin
	if (p->type == NodeType::Read)
	{
		int tmpInt;
		char *tmpStr;

		std::string line;
		std::getline(std::cin, line);

		if (GetSymbol(p->leafValue)->type == NodeType::Int)
		{
			tmpInt = std::atoi(line.c_str());
			GetSymbol(p->leafValue)->value = Value(tmpInt);
		}
		else if (GetSymbol(p->leafValue)->type == NodeType::String)
		{
			tmpStr = new char[line.length() + 1];
			std::strcpy(tmpStr, line.c_str());
			GetSymbol(p->leafValue)->value = Value(tmpStr);
		}
		else
			Error("No matching data type for input", p->lineNo);

		return 1;
	}

	//Print node arguments
	if (p->type == NodeType::Print)
	{
		if (GetNodeType(p->args[0]) == NodeType::Int)
			printf("%d", Interpret(p->args[0]).integer);
		else if (GetNodeType(p->args[0]) == NodeType::String)
			printf("%s", Interpret(p->args[0]).str);
		else //No type, probably wierd print
			printf("%d", GetNodeType(p->args[0]));
		return 0;
	}

	//While loop
	if (p->type == NodeType::While)
	{
		while (Interpret(p->args[0]).integer)
		{
			Interpret(p->args[1]);
		}
		return 0;
	}

	//If statement
	if (p->type == NodeType::Condition)
	{
		if (Interpret(p->args[0]).integer)
		{
			Interpret(p->args[1]);
		}
		else
		{
			Interpret(p->args[2]);
		}
		return 0;
	}

	//Get key press
	if (p->type == NodeType::GetKey)
	{
		int t = getch();
		if (t == '\033') //Indicates arrow key
		{
			getch(); //Skip to get key code
			return Value(getch());
		}
		return Value(t);
	}

	//Random
	if (p->type == NodeType::Random)
	{
		return Value(rand());
	}

	//Move cursor to coord
	if (p->type == NodeType::Coord)
	{
		printf("\x1b[%d;%df", Interpret(p->args[1]).integer, Interpret(p->args[0]).integer);
		return 0;
	}

	//Clear screen
	if (p->type == NodeType::Clear)
	{
		printf("\033[2J");
		return 0;
	}

	//Return variable value from symbol list
	if (p->type == NodeType::Variable)
	{
		return GetSymbol(p->leafValue)->value;
	}

	//Data types, return leaf value
	if (p->type == NodeType::Int || p->type == NodeType::String)
	{
		return p->leafValue;
	}

	//Multiple statements, interpret node arguments
	if (p->type == NodeType::Statement)
	{
		return Interpret(p->args[2]) + Interpret(p->args[1]) + Interpret(p->args[0]);
	}

	return Value(0);
}

int GetNodeType(TreeNode* p)
{
	if (p == 0)
		return 0;

	switch (p->type) 
	{
		case NodeType::GetKey: case NodeType::Random:
			return NodeType::Int;
		case NodeType::Int: case NodeType::String:
			return p->type;
		case NodeType::Variable:
			return GetSymbol(p->leafValue)->type;
		default:
			return GetNodeType(p->args[0]) | GetNodeType(p->args[1]) | GetNodeType(p->args[2]);

	}
}