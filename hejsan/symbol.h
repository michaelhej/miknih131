int GetSymbolPlace(char* name);
int DeclareSymbol(char* name, int type);
int SaveSymbol(Value symName, Value value);
Symbol* GetSymbol(Value symName);
Value GetSymbolValue(char* name);