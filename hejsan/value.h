union Value
{
    int integer;
    char *str;

    //Value constructors
    Value(){}
    Value(int integer_) : integer(integer_) { }
    Value(char* str_) : str(str_) { }

    Value& operator+(const Value& other)
    {
        integer += other.integer;
        return *this;
    }
};

class Symbol
{
public:
    char *name;
    int id;
    int type;
    Value value;
};

class TreeNode
{
public:

    enum TreeNodeType { String };
    
    int type;
    Value leafValue;
    TreeNode* args[3];

    int lineNo;
};