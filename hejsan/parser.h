extern int yyerror(char const *msg);
extern int yylex();
extern int lineNo;

extern void Execute(TreeNode *p);
extern void TranslateTree(TreeNode *p);
extern int Interpret(TreeNode *p);

extern char* outFile;