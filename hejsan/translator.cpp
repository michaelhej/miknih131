/*
Quick test fiddeling with translating into c++ code for further compiling
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "global.h"
#include "symbol.h"

void Translate(TreeNode* p);

char* outFile;
std::ostringstream out;
std::map <char*, std::string> variables;

//Retrun a short variable name for a node variable (bypassing naming conflicts)
std::string ShortVar(TreeNode* p)
{
	std::stringstream ss;
	char* varName = p->leafValue.str;
	if (GetSymbolPlace(varName) == -1)
		ss << "v" << DeclareSymbol(varName, 0);
	else
		ss << "v" << GetSymbolPlace(varName);
	return ss.str();
}

//Output minimized version of custom getch-function (to guarantee same arrow values)
void MakeFunctionGetch()
{
	out << "#include <termios.h>" << std::endl;
	out << "#include <unistd.h>" << std::endl;
	out << "int getch(void) {struct termios oldattr, newattr;int ch;";
	out << "tcgetattr( STDIN_FILENO, &oldattr );newattr = oldattr;";
	out << "newattr.c_lflag &= ~( ICANON | ECHO );tcsetattr( STDIN_FILENO, TCSANOW, &newattr );";
	out << "ch = getchar();tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );return ch;	}";
}

//Outputs syntac tree to a to be compiles with c++
void TranslateTree(TreeNode* p)
{
	out << "#include <iostream>" << std::endl;
	out << "#include <sstream>" << std::endl;
	out << "#include <string>" << std::endl;
	out << "#include <stdio.h>" << std::endl;
	out << "#include <stdlib.h>" << std::endl;
	out << "#include <time.h>" << std::endl;

	MakeFunctionGetch();
	out << "int main() {";
	out << "srand (time(NULL));";
	Translate(p);
	out << "}";

	printf("Printing to %s\n", outFile);
	std::ofstream fileStream;
	fileStream.open(outFile);
	fileStream << out.str();
	fileStream.close();
}

//Outputs C++ code for node
void Translate(TreeNode* p)
{
	if (p == 0)
		return;

	switch (p->type)
	{
		case '+': case '-': case '*': case '/': case '^': case '%': case '&': case '|': case '>': case '<':
			out << "(";
			Translate(p->args[0]);
			out << (char)p->type;
			Translate(p->args[1]);
			out << ")";
	}

	if (p->type == '^')
	{
		out << "pow(";
		Translate(p->args[0]);
		out << ",";
		Translate(p->args[1]);
		out << ");";
	}

	if (p->type == '=')
	{
		out << "(";
		Translate(p->args[0]);
		out << "==";
		Translate(p->args[1]);
		out << ")";
	}

	if (p->type == NodeType::GetKey)
	{
		out << "getch()";
	}

	if (p->type == NodeType::Random)
	{
		out << "rand()";
	}

	if (p->type == NodeType::Declaration)
	{
		if (p->args[0]->type == NodeType::Int)
			out << "int ";
		else if (p->args[0]->type == NodeType::String)
			out << "std::string ";
		out << ShortVar(p->args[0]) << ";";

		//Optional (assignment
		Translate(p->args[1]);		
	}

	if (p->type == NodeType::Assignment)
	{
		out << ShortVar(p) << "=";
		Translate(p->args[0]);
		out << ";";
	}


	if (p->type == NodeType::Coord)
	{
		out << "printf(\"\\x1b[%d;%df\",";
			Translate(p->args[1]);
			out << ",";
			Translate(p->args[0]);
		out << ");";
	}

	if (p->type == NodeType::Clear)
	{
		out << "printf(\"\\033[2J\");";
	}

	if (p->type == NodeType::String)
	{
		if (strcmp(p->leafValue.str, "\n") == 0)
			out << "'\\n'";
		else
			out << '"' << p->leafValue.str << '"';
	}

	if (p->type == NodeType::Int)
	{
		out << p->leafValue.integer;
	}

	if (p->type == NodeType::Print)
	{
		//Meta!
		out << "std::cout<<";
		Translate(p->args[0]);
		out << ";";
	}

	if (p->type == NodeType::Variable)
	{
		out << ShortVar(p);
	}

	if (p->type == NodeType::Read)
	{
		out << "std::cin>>" << ShortVar(p) << ";";
	}

	if (p->type == NodeType::While)
	{
		out << "while(";
		Translate(p->args[0]);
		out << "){";
		Translate(p->args[1]);
		out << "}";
	}

	if (p->type == NodeType::Condition)
	{
		out << "if(";
		Translate(p->args[0]);
		out << "){";
		Translate(p->args[1]);
		out << "}else{";
		Translate(p->args[2]);
		out << "}";
	}

	if (p->type == NodeType::Statement)
	{
		Translate(p->args[0]);
		Translate(p->args[1]);
		Translate(p->args[2]);
	}
}