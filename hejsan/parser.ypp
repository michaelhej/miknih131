%code top {
  #include "global.h"
}

%{
#include <stdio.h>
#include <cmath>
#include "tree.h"
#include "parser.h"
%}

%union {
  int value;
  char* string;
  TreeNode* p;
}

%token NUM ID DONE IF ELSE PRINT READ WHILE STRING OPERATOR DEFINT DEFSTR LET BE COLOR EQUALS GETKEY COORD CLEAR RANDOM
%left IF ELSE WHILE
%left '='
%left EQUALS LET BE
%left '<' '>'
%left '+' '-'
%left '*' '/' '%' '&' '|'
%left '^'

%type <p> expr
%type <p> statement
%type <p> assignment
%type <p> block
%type <p> list
%type <p> operation
%type <p> declaration
%type <value> NUM
%type <string> ID
%type <string> STRING

%%
start       : list DONE               { Execute($1); }

list        : /* empty */             { $$ = MakeNode(NodeType::Stop); }
            | expr                    { $$ = $1;  }
            | expr ';' list           { $$ = MakeNode(NodeType::Statement, $1, $3); }
            | block list              { $$ = MakeNode(NodeType::Statement, $1, $2); }
            | statement list          { $$ = MakeNode(NodeType::Statement, $1, $2); }
            | ';' list                { $$ = $2; }
            | declaration list        { $$ = MakeNode(NodeType::Statement, $1, $2); }

block       : '{' list '}'            { $$ = $2; }

statement   : IF expr block           { $$ = MakeNode(NodeType::Condition, $2, $3); }
            | IF expr block
              ELSE block              { $$ = MakeNode(NodeType::Condition, $2, $3, $5); }
            | WHILE expr block        { $$ = MakeNode(NodeType::While, $2, $3); }
            | COLOR expr              { $$ = MakeNode(NodeType::Color, $2); }
            | PRINT expr              { $$ = MakeNode(NodeType::Print, $2); }
            | READ ID                 { $$ = MakeLeaf(NodeType::Read, $2); }
            | COORD expr expr         { $$ = MakeNode(NodeType::Coord, $2, $3); }
            | CLEAR                   { $$ = MakeLeaf(NodeType::Clear, 0); }

declaration : ID DEFINT ';'           { $$ = MakeNode(NodeType::Declaration, MakeLeaf(NodeType::Int, $1)); }
            | ID DEFINT '=' expr ';'  { $$ = MakeNode(NodeType::Declaration, MakeLeaf(NodeType::Int, $1), MakeAssignNode($1, $4)); }

declaration : ID DEFSTR ';'           { $$ = MakeNode(NodeType::Declaration, MakeLeaf(NodeType::String, $1)); }
            | ID DEFSTR '=' expr ';'  { $$ = MakeNode(NodeType::Declaration, MakeLeaf(NodeType::String, $1), MakeAssignNode($1, $4)); }

assignment  : ID '=' expr             { $$ = MakeAssignNode($1, $3); } 
            | LET ID BE expr          { $$ = MakeAssignNode($2, $4); }

expr        : '(' expr ')'            { $$ = $2; }
            | operation               { $$ = $1; }
            | NUM                     { $$ = MakeLeaf(NodeType::Int, Value($1));  }
            | ID                      { $$ = MakeLeaf(NodeType::Variable, Value($1)); }
            | assignment              { $$ = $1; }
            | STRING                  { $$ = MakeLeaf(NodeType::String, Value($1)); }
            | GETKEY                  { $$ = MakeLeaf(NodeType::GetKey, 0); }
            | RANDOM                  { $$ = MakeLeaf(NodeType::Random, 0); }
            
operation   : expr '+' expr           { $$ = MakeNode('+', $1, $3); }
            | expr '-' expr           { $$ = MakeNode('-', $1, $3); }
            | expr '*' expr           { $$ = MakeNode('*', $1, $3); }
            | expr '%' expr           { $$ = MakeNode('%', $1, $3); }
            | expr '^' expr           { $$ = MakeNode('^', $1, $3); }
            | expr '&' expr           { $$ = MakeNode('&', $1, $3); }
            | expr '|' expr           { $$ = MakeNode('|', $1, $3); }
            | expr '>' expr           { $$ = MakeNode('>', $1, $3); }
            | expr '<' expr           { $$ = MakeNode('<', $1, $3); }
            | expr EQUALS expr        { $$ = MakeNode('=', $1, $3); }

%%


int yyerror(char const *msg)
{
    printf("Line: %d Error: %s\n", lineNo, msg);
    return 0;
}

void Execute(TreeNode* p)
{   
    if (outFile == NULL)
        Interpret(p);
    else
        TranslateTree(p);
}
