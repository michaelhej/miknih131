#include "global.h"
extern int lineNo;

TreeNode* MakeLeaf(int type, Value value)
{
    TreeNode* node = new TreeNode();
    node->type = type;
    node->leafValue = value;
    node->lineNo = lineNo;
    return node;
}

TreeNode* MakeAssignNode(char* name, TreeNode* assignNode)
{
    TreeNode* node = new TreeNode();
    node->type = NodeType::Assignment;
    node->leafValue = Value(name);
    node->args[0] = assignNode;
    node->lineNo = lineNo;
    return node;
}

TreeNode* MakeNode(int type, TreeNode* arg0 = 0, TreeNode* arg1 = 0, TreeNode* arg2 = 0)
{
    TreeNode* node = new TreeNode();
    node->type = type;
    node->args[0] = arg0;
    node->args[1] = arg1;
    node->args[2] = arg2;
    node->lineNo = lineNo;
    return node;
}