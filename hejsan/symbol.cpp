/*
Symbol table for keepng variables
*/

#include "global.h"

std::vector<Symbol> symbols;

int GetSymbolPlace(char* name)
{
    for (uint i = 0; i < symbols.size(); i++)
    {
        if (strcmp(symbols[i].name, name) == 0)
        {
            return i;
        }
    }
    return -1;
}

int DeclareSymbol(char* name, int type)
{
    if (GetSymbolPlace(name) != -1)
    {
        //Error("Variable already declared!"); //Don't bother
        return -1;
    }
    Symbol symbol;
    symbol.name = name;
    symbol.type = type;
    symbols.push_back(symbol);
    
    return symbols.size() - 1;
}

int SaveSymbol(Value symName, Value value)
{
    for (uint i = 0; i < symbols.size(); i++)
    {
        if (strcmp(symbols[i].name, symName.str) == 0)
        {
            symbols[i].value = value;
            return i;
        }
    }

    Error(strcat("Variable not declared: ", symName.str));
    return -1;
}


Symbol* GetSymbol(Value symName)
{
    for (uint i = 0; i < symbols.size(); i++)
    {
        if (strcmp(symbols[i].name, symName.str) == 0)
        {
            return &symbols[i];
        }
    }
    return 0;
}

Value GetSymbolValue(char* name)
{
    for (uint i = 0; i < symbols.size(); i++)
    {
        if (strcmp(symbols[i].name, name) == 0)
        {
            return symbols[i].value;
        }
    }
    Error(strcat("Symbol not found: ", name));
    return Value(0);
}