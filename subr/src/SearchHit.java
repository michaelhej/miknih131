/**
 * Created by michael on 16/02/16.
 */
public class SearchHit
{
    public String name;
    public String url;
    public SubParser subParser;
}
