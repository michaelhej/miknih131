import java.util.ArrayList;

/**
 * Created by michael on 16/02/16.
 */
/*
public interface SubParser
{
    ArrayList<Subtitle> search(String query)    throws Exception;
    String downloadSub(Subtitle sub)            throws Exception;
}
*/

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import com.esotericsoftware.minlog.Log;

/**
 * Created by michael on 15/02/16.
 */
public class SubParser
{
    private final String siteUrl = "http://www.addic7ed.com/";
    private final String searchRegex = genRegexSearch();
    private final String downloadRegex = genRegexDownload();
    private final String versionRegex = genRegexVersion();
    private String referer;

    public ArrayList<Subtitle> search(String query) throws Exception
    {
        String searchUrl = "search.php?search=%s";
        searchUrl = searchUrl.replace("%s", query);
        searchUrl = searchUrl.replaceAll(" ", "%20");

        return parsePage(searchUrl);
    }

    protected ArrayList<Subtitle> parsePage(String url) throws Exception
    {
        ArrayList<Subtitle> subtitles = new ArrayList<>();
        return subtitles;
    }

    protected String downloadSub(Subtitle sub) throws Exception
    {
        return getSrt(sub.subUrl);//"apa";
    }

    /**
     * Parse a download page
     * @param source
     * @return  List of subtitles on the download page
     * @throws Exception
     */
    protected ArrayList<Subtitle> parseDownload(String source) throws Exception
    {
        ArrayList<Subtitle> subtitles = new ArrayList<>();
        String name = nameFromDownloadSource(source);

        Matcher m = Pattern.compile(downloadRegex, Pattern.DOTALL | Pattern.MULTILINE).matcher(source);

        while (m.find())
        {
            subtitles.addAll(parseVersion(name, m.group("version"), m.group(0)).stream().collect(Collectors.toList()));
        }
        return subtitles;
    }

    /**
     * Parse a version of a subtitle from a download page
     * @param version
     * @param source
     * @return  all subtitles for supplied
     */
    protected ArrayList<Subtitle> parseVersion(String name, String version, String source)
    {
        ArrayList<Subtitle> subtitles = new ArrayList<>();
        Matcher m = Pattern.compile(versionRegex, Pattern.DOTALL | Pattern.MULTILINE).matcher(source);

        while (m.find())
        {
            Subtitle subtitle = new Subtitle();
            subtitle.name = name;
            subtitle.version = version;
            subtitle.comment = m.group("comment").trim();
            subtitle.language = m.group("language").trim();
            subtitle.subUrl = m.group("href").trim();
            subtitle.subParser = this;

            subtitles.add(subtitle);
        }
        return subtitles;
    }

    protected ArrayList<SearchHit> parseSearch(String source) throws Exception
    {
        ArrayList<SearchHit> hits = new ArrayList<>();
        Matcher m = Pattern.compile(searchRegex).matcher(source);

        while (m.find())
        {
            SearchHit hit = new SearchHit();
            hit.name = m.group("name");
            hit.url = m.group("url");
            hit.subParser = this;
            hits.add(hit);
        }
        return hits;
    }

    protected String getPageType(String source)
    {
        return "Unknown";
    }

    protected String getSrt(String url) throws Exception
    {
        Log.debug("Downloading srt " + url);
        URL getURL = new URL(siteUrl + url);

        HttpURLConnection conn = (HttpURLConnection)getURL.openConnection();
        addRequestProperties(conn);
        conn.setRequestProperty("Referer", siteUrl + referer);
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");

        Scanner scanner = new Scanner(conn.getInputStream(), "UTF-8").useDelimiter("\\A");

        String output = scanner.hasNext() ? scanner.next() : "";
        return output;
    }

    /**
     * Get source code from URL
     * @param url
     * @param gzip
     * @return
     * @throws Exception
     */
    protected String getSource(String url, Boolean gzip) throws Exception
    {
        Log.debug("Downloading url " + url);
        URL getURL = new URL(siteUrl + url);

        HttpURLConnection conn = (HttpURLConnection)getURL.openConnection();
        addRequestProperties(conn);
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");

        Scanner scanner;
        InputStream response;
        if (gzip) {
            InputStream gzippedResponse = conn.getInputStream();
            response = new GZIPInputStream(gzippedResponse);
        }
        else
        {
            response = conn.getInputStream();
        }
        scanner = new Scanner(response, "UTF-8").useDelimiter("\\A");

        String output = scanner.hasNext() ? scanner.next() : "";
        return output;
    }

    /**
     * Add standard HTTP-request-headers
     * @param conn
     */
    protected void addRequestProperties(HttpURLConnection conn)
    {
        conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.8,sv;q=0.6");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cookie", "PHPSESSID=95v211jdbcv53uup6d1h57i140");
        conn.setRequestProperty("Upgrade-Insecure-Requests", "1");
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36");
    }

    protected String nameFromDownloadSource(String source)
    {
        Matcher m = Pattern.compile(genRegexSubName(), Pattern.DOTALL | Pattern.MULTILINE).matcher(source);
        if (m.find())
            return m.group("name").trim();

        Log.debug("No name found in download");
        return "";
    }

    protected String genRegexSubName()
    {
        return "";
    }

    protected String genRegexSearch()
    {
        return "";
    }

    protected String genRegexDownload()
    {
        return "";
    }

    protected String genRegexVersion()
    {
        return "";
    }
}
