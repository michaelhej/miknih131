import com.esotericsoftware.minlog.Log;

import java.net.HttpURLConnection;
import java.util.ArrayList;

/**
 * Created by michael on 15/02/16.
 */
public class Addic7ed extends SubParser
{
    private final String siteUrl = "http://www.addic7ed.com/";
    private final String searchRegex = genRegexSearch();
    private final String downloadRegex = genRegexDownload();
    private final String versionRegex = genRegexVersion();
    private String referer;

    public Addic7ed() {

    }

    public ArrayList<Subtitle> search(String query) throws Exception
    {
        String searchUrl = "search.php?search=%s";
        searchUrl = searchUrl.replace("%s", query);
        searchUrl = searchUrl.replaceAll(" ", "%20");

        return parsePage(searchUrl);
    }

    @Override
    protected ArrayList<Subtitle> parsePage(String url) throws Exception
    {
        referer = url;
        ArrayList<Subtitle> subtitles = new ArrayList<>();

        String source = getSource(url, true);

        String pageType = getPageType(source);
        Log.debug("Page type " + pageType);

        if (pageType.equals("Download"))
        {
            return parseDownload(source);
        }
        if (pageType.equals("Search"))
        {
            ArrayList<SearchHit> hits = parseSearch(source);
            Log.debug(hits.size() + " hits for " + url);
            System.out.println();
            if (hits.size() > 0)
                return parsePage(hits.get(0).url);
        }

        return subtitles;
    }

    public String downloadSub(Subtitle sub) throws Exception
    {
        return getSrt(sub.subUrl);
    }

    protected String getPageType(String source)
    {
        if (source.contains("<title>\n\t\t Download"))
            return "Download";
        else if (source.contains("<title>\nSearch"))
            return "Search";
        else
            return "Unknown";
    }

    @Override
    protected void addRequestProperties(HttpURLConnection conn)
    {
        super.addRequestProperties(conn);
        conn.setRequestProperty("Host", "www.addic7ed.com");
    }

    /**
     * Regex for retrieving the name from a download page
     * @return
     */
    protected String genRegexSubName()
    {
        String regex = "";
        regex += "<span class=\"titulo\">\n[ |\t]*(?<name>[\\w]?[^<]+?)<small>";
        return regex;
    }

    /**
     * Regex for retrieving url and name via a search
     * @return
     */
    protected String genRegexSearch()
    {
        String regex = "";
        regex += "</td><td><a href=\"";
        regex += "(?<url>.*?)";       //Stage url
        regex += "\" debug=\"[0-9]*\">";
        regex += "(?<name>[^<]*)";    //Match name
        regex += "</a></td></tr>";
        return regex;
    }

    /**
     * Regex for retrieving version name from a subtitle
     * @return
     */
    protected String genRegexDownload()
    {
        String regex = "";
        regex += ">Version (?<version>[^,<]*), ([0-9]|\\.)* MBs"; //Version, Size
        regex += "(.*?)";
        regex += "<!-- table header -->";
        return regex;
    }

    /**
     * Regex for retrieving info from a subtitle
     * @return
     */
    protected String genRegexVersion()
    {
        String regex = "";
        regex += "class=\"newsDate\"[^>]*>";
        regex += "(?<comment>[^<]*?)"; //Comment
        regex += "<img .*?";
        regex += "class=\"language\">(?<language>[^<]*)<a href="; //Language
        regex += ".*?";
        regex += "href=\"(?<href>[^\"]*?)\"><strong>(?:(?:Download)|(?:original))??</strong>"; //Sub href
        //regex += ".*?";
        return regex;
    }
}
