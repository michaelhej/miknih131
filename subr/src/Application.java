import javax.xml.bind.Element;
import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by michael on 22/03/16.
 */
@XmlRootElement
public class Application {

    private List<Element> keys;

    @XmlAnyElement
    public List<Element> getKeys() {
        return keys;
    }

    public void setKeys(List<Element> keys) {
        this.keys = keys;
    }

}