import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import com.esotericsoftware.minlog.Log;
/**
 * Created by michael on 15/02/16.
 */
public class Subr
{
    public static void main(String[] args) throws Exception
    {
        Log.set(2);
        //search("american crime story s01e07");

        //XManager.Read();
        XManager x = new XManager();
        //x.WriteB();
        x.ReadB();
        Log.debug("Done");
    }

    private static void search(String query) throws Exception
    {
        SubParser addic7ed = new Addic7ed();

        //SubParser subscene = new Subscene();

        //subscene.search(query);
        ArrayList<Subtitle> subs = addic7ed.search(query);

        int i = 0;
        for (Subtitle sub : subs)
        {
            System.out.println(i + " " + sub.name);
            System.out.println(sub.version);
            System.out.println(sub.language);
            if (!sub.comment.equals(""))
                System.out.println(sub.comment);
            System.out.println("-----");
            i++;

            if (i > 3)
                break;
        }

        //String id = input("Download index")[0];

        //subs.get(Integer.parseInt(id)).download();
    }

    private static String[] input(String prompt) throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(prompt);
        String line = br.readLine();
        String cmd[] = line.split(" ");
        return cmd;
    }
}

