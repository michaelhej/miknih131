
import com.esotericsoftware.minlog.Log;
import com.piq.XTest;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by michael on 18/03/16.
 */
public class XManager
{
    public static void Read() throws Exception
    {

        Document doc = getDoc( "/Users/michael/Documents/config.xml");
        Log.debug("Root: " + doc.getDocumentElement().getNodeName());

        NodeList nodes = doc.getElementsByTagName("valve");

        for (int i = 0; i < nodes.getLength(); i++)
        {
            Element element = (Element)nodes.item(i);
            System.out.println(element.getAttribute("name"));
        }

    }

    public static Document getDoc(String filename) throws Exception
    {
        File input = new File(filename);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(input);

        doc.getDocumentElement().normalize();
        return doc;
    }


    public void ReadB() throws Exception
    {

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document document = docBuilder.parse(new File("mega.zml"));


        NodeList pipes = document.getElementsByTagName("application").item(0).getChildNodes();
        for (int i = 0; i < pipes.getLength(); i++) {
            Node node = pipes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                System.out.println("===" + node.getNodeName() + "===");
                parsePipe(node);
            }
        }
    }

    public static void parsePipe(Node pipe) throws Exception
    {
        NodeList items = pipe.getChildNodes();
        for (int i = 0; i < items.getLength(); i++) {
            Node node = items.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element)node;
                System.out.print(node.getNodeName() + " > ");


                if (e.getAttribute("class") != null && e.getAttribute("class").equals("com.piq.XTest"))
                {
                    Element config = (Element)e.getElementsByTagName("config").item(0);
                    XTest x = instantiate(config, Class.forName(e.getAttribute("class")));

                    System.out.println("\nX info: " + x.info);
                    System.out.println("\nX sum: " + x.sum);
                    System.out.println("\nX enable: " + x.enabled);
                }
            }
        }
    }

    public static <T> T instantiate(Element e, Class c) throws Exception {
        Object instance = c.newInstance();
        Field fields[] = c.getFields();

        for (Field f : fields) {
            if (e.hasAttribute(f.getName())) {
                f.set(instance, parse(e, f));
                e.removeAttribute(f.getName());
            }
        }

        for (int i = 0; i < e.getAttributes().getLength(); i++)
            System.out.println("\nUnknown config: " + e.getAttributes().item(i));

        return (T)instance;
    }

    public static Object parse(Element e, Field f) throws Exception {
        Method methods[] = f.getType().getMethods();

        for (Method m : methods) {
            if (m.getName().toLowerCase().contains("parse") && m.getParameterCount() == 1) {
                return m.invoke(null, e.getAttribute(f.getName()));
            }
        }

        return e.getAttribute(f.getName());
    }


    public static void parseNode(Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName());

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                parseNode(currentNode);
            }
        }
    }
}
