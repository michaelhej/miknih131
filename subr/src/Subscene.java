//import java.io.*;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.Scanner;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.zip.GZIPInputStream;
//import com.esotericsoftware.minlog.Log;
//
///**
// * Created by michael on 15/02/16.
// */
//
//public class Subscene implements SubParser
//{
//    private static final String siteUrl = "http://subscene.com/";
//    private static final String searchRegex = genRegexSearch();
//    private static final String downloadRegex = genRegexDownload();
//    private static final String versionRegex = genRegexVersion();
//    private static String referer;
//
//    public Subscene() {
//
//    }
//
//    public ArrayList<Subtitle> search(String query) throws Exception
//    {
//        String searchUrl = "/subtitles/release?q=";
//        searchUrl = searchUrl.replace("%s", query);
//        searchUrl = searchUrl.replaceAll(" ", "%20");
//
//        return parsePage(searchUrl);
//    }
//
//    public ArrayList<Subtitle> parsePage(String url) throws Exception
//    {
//        referer = url;
//        ArrayList<Subtitle> subtitles = new ArrayList<>();
//
//        String source;// = getSource(url);
//
//        Scanner scanner = new Scanner(new FileInputStream("/Users/michael/Downloads/mr.html"), "UTF-8").useDelimiter("\\A");
//        source = scanner.hasNext() ? scanner.next() : "";
//
//        String pageType = getPageType(source);
//        Log.debug("Page type " + pageType);
//
//        if (pageType.equals("Download"))
//        {
//            return parseDownload(source);
//        }
//        if (pageType.equals("Search"))
//        {
//            ArrayList<SearchHit> hits = parseSearch(source);
//            Log.debug(hits.size() + " hits for " + url);
//            System.out.println();
//            if (hits.size() > 0)
//                return parsePage(hits.get(0).url);
//        }
//
//        return subtitles;
//    }
//
//    public String downloadSub(Subtitle sub) throws Exception
//    {
//        return getSrt(sub.subUrl);//"apa";
//    }
//
//    /**
//     * Parse a download page
//     * @param source
//     * @return  List of subtitles on the download page
//     * @throws Exception
//     */
//    private ArrayList<Subtitle> parseDownload(String source) throws Exception
//    {
//        ArrayList<Subtitle> subtitles = new ArrayList<>();
//        Matcher m = Pattern.compile(downloadRegex, Pattern.DOTALL | Pattern.MULTILINE).matcher(source);
//
//        while (m.find())
//        {
//            for (Subtitle sub : parseVersion(m.group(1), m.group(0)))
//                subtitles.add(sub);
//        }
//        return subtitles;
//    }
//
//    /**
//     * Parse a version of a subtitle from a download page
//     * @param version
//     * @param source
//     * @return  all subtitles for supplied
//     */
//    private ArrayList<Subtitle> parseVersion(String version, String source)
//    {
//        ArrayList<Subtitle> subtitles = new ArrayList<>();
//        Matcher m = Pattern.compile(versionRegex, Pattern.DOTALL | Pattern.MULTILINE).matcher(source);
//
//        while (m.find())
//        {
//            Subtitle subtitle = new Subtitle();
//            subtitle.name = version;
//            subtitle.comment = m.group("comment").trim();
//            subtitle.language = m.group("language").trim();
//            subtitle.subUrl = m.group("href").trim();
//            subtitle.subParser = this;
//
//            subtitles.add(subtitle);
//
//            //for (int i = 1; i < m.groupCount() + 1; i++)
//            //    System.out.println(m.group(i));
//
//        }
//        return subtitles;
//    }
//
//
//    private ArrayList<SearchHit> parseSearch(String source) throws Exception
//    {
//        ArrayList<SearchHit> hits = new ArrayList<>();
//        Matcher m = Pattern.compile(searchRegex).matcher(source);
//
//        while (m.find())
//        {
//            SearchHit hit = new SearchHit();
//            hit.name = m.group("name");
//            hit.url = m.group("url");
//            hit.subParser = this;
//            hits.add(hit);
//            Log.info(hit.name + " - " + hit.url);
//        }
//        return hits;
//    }
//
//    private static String getPageType(String source)
//    {
//        if (source.contains("<title>Subscene - Subtitles for"))
//            return "Download";
//        else if (source.contains("<div class=\"search-result\">"))
//            return "Search";
//        else
//            return "Unknown";
//    }
//
//    private static String getSrt(String url) throws Exception
//    {
//        Log.debug("Downloading srt " + url);
//        URL getURL = new URL(siteUrl + url);
//
//        HttpURLConnection conn = (HttpURLConnection)getURL.openConnection();
//        addRequestProperties(conn);
//        conn.setRequestProperty("Referer", siteUrl + referer);
//        conn.setUseCaches(false);
//        conn.setDoInput(true);
//        conn.setDoOutput(true);
//        conn.setRequestMethod("GET");
//
//        Scanner scanner = new Scanner(conn.getInputStream(), "UTF-8").useDelimiter("\\A");
//
//        String output = scanner.hasNext() ? scanner.next() : "";
//        return output;
//    }
//
//    private static String getSource(String url) throws Exception
//    {
//        Log.debug("Downloading url " + url);
//        URL getURL = new URL(siteUrl + url);
//
//        HttpURLConnection conn = (HttpURLConnection)getURL.openConnection();
//        addRequestProperties(conn);
//        conn.setUseCaches(false);
//        conn.setDoInput(true);
//        conn.setDoOutput(true);
//
//        conn.setRequestMethod("GET");
//
//        InputStream gzippedResponse = conn.getInputStream();
//        InputStream ungzippedResponse = new GZIPInputStream(gzippedResponse);
//
//        Scanner scanner = new Scanner(ungzippedResponse, "UTF-8").useDelimiter("\\A");
//
//        String output = scanner.hasNext() ? scanner.next() : "";
//        return output;
//    }
//
//    private static void addRequestProperties(HttpURLConnection conn)
//    {
//        conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
//        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.8,sv;q=0.6");
//        conn.setRequestProperty("Connection", "keep-alive");
//        conn.setRequestProperty("Cookie", "PHPSESSID=95v211jdbcv53uup6d1h57i140");
//        conn.setRequestProperty("Host", "subscene.com");
//        conn.setRequestProperty("Upgrade-Insecure-Requests", "1");
//        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36");
//    }
//
//    private static String genRegexSearch()
//    {
//        String regex = "<div class=\"title\">\n" +
//                "\t\t<a href=\"/subtitles/(?<url>[^\"]*)\">(?<name>[^>]*)</a>\n" +
//                "\t</div>\n" +
//                "\t<div class=\"subtle count\">\n" +
//                "\n" +
//                "\t\t[0-9]+ subtitles\n" +
//                "\t</div>";
//        return regex;
//    }
//
//    private static String genRegexDownload()
//    {
//        String regex = "";
//        regex += ">Version ([^,<]*), ([0-9]|\\.)* MBs"; //Version, Size
//        regex += "(.*?)";
//        regex += "<!-- table header -->";
//        return regex;
//    }
//
//    private static String genRegexVersion()
//    {
//        String regex = "";
//        regex += "class=\"newsDate\"[^>]*>";
//        regex += "(?<comment>[^<]*?)"; //Comment
//        regex += "<img .*?";
//        regex += "class=\"language\">(?<language>[^<]*)<a href="; //Language
//        regex += ".*?";
//        regex += "href=\"(?<href>[^\"]*?)\"><strong>(?:(?:Download)|(?:original))??</strong>"; //Sub href
//        //regex += ".*?";
//        return regex;
//    }
//}
