import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by michael on 16/02/16.
 */
public class NameParser
{
    public String name;
    public String season;
    public String episode;
    public String quality;
    public MediaType type;

    private String input;

    public NameParser(String input)
    {
        this.input = input;
    }

    private void parse()
    {
        Matcher m = Pattern.compile(getShowRegex(), Pattern.CASE_INSENSITIVE).matcher(input);
        if (m.find())
        {
            type = MediaType.Show;
            name = m.group("name");
            season = m.group("season");
            episode = m.group("episode");
        }
    }

    private static String getShowRegex()
    {
        String regex = "";
        regex += "(?<name>.*?)S";
        regex += "(?<season>[0-9]+)E";
        regex += "(?<episode>[0-9]+)";
        regex += "(?<rest>.*?)[\\.| |-]";
        regex += "(?<release>[\\w]+)\\.";
        regex += "(?<ext>(?:[a-z]+[\\w]?){2,4})";
        regex += "$";
        return regex;
    }

    private static String getMovieRegex()
    {
        String regex = "";
        regex += "(?<name>.*?)";
        regex += "(?<year>[0-9]{4})";
        regex += "(?<rest>.*?)??[\\.| |-]";
        regex += "(?<release>[\\w]+)\\.(?<ext>(?:[a-z]+[\\w]?){2,4})";
        regex += "$";
        return regex;
    }

    //(?<name>.*?)(?<break>(?:[0-9]{4})|(?:[0-9]{3,4}p)|(?:brrip))(?<rest>.*?)??[\.| |-](?<release>[\w]+)\.(?<ext>(?:[a-z]+[\w]?){2,4})\n

    public enum MediaType { Show, Movie };
}

//http://regexr.com/
//(.*?)S([0-9]+)E([0-9]+)(?:[^0-9]*?)([0-9]{3,4}p)+(.*?)(mkv|mp4)
//http://www.addic7ed.com/serie/Lucifer/1/3/The_Would-Be_Prince_of_Darkness

//Shows

//Lucifer.S01E06.720p.HDTV.X264-DIMENSION.mkv
//It's Always Sunny in Philadelphia S11E06 Being Frank 720p WEB-DL AAC2.0 H.264-Oosh.mkv
//It's Always Sunny in Philadelphia S11E06 Being Frank 480p WEB-DL AAC2.0 H.264-Oosh.mkv
//its.always.sunny.in.philadelphia.s11e06.hdtv.x264-fleet.mp4
//It's Always Sunny in Philadelphia S11E05 Mac & Dennis Move To The Suburbs 720p WEB-DL AAC2.0 H.264-Oosh.mkv
//its.always.sunny.in.philadelphia.s11e04.720p.hdtv.x264-avs.mkv
//not.safe.with.nikki.glaser.s01e02.720p.hdtv.x264.mtg.mkv
//the.willis.family.s01e05.720p.hdtv.x264-cbfm.mkv
//family.feud.nz.s01e03.hdtv.x264-fihtv.mkv
//the.ellen.degeneres.show.2016.02.12.720p.hdtv.x264-alterego.mkv
//holby.city.s18e22.on.the.ropes.hdtv.x264-organic.mp4
//nbl.2015-16.grand.final.game.2.new.zealand.breakers.vs.perth.wildcats.pdtv.x264-cbfm.mp4
//countryside.999.s04e06.hdtv.x264-barge.mp4
//holby.city.s18e22.on.the.ropes.720p.hdtv.x264-organic.mkv
//The.Blacklist.S01E15.720p.HDTV.X264-DIMENSION

//Movies

//(?<name>.*?)(?<year>[0-9]{4})(.*)??(?<quality>[\d]{3,4}p)?(?<rest>.*?)??[\.| |-](?<release>[\w]+)\.(?<ext>(?:[a-z]+[\w]?){2,4})\n

//About.Time.2013.720p.BluRay.x264-x0r.mkv
//About.Time.2013.720p.BluRay.x264-x0r.mkv
//In.the.Heart.of.the.Sea.2015.720p.BRRip.X264.AC3-EVO.mkv
//Steve.Jobs.2015.720p.BRRip.X264.AC3-EVO.mkv
//santi-spectre.brrip.xvid.avi
//The.Big.Short.2015.720p.BluRay.x264-x0r.mkv
//Straight.Outta.Compton.2015.DC.720p.BRRiP.XViD.AC3-LEGi0N.avi
//santi-mrholmes.brrip.xvid.avi
//Fantastic.Four.2015.HC.720p.XViD.AC3-LEGi0N.avi
//Star.Wars.Episode.II.Attack.Of.The.Clones.BRRip.720p.x264-Warzone.mkv
//Gods.of.Egypt.2016.TS.XviD-ETRG.avi
//Gods Of Eygpt 2016 READNFO TS XviD AC3 ACAB.avi
//DeadPool 2016 HD-TS x264-CPG.mkv
//Alvin.And.The.Chipmunks.The.Road.Chip.2015.720p.BluRay.x264-x0r.mkv
//The.Peanuts.Movie.2015.DVDSCR.X264.AC3-EVO.mkv
//Dumb.and.Dumber.To.2014.720p.HC.R6.HDRiP.X264.AAC-Blackjesus.mkv
//Default.2014.HDRip.XviD.AC3-EVO.avi
//Meru.2015.Bluray.1080p.DTS-HD.x264-Grym.mkv
//BBC.How.to.Get.Ahead.1of3.At.Medieval.Court.720p.HDTV.x264.AC3.MVGroup.Forum.mkv
//The Lord of the Rings The Fellowship of the Ring 2001 2 BluRay 1080p DTS-HD MA 6.1 AVC ReMuX-SiMPLE@BluRG.mkv