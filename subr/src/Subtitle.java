import java.io.FileWriter;

/**
 * Created by michael on 15/02/16.
 */
public class Subtitle
{
    public String name;
    public String version;
    public String subUrl;
    public String language;
    public String comment;
    public SubParser subParser;

    private String preview;


    public Subtitle()
    {

    }

    public String getPreview() throws Exception
    {
        if (preview == null)
        {
            preview = subParser.downloadSub(this);
        }
        return preview;
    }

    public boolean download() throws Exception
    {
        FileWriter writer = new FileWriter("/Users/michael/Documents/"+name+".srt");
        writer.write(subParser.downloadSub(this));
        writer.close();
        return true;
    }
}
