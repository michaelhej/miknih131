package clientApp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import robots.ImageMap;
import robots.Player;
import robots.Point;
import robots.Score;


public class Main extends Application implements ClientInterface
{
	static Stage primaryStage;
	static String selectedIcon = "pok1";
	
	@Override
	public void start(Stage primaryStage)
	{
		Main.primaryStage = primaryStage;
		
		loadLoginPane();
	}
	
	/**
	 * Highlight selected icon at login screen
	 * @param event
	 */
	@FXML
	public void setIcon(MouseEvent event)
	{
		Object source = event.getSource();
		StackPane pane = (StackPane)source;

		while (primaryStage.getScene().lookup(".characterSelected") != null)
			primaryStage.getScene().lookup(".characterSelected").getStyleClass().remove(0);
		
		pane.getStyleClass().add("characterSelected");

		selectedIcon = pane.getId();
	}

	/**
	 * Connect the GameClient to server specified in ui
	 */
	public void connectToServer()
	{
		TextField fieldUsername = sceneItem("inputUsername");
		fieldUsername.setText(fieldUsername.getText().replace(':', ' '));
		String host = ((TextField)sceneItem("inputHost")).getText();
		
		String username = fieldUsername.getText();
		((ImageView)sceneItem("imgLoading")).setVisible(true);
		
		Player me = new Player(0, username, selectedIcon);
		GameClient.connectToServer(host, me, this);
		
		writeDefaultValues();
	}
	
	/**
	 * Set the status message on the login screen
	 */
	public void statusMessage(String msg)
	{
        Platform.runLater(() ->
        {
        	Label label = (Label)primaryStage.getScene().lookup("#labelStatus");
        	if (label == null)
        		return;
    		label.setText(msg);
    		((ImageView)sceneItem("imgLoading")).setVisible(false);
        });
	}
	
	/**
	 * Client successfully connects to server
	 * Add game pane to client game pane
	 */
	public void connected()
	{
		Platform.runLater(() ->
		{
        	loadPane("ClientGame");

        	//Teleport
    		Button btn = sceneItem("btnTeleport");
    		btn.setOnAction(event->{ performAction("teleport"); });

    		//Safe teleport
    		btn = sceneItem("btnSafeTeleport");
    		btn.setOnAction(event->{ performAction("safeTeleport"); });
    		
    		//Attack
    		btn = sceneItem("btnAttack");
    		btn.setOnAction(event->{ performAction("attack"); });
    		
			//Map stage closing to disconnect
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				public void handle(WindowEvent we)
				{
					System.out.println("Client stage closing...");
					GameClient.disconnect();
					Platform.exit();
					System.exit(0);
				}
			}); 
        });
	}
	/**
	 * Initializes a new game and maps keyboard and mouse input
	 */
	public void initGame()
	{
		Platform.runLater(() ->
		{
	    	Pane pane = sceneItem("gamePane"); 
	    	pane.getChildren().remove(sceneItem("labelWaitingForPlayers")); //Waiting for players
	    	pane.getChildren().add(GameClient.getGame().getGamePane());
	    	
			Scene scene = primaryStage.getScene();
			scene.getStylesheets().add(GameClient.getGame().getStyleSheet());
			
			mapKeyboard(scene);
			mapMouse(scene);
			mapResize(scene);
			
			redraw();
        });
	}

	/**
	 * Redraw the game grid
	 */
	public void redraw()
	{
		Platform.runLater(() ->
		{
        	GameClient.getGame().resetGrid();
        	Pane gp = sceneItem("gamePane");
        	GameClient.getGame().resize(gp.getWidth(), gp.getHeight());
        });
	}
	
	/**
	 * Game has exited
	 */
	public void exit(ExitReason reason)
	{
		Platform.runLater(() ->
		{
        	primaryStage.setOnCloseRequest(null);
        	if (reason == ExitReason.Death)
        	{
        		showHighscore();
        		return;
        	}
        	loadLoginPane();
        	
        	if (reason == ExitReason.InProgress)
        		statusMessage("A game is in progress. Try later!");	
        	
        	if (reason == ExitReason.Kicked)
        		statusMessage("Kicked from game! Bad boy...");
        	
        	if (reason == ExitReason.ConnectionLost)
        		statusMessage("Disconnected from server...!");
        });
	}
	
	/**
	 * Show the login screen
	 */
	private void loadLoginPane()
	{
		loadPane("ClientLogin");
    	readDefaultValues();

		//Connect button
		Button startBtn = sceneItem("btnConnect");
		startBtn.setOnAction(event->{ connectToServer(); });

		Platform.runLater(() ->
		{
			for (int i = 1; i <= 5; i++)
			{
				ImageView img = sceneItem("imgPok" + i);
				img.setImage(ImageMap.getImage("pok" + i));
			}
		});
	}
	
	/**
	 * Draw the highscore list
	 */
	private void showHighscore()
	{	
		loadPane("ClientHighscore");
		Pane hsPane = sceneItem("paneHighscore");
		
		if (GameClient.highscore == null)
			System.out.println("NO HIGHSCORE RECEIVED!");
		
		if (hsPane == null || GameClient.highscore == null)
		{
			loadLoginPane();
			return;
		}
		
		hsPane.getChildren().clear();
		
		int offset = 1;
		for (Score score : GameClient.highscore.getScores())
		{
			Label nameLabel = new Label(score.name);
			nameLabel.layoutXProperty().set(20);
			nameLabel.layoutYProperty().set(offset*20);
			nameLabel.setTextFill(Color.web("#fff"));
			
			Label scoreLabel = new Label(score.score + "");
			scoreLabel.layoutXProperty().set(240);
			scoreLabel.layoutYProperty().set(offset*20);
			if (score.score == GameClient.score)
				scoreLabel.setTextFill(Color.web("#00ff00"));
			else
				scoreLabel.setTextFill(Color.web("#fff"));

			hsPane.getChildren().add(nameLabel);
			hsPane.getChildren().add(scoreLabel);
			
			offset++;
		}
		
		Label footer = sceneItem("labelFooter");
		footer.setText("Game ended!\n" + 
					   "Your score: " + GameClient.score);
		
		//Return to loginscreen on close
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
		{
			public void handle(WindowEvent we)
			{
				System.out.println("Highscore closed");
				primaryStage.setOnCloseRequest(null);
            	loadLoginPane();
            	we.consume();
			}
		});
	}

	/**
	 * Flash new turn animation
	 */
	public void newTurn(boolean myTurn)
	{
		Pane btnPane = sceneItem("paneActionButtons");
		if (btnPane == null)
			return;
		if (myTurn)
		{
			btnPane.setDisable(false);
		}
		else
		{
			btnPane.setDisable(true);
			setLabel("Timer", "");
			return;
		}
		
		
		Platform.runLater(() ->
		{
        	setLabel("Notify", "Your turn!");
        	flashNotify("NewTurn", 100);
        });
	}
	
	/**
	 * Flash the notify icon
	 * @param paneName	NewTurn|NewLevel
	 * @param offset
	 */
	private void flashNotify(String paneName, double offset)
	{
		Platform.runLater(() ->
		{
			Pane turnPane = sceneItem("pane" + paneName);
			Pane gamePane = sceneItem("gamePane");
			
			if (turnPane == null || gamePane == null)
				return;
			
			turnPane.setVisible(true);
			turnPane.toFront();
			turnPane.setLayoutX(gamePane.getWidth() / 2 - 100);
			turnPane.setLayoutY(gamePane.getHeight() / 2 - offset);
			
			FadeTransition ft = new FadeTransition(Duration.millis(500), turnPane);
			ft.setFromValue(0);
			ft.setToValue(0.9);
			ft.setCycleCount(2);
			ft.setAutoReverse(true);
		    ft.play();
		    
		    ft.setOnFinished(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					turnPane.setVisible(false);
				}
			});
        });
	}
	
	public void setLabel(String field, String value)
	{
		Platform.runLater(() ->
		{
        	Label labelTimer = sceneItem("label" + field);
        	if (labelTimer == null)
        		return;
        	
        	labelTimer.setText(value);

        	if (field.equals("Level"))
        	{
            	setLabel("NotifyLevel", "Level " + value + "");
            	flashNotify("NewLevel", 200);
            		redraw();
            }
 
            	/* Disable/enable action buttons */
        	
        	if (field.equals("Teleports"))
        	{
        		Button btn = (Button)sceneItem("btnTeleport");
        		if (Integer.parseInt(value) > 0)
        			btn.setDisable(false);
        		else
        			btn.setDisable(true);
        	}

        	if (field.equals("SafeTeleports"))
        	{
        		Button btn = (Button)sceneItem("btnSafeTeleport");
        		if (Integer.parseInt(value) > 0)
        			btn.setDisable(false);
        		else
        			btn.setDisable(true);
        	}

        	if (field.equals("Attacks"))
        	{
        		Button btn = (Button)sceneItem("btnAttack");
        		if (Integer.parseInt(value) > 0)
        			btn.setDisable(false);
        		else
        			btn.setDisable(true);
        	}
        });
	}
	
	/**
	 * Highlight adjacent grids by changing its css class
	 */
	public void highlighAdjacent(Point position, String cssClass)
	{
		Platform.runLater(() ->
		{
			GameClient.getGame().setAdjacentClass(cssClass, position);
	    }); 
	}
	
	/**
	 * Read defaults from the harrdrive
	 * host, username, icon
	 */
	private void readDefaultValues()
	{
		String host = "";
		String username = "";
		String icon = "";
		
		//Read default values from file
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader("rdefaults.txt"));
			
			host = reader.readLine();
			username = reader.readLine();
			icon = reader.readLine();
			reader.close();
		}
		catch(Exception ex)
		{
			System.out.print("Could not read defaults from rdefaults.txt");
			return;
		}
		
		//Set default values to ui
		try
		{
			TextField inputHost = ((TextField)sceneItem("inputHost"));
			inputHost.setText(host);
			
			TextField inputUsername = ((TextField)sceneItem("inputUsername"));
			inputUsername.setText(username);
			
			if (icon.equals(""))
				return;
			
			StackPane pane = (StackPane)sceneItem(icon);
			if (pane == null)
			{
				System.out.println("Could not parse icon: " + icon);
				return;
			}
	
			while (primaryStage.getScene().lookup(".characterSelected") != null)
				primaryStage.getScene().lookup(".characterSelected").getStyleClass().remove(0);
			
			pane.getStyleClass().add("characterSelected");
	
			selectedIcon = pane.getId();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void writeDefaultValues()
	{
		String host = ((TextField)sceneItem("inputHost")).getText();
		String username = ((TextField)sceneItem("inputUsername")).getText();
		
		try
		{
			PrintWriter writer = new PrintWriter("rdefaults.txt", "UTF-8");
			writer.println(host);
			writer.println(username);
			writer.println(selectedIcon);
			writer.close();
		}
		catch(Exception ex)
		{
			System.out.print("Could not write defaults to rdefaults.txt");
		}
	}

	/**
	 * Retreive an item from scene
	 * @param itemId	item fx-id
	 * @return
	 */
	private <T> T sceneItem(String itemId)
	{
		if (primaryStage.getScene().lookup("#" + itemId) == null)
		{
			System.out.println("Count not find: " + itemId);
			return null;
		}
		
		return (T)primaryStage.getScene().lookup("#" + itemId);
	}
	
	/**
	 * Load a new fxml-pane as scene
	 * @param graph
	 * @return
	 */
	private Scene loadPane(String graph)
	{
		AnchorPane pane = null;
		try
		{
			pane = FXMLLoader.load(getClass().getResource(graph + ".fxml"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		Scene scene = new Scene(pane, 800, 600);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		//Show scene
		primaryStage.setScene(scene);
		
		if (!primaryStage.isShowing())
			primaryStage.show();
		
		return scene;
	}
	
	/**
	 * Perform an action in the GameClient
	 * @param action
	 */
	private void performAction(String action)
	{
		GameClient.getGame().setAdjacentClass("gameCell", GameClient.getPosition());
		
		switch (action)
		{
		case "teleport":
			GameClient.teleport();
			break;
		case "safeTeleport":
			GameClient.safeTeleport();
			break;
		case "attack":
			GameClient.attack();
			break;			
		}
	}
	
	/**
	 * Grab keyboard input on game scene
	 * @param scene
	 * @param game
	 */
	private void mapKeyboard(Scene scene)
	{
	    scene.setOnKeyPressed(new EventHandler<KeyEvent>()
	    {
	        @Override
	        public void handle(KeyEvent event)
	        {
	        	switch (event.getCode())
	        	{
	        		//Actions
	        		case T:
	        			performAction("teleport");
	        			break;
	        		case Y:
	        			performAction("safeTeleport");
	        			break;
	        		case U:
	        			performAction("attack");
	        			break;
	        			
	        		//Arrow movement
					case LEFT:
						GameClient.move(-1, 0);
						break;
					case RIGHT:
						GameClient.move(1, 0);
						break;
					case UP:
						GameClient.move(0, -1);
						break;
					case DOWN:
						GameClient.move(0, 1);
						break;
						
					//Move 8 grid
					case Z:
						GameClient.move(-1, 1);
						break;
					case X:
						GameClient.move(0, 1);
						break;
					case C:
						GameClient.move(1, 1);
						break;
					case A:
						GameClient.move(-1, 0);
						break;
					case D:
						GameClient.move(1, 0);
						break;
					case Q:
						GameClient.move(-1, -1);
						break;
					case W:
						GameClient.move(0, -1);
						break;
					case E:
						GameClient.move(1, -1);
						break;
						
					//Num pad should be corrent. Not tried...
					case NUMPAD1:
						GameClient.move(-1, 1);
						break;
					case NUMPAD2:
						GameClient.move(0, 1);
						break;
					case NUMPAD3:
						GameClient.move(1, 1);
						break;
					case NUMPAD4:
						GameClient.move(-1, 0);
						break;
					case NUMPAD6:
						GameClient.move(1, 0);
						break;
					case NUMPAD7:
						GameClient.move(-1, -1);
						break;
					case NUMPAD8:
						GameClient.move(0, -1);
						break;
					case NUMPAD9:
						GameClient.move(1, -1);
						break;
					default:
						break;
				}
			}
		});
	}
	
	/**
	 * Maps redraw function to scene resize
	 * @param scene
	 */
	private void mapResize(Scene scene)
	{
		scene.widthProperty().addListener(new ChangeListener<Number>()
		{
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth)
		    {
		        redraw();
		    }
		});
		
		scene.heightProperty().addListener(new ChangeListener<Number>()
		{
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight)
		    {
		    	redraw();
		    }  
		});
	}
	
	/**
	 * Grab mouse input on game scene
	 * @param scene
	 * @param game
	 */
	private void mapMouse(Scene scene)
	{
		/**
		 * Move to(wards) grid on click
		 */
		scene.setOnMouseClicked(new EventHandler<MouseEvent>()
		{
			@Override
            public void handle(MouseEvent event)
			{
				Point gridPosition = GameClient.getGame().mouseToGrid((int)event.getX(), (int)event.getY());
				
				if (gridPosition.x < 0)
					return;
				
                GameClient.moveTowards(gridPosition);
            }
		});
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
