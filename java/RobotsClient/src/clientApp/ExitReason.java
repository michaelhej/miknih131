package clientApp;

public enum ExitReason
{
	InProgress,
	Death,
	Win,
	ConnectionLost,
	Kicked
}
