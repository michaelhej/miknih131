package clientApp;

import robots.Point;

public interface ClientInterface
{
	void statusMessage(String msg);
	void setLabel(String field, String value);
	void connected();
	void redraw();
	void newTurn(boolean myTurn);
	void initGame();
	void highlighAdjacent(Point position, String cssClass);
	void exit(ExitReason reason);
}
