package clientApp;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import robots.Actor;
import robots.GameSettings;
import robots.Highscore;
import robots.Player;
import robots.Point;
import robots.Robot;
import robots.Robots;

public class GameClient
{
	public static GameClient client;
	public static Highscore highscore;
	public static int score = 0;
	
	public String host;
	public ClientInterface ui;
	
	private boolean myTurn = false;
	private Robots game;
	private Player me;
	private Socket socket;
	private ObjectOutputStream writer;
	private ObjectInputStream reader;
	
	private GameClient() {	}
	
	/* STATIC METHODS */
	
	/**
	 * Static function connecting the client instance to a server
	 * creates a new client thread if no such exists
	 * @param host
	 * @param player
	 * @param ui	clientInterface to be used to this GameClient
	 */ 
	public static void connectToServer(String host, Player player, ClientInterface ui)
	{
		if (client == null)
		{
			client = new GameClient();
		}
		else
		{
			if (client.socket != null && client.socket.isConnected())
			{
				System.out.println("Client already connected!");
				return;
			}
		} 
		client.game = new Robots();
		
		client.ui = ui;
		client.me = player;
		new Thread(() -> client.connect(host)).start();
	}
	
	/**
	 * Return the game instance used by the game client
	 * @return
	 */
	public static Robots getGame()
	{
		return client.game;
	}
	
	/**
	 * Return the position of player associated with game client
	 * @return
	 */
	public static Point getPosition()
	{
		return client.me.position;
	}
	
	/**
	 * Perform move command to player
	 * @param dx
	 * @param dy
	 */
	public static void move(int dx, int dy)
	{
		client.ui.highlighAdjacent(client.me.position, "gameCell");
		client.movePlayer(dx, dy);
	}

	/**
	 * Perform a teleport
	 */
	public static void teleport()
	{
		if (client.game.settings.teleports <= 0)
			return;

		client.game.settings.teleports--;
		client.performAction("teleport");
	}
	
	/**
	 * Perform a safe telert
	 */
	public static void safeTeleport()      
	{
		if (client.game.settings.safeTeleports <= 0)
			return;

		client.game.settings.safeTeleports--;
		client.performAction("safeTeleport");
	}
	
	/**
	 * Perform an attack
	 */
	public static void attack()
	{
		if (client.game.settings.attacks <= 0)
			return;

		client.game.settings.attacks--;
		client.performAction("attack");
	}

	/**
	 * Move player towards a point
	 * @param targetPosition
	 */
	public static void moveTowards(Point targetPosition)
	{
		//Only allow click on adjacent grids
		if (client.distanceTo(targetPosition) > 1.5)
			return;

		Point dPosition = new Point(0, 0);
		dPosition.x = Integer.signum(targetPosition.x - client.me.position.x);
		dPosition.y = Integer.signum(targetPosition.y - client.me.position.y);
		
		move(dPosition.x, dPosition.y);
	}
	
	/* END STATIC METHODS */
	
	/**
	 * Connects the client to host
	 * @param host
	 */
	private void connect(String host)
	{
		if (socket != null && socket.isConnected())
		{
			System.out.println("Already connected. Aborting.");
			return;
		}
		
		try
		{
			socket = new Socket(host, 12121);
		
			if (socket.isConnected())
			{
				ui.connected();
				System.out.println("Client connected!");
				
				handshake();
				listen();
			}
		}
		catch (Exception ex)
		{
			ui.statusMessage("Error connecting to " + host);
			return;    
		} 
	}
	
	/**
	 * Disconnect client from server
	 */
	public static void disconnect()  
	{
		try
		{
			if (client == null || client.socket == null)
				return;
			
			client.socket.close();
		}
		catch (IOException ex)
		{
			/* Ignore */
		}
	}


	/**
	 * Initialize network streams and send player info
	 */
	private void handshake()
	{
		try
		{
			reader = new ObjectInputStream(socket.getInputStream());
			writer = new ObjectOutputStream(socket.getOutputStream());

			writer.writeObject("name:" + me.name);
			writer.writeObject("icon:" + me.playerIcon);
			writer.flush();
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Listening for incoming commands from the game server
	 * @throws IOException
	 */
	private void listen()
	{
		boolean firstSettings = true;
		int errcount = 0;
		while (socket.isConnected() && errcount < 3) //Allow for 3 errors
		{
			try
			{
				if (reader == null)
					break;
				
				Object obj = reader.readObject();
				
				if (obj == null) //Server disconnected
					break;
				
				if (obj instanceof GameSettings)
				{
					assignSettings(obj, firstSettings);
					firstSettings = false;
					continue;
				}

				if (obj instanceof Actor)
					addActor(obj);

				if (obj instanceof Highscore)
					addHighscore(obj);
				
				if (obj instanceof String)
					parseCommand((String)obj);
			}
			catch (SocketException | EOFException ex)
			{
				//Server disconnected
				break;
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				errcount++;
				continue;
			}
		}
		exit(ExitReason.ConnectionLost);
		System.out.println("Disconnected from server!");
	}
	
	/**
	 * Assign a new settings object
	 * @param obj
	 * @param first
	 */
	private void assignSettings(Object obj, boolean first)
	{
		if (!first)
			return;

		game.settings = (GameSettings)obj;
		game.createGrid();
		ui.initGame();
		ui.redraw();
		
		ui.setLabel("Teleports", Integer.toString(game.settings.teleports));
		ui.setLabel("SafeTeleports", Integer.toString(game.settings.safeTeleports));
		ui.setLabel("Attacks", Integer.toString(game.settings.attacks));
	} 
	
	/**
	 * Add a new actor to the game
	 * @param o
	 */
	private void addActor(Object o)
	{
		Actor actor = (Actor)o;
		
		if (!actor.dead)
			game.addActor(actor);
		else
			game.rmActor(actor);
		
		if (actor instanceof Robot)
		{
			Robot newRobot = (Robot)actor;
			newRobot.checkChasingMe(me.id);
		}
		
		ui.redraw();
	}
	
	private void addHighscore(Object obj)
	{
		highscore = (Highscore)obj;
	}
	
	/**
	 * Perform an action
	 * @param strAction teleport|safeTeleport|attack
	 */
	public void performAction(String strAction)
	{
		if (!myTurn)
			return;

		myTurn = false; 
		sendToServer("action:" + strAction);
		
		ui.highlighAdjacent(me.position, "gameCell");
		ui.newTurn(false);
		
		ui.setLabel("Teleports", Integer.toString(game.settings.teleports));
		ui.setLabel("SafeTeleports", Integer.toString(game.settings.safeTeleports));
		ui.setLabel("Attacks", Integer.toString(game.settings.attacks));
	}
	
	private double distanceTo(Point position)
	{
		return Math.sqrt( (position.x - me.position.x)*(position.x - me.position.x)
						+ (position.y - me.position.y)*(position.y - me.position.y));
	}
	
	/**
	 * Move the player
	 * @param dx
	 * @param dy
	 */
	private void movePlayer(int dx, int dy)
	{
		if (!myTurn)
			return;

		myTurn = false;
		ui.newTurn(false);
		
		game.setAdjacentClass("gameCell", me.position);
		game.moveActor(me.id, me.position.x + dx, me.position.y + dy);
		
		new Thread(() -> sendPosition()).start();
	}
	
	/**
	 * Send players current position to the server
	 */
	private void sendPosition()
	{
		sendToServer("move:" + me.position.x + ":" + me.position.y);
	}
	
	/**
	 * Send a string command to the server
	 * @param str
	 */
	private void sendToServer(Object obj)
	{  
		try
		{
			writer.writeObject(obj);
			writer.flush();
		}
		catch(SocketException ex)
		{ 
			exit(ExitReason.ConnectionLost);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Parse an incoming server command
	 * @param cmdStr
	 */
	private void parseCommand(String cmdStr)
	{
		String[] cmd = cmdStr.split(":");
		
		if (!cmd[0].equals("timer"))
			System.out.println("Command: " + cmd[0]);
		
		if (!cmd[0].equals("turn") && !cmd[0].equals("timer"))
			ui.highlighAdjacent(me.position, "gameCell");
		
		try
		{
			switch (cmd[0])
			{
			case "id":
				assignId(cmd);
				break;
			case "turn":
				turn();
				break;
			case "clear":
				game.clearNPCs();
				break;
			case "move":
				moveActor(cmd);
				break;
			case "kill":
				newKill();
				break;
			case "pos":
				setMyPosition(cmd);
				break;
			case "level":
				newLevel(cmd);
				break;
			case "score":
				setScore(cmd);
				break;
			case "round":
				setRound(cmd);
				break;
			case "timer":
				timerTick(cmd);
				break;
			case "timeout":
				timeout();
				break;
			case "kick":
				exit(ExitReason.Kicked);
				break;
			case "disconnect":
				exit(ExitReason.ConnectionLost);
				break;
			case "inprogress":
				exit(ExitReason.InProgress);
				break;
			case "dead":
				exit(ExitReason.Death);
				break;
			case "win":
				exit(ExitReason.Win);
				break;
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Assigns our self a new id and adds to game
	 * @param cmd
	 */
	private void assignId(String[] cmd)
	{
		me.id = Integer.parseInt(cmd[1]);
		game.clearNPCs();
		game.addPlayer(me);
	}
	
	/**
	 * Our turn
	 */
	private void turn()
	{
		myTurn = true;
		ui.newTurn(true);
		ui.highlighAdjacent(me.position, "gameCell-allow");
	}
	
	/**
	 * Performs a move command for an actor
	 * @param cmd
	 */
	private void moveActor(String[] cmd)
	{
		game.moveActor(	Integer.parseInt(cmd[1]),
						Integer.parseInt(cmd[2]),
						Integer.parseInt(cmd[3]));
	}
	
	/**
	 * We killed a robot
	 * Reaping reward
	 */
	private void newKill()
	{
		game.settings.teleports += game.settings.teleportsPerKill;
		game.settings.safeTeleports += game.settings.safeTeleportsPerKill;
		
		ui.setLabel("Teleports", Integer.toString(game.settings.teleports));
		ui.setLabel("SafeTeleports", Integer.toString(game.settings.safeTeleports));
	}
	
	/**
	 * Receives a new position from server
	 * @param cmd
	 */
	private void setMyPosition(String[] cmd)
	{
		ui.highlighAdjacent(me.position, "gameCell");
		me.position.x = Integer.parseInt(cmd[1]);
		me.position.y = Integer.parseInt(cmd[2]);
		
		if (myTurn)
			ui.highlighAdjacent(me.position, "gameCell-allow");
		
		ui.redraw();
		
		/* Exception could be caused in ui drawing
		   if highlighAdjacent are called when changing
		   position, can happen when player is only player left
		   and teleporting very fast. Fixes with a small sleep. */
		
		try
		{
			Thread.sleep(100);
		}
		catch (Exception ex)
		{
			/* Ignore */
		}
	}
	
	/**
	 * Our timeout timer ticks
	 * @param cmd
	 */
	private void timerTick(String[] cmd)
	{
		ui.setLabel("Timer", cmd[1]);
	}

	/**
	 * Timeout occurred
	 * Turn voided
	 */
	private void timeout()
	{
		myTurn = false;
		ui.newTurn(false);
		ui.highlighAdjacent(me.position, "gameCell");
	}
	
	/**
	 * Next level!
	 * Awarded new attacks
	 * @param cmd
	 */
	private void newLevel(String[] cmd)
	{
		ui.setLabel("Level", cmd[1]);
		
		game.settings.attacks += game.settings.attacksPerLevel;
		ui.setLabel("Attacks", Integer.toString(game.settings.attacks));
	}
	
	/**
	 * New round
	 * @param cmd
	 */
	private void setRound(String[] cmd)
	{
		ui.setLabel("Round", cmd[1]);
	}
	
	/**
	 * Update score
	 * @param cmd
	 */
	private void setScore(String[] cmd)
	{
		score = Integer.parseInt(cmd[1]);
		ui.setLabel("Score", cmd[1]);
	}
	
	/**
	 * Quitting game
	 * @param reason
	 */
	private void exit(ExitReason reason)
	{
		if (highscore != null)
			highscore.add(me.name, score);
		try
		{
			System.out.println(" -- Exit -- reason: " + reason);
			socket.close();
			socket = null;
			
		}
		catch (Exception ex)
		{
			/* Ignore */
		}
		finally
		{
			ui.exit(reason);
		}
		
	}
}
