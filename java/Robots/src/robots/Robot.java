package robots;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Collectors;

import robots.GameSettings.RobotChaseType;

public class Robot extends Actor implements Serializable
{
	private static final long serialVersionUID = -7462854770170490863L;
	public int targetPlayerId;
	RobotChaseType chaseType;
	final double perception = 5;
	transient Robots game;
	private transient Actor targetPlayer;
	private String robotIcon = "robot";
	
	public Robot(int id)
	{
		this(id, 0, 0);
	}
	
	public Robot(int id, int x, int y)
	{
		this.id = id;
		this.position = new Point(x, y);
		
		chaseType = RobotChaseType.ClosestPlayer;
	}
	
	public Actor getTarget()
	{
		return targetPlayer;
	}
	
	public void aquireNewTarget()
	{
		targetPlayer = closestPlayer();
		if (targetPlayer != null)
			targetPlayerId = targetPlayer.id;
	}
	
	/**
	 * Changes icon for robot depending on if it is chasing client player
	 * @param playerId
	 */
	public void checkChasingMe(int playerId)
	{
		if (chaseType == RobotChaseType.ClosestPlayer)
		{
			return;
			//if (closestPlayer().id == playerId)
			//	robotIcon = "RedRobot";
		}
		//if (playerId == targetPlayerId)
		//	robotIcon = "redRobot";
	}
	
	public void setGame(Robots game)
	{
		this.game = game;
	}
	
	/**
	 * Returns the next position for this robot based on chase type
	 * @return
	 */
	public Point nextPosition()
	{
		Point newPosition = position;
		chaseType = game.settings.chaseType;

		if (chaseType == RobotChaseType.ClosestPlayer)
		{
			newPosition = moveTowards(closestPlayer().position);
			if (distanceTo(newPosition) > perception)
				newPosition = position;
		}

		if (chaseType == RobotChaseType.TargetPlayer)
		{
			if (targetPlayer == null || !game.getPlayers().contains(targetPlayer) || targetPlayer.dead)
				aquireNewTarget();
			
			newPosition = moveTowards(targetPlayer.position);
		}
		
		return newPosition;
	}
	
	/**
	 * Return a new point towards target position
	 * @param position
	 * @return
	 */
	private Point moveTowards(Point targetPosition)
	{ 
		Point dPosition = new Point(0, 0);
		dPosition.x = Integer.signum(targetPosition.x - this.position.x);
		dPosition.y = Integer.signum(targetPosition.y - this.position.y);
		
		return new Point(this.position.x + dPosition.x, this.position.y + dPosition.y);
	}
	
	/**
	 * Returns the closest player
	 * @return
	 */
	private Actor closestPlayer()
	{
		if (game.getPlayers().isEmpty())
			return this;
		
		double closestDistance = Double.MAX_VALUE;
		Actor closestPlayer = game.getPlayers().get(0);
		
		for (Actor actor : game.getPlayers())
		{
			double distance = distanceTo(actor.position);
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestPlayer = actor;
			}
		}
		
		return closestPlayer;
	}
	
	/**
	 * Calculates distance from robot to a position
	 * @param targetPosition
	 * @return
	 */
	private double distanceTo(Point targetPosition)
	{
		return Math.sqrt( (position.x - targetPosition.x)*(position.x - targetPosition.x)
						+ (position.y - targetPosition.y)*(position.y - targetPosition.y));
	}
	
	@Override
	protected String icon()
	{
		return robotIcon;
	};
}
