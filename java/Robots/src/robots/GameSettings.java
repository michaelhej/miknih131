package robots;

import java.io.Serializable;

public class GameSettings implements Serializable
{
	private static final long serialVersionUID = 2786045042549422922L;
	public final int cellGap	=	3;
	public Point gridSize		=	new Point(25, 25);
	public int roundsPerLevel	=	10;
	public int cellSize			=	20;
	public int initialRubble	=	0;
	public int rubblePerLevel	=	0;
	public int initialRobots	=	5;
	public int robotsPerLevel	=	5;
	public int teleports	 	= 	0;
	public int safeTeleports	=	0;
	public int attacks			=	0;
	public int attackMaxKills	=	2;
	public int attacksPerLevel	= 	0;
	public int attackRange		=	1;
	public int teleportsPerKill	=	0;
	public int safeTeleportsPerKill=0;
	
	public RobotChaseType chaseType	= RobotChaseType.ClosestPlayer;
	public RobotCollisionType robotCollision = RobotCollisionType.Rubble;
	
	//Enums
	public enum RobotCollisionType implements Serializable
	{ Rubble, Merge }
	
	public enum RobotChaseType implements Serializable
	{ ClosestPlayer, TargetPlayer }
}
