package robots;

import java.io.File;
import java.util.HashMap;

import javafx.scene.image.Image;

public class ImageMap
{
	private HashMap<String, Image> images = new HashMap<String, Image>();
	private static ImageMap map;
	private final String imgPath = "robots" + File.separator + "resources" + File.separator;
	
	/**
	 * Create image objects and map to icon names
	 */
	private ImageMap()
	{
		images.put("robot", getImg("Robot.png"));
		images.put("redRobot", getImg("RedRobot.png"));
		images.put("dirt", getImg("dirt.png"));
		images.put("metal", getImg("metal.png"));
		images.put("pok1", getImg("pok1.png"));
		images.put("pok2", getImg("pok2.png"));
		images.put("pok3", getImg("pok3.png"));
		images.put("pok4", getImg("pok4.png"));
		images.put("pok5", getImg("pok5.png"));
	}
	
	private Image getImg(String name)
	{
		Image img = null;
		try
		{
			img = new Image(imgPath + name);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return img;
	}
	
	public static Image getImage(String img)
	{
		if (map == null)
			map = new ImageMap();
		
		return map.images.get(img);
	}
}
