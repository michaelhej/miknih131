package robots;

import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class GameCell extends StackPane
{
	private Actor actor;
	private Robots game;
	private ImageView image;
	int x;
	int y;
	
	/**
	 * Instantiate a new game cell with an image
	 * @param cellSize
	 */
	public GameCell(int x, int y)
	{
		super();
		this.x = x;
		this.y = y;
		
		int cellSize = 30;
    	this.getStyleClass().add("gameCell");
    	this.setMinSize(cellSize, cellSize);
    	this.setMaxSize(cellSize, cellSize);

		image = new ImageView();
    	image.fitWidthProperty().bind(this.widthProperty());
    	image.fitHeightProperty().bind(this.heightProperty());
    	
		image.setVisible(false);
		
		this.getChildren().add(image);
		
		/*
		this.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>()
		{
		     @Override
		     public void handle(MouseEvent event) {
		         System.out.print("Cell pressed!  ");
		         if (actor instanceof Player)
		         {
		        	 Player player = (Player)actor;
		        	 System.out.println("player in cell: " + player.id + " " + player.name + " " + player.icon());
		         }
		         System.out.println("");
		         event.consume();
		     }
		});
		*/
	}
	
	public Actor getActor()
	{
		if (actor != null)
		{
			if (actor.position.x != x || actor.position.y != y)
			{
				return null;
			}
		}
		return actor;
	}
	
	public void resize(int cellSize)
	{
    	this.setMinSize(cellSize, cellSize);
    	this.setMaxSize(cellSize, cellSize);
	}
	
	public void setClass(String cssClass)
	{
		this.getStyleClass().remove(0);
		this.getStyleClass().add(cssClass);
	}
	
	//Clears this cell if is does not contain a player
	public void clearNPC()
	{
		if (actor == null)
			return;
		
		if (!(actor instanceof Player))
			actor = null;
	}
	
	public void setActor(Actor actor)
	{
		this.actor = actor;
		if (actor != null)
		{
			image.setImage(ImageMap.getImage(actor.icon()));
			image.setVisible(true);
		}
		else
		{
			image.setVisible(false);
		}
	}
}
