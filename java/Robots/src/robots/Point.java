package robots;

import java.io.Serializable;

public class Point implements Serializable
{
	private static final long serialVersionUID = 1L;
	public int x;
	public int y;
	
	public Point()
	{
		x = y = 0;
	}
	
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Point(Point p)
	{
		this.x = p.x;
		this.y = p.y;
	}
}
