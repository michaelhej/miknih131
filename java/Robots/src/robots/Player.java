package robots;

import java.io.Serializable;
import java.net.Socket;

public class Player extends Actor implements Serializable
{
	private static final long serialVersionUID = 4496083143838892527L;
	public String name;
	public String playerIcon;
	public int teleportsLeft;
	public int safeTeleportsLeft;
	public int attacksLeft;
	
	//Server variables
	transient public Socket socket;
	
	public Player() { }
	
	public Player(int id, String name, String icon)
	{
		this.id = id;
		this.name = name;
		this.playerIcon = icon;
		this.position = new Point();
		this.dead = false;
	}
	
	@Override
	public String icon()
	{
		return playerIcon;
	}

	@Override
	public String toString()
	{
		return name;
	}
}
