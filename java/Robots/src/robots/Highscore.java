package robots;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Highscore implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final static String filename = "highscore.robots";
	private ArrayList<Score> scores;
	
	public Highscore()
	{
		scores = new ArrayList<Score>();
	}
	
	/**
	 * Add a score to the list
	 * @param name
	 * @param score
	 */
	public void add(String name, int score)
	{
		scores.add(new Score(name, score));
	}
	
	/**
	 * Returns a list with sorted scores
	 * @return
	 */
	public ArrayList<Score> getScores()
	{
		Collections.sort(scores);
		
		if (scores.size() < 10)
			return scores;
		
        return new ArrayList<Score>(scores.subList(0, 10));
    }
	/**
	 * Read highscore from file
	 * @return new highscore object
	 */
	public static Highscore readHighscore()
	{
		Highscore highscore = new Highscore();
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
		    String line;
		    while ((line = br.readLine()) != null)
		    {
		       String[] scoreLine = line.split(":");
		       highscore.add(scoreLine[0], Integer.parseInt(scoreLine[1]));
		    }
		    br.close();
		}
		catch (Exception ex)
		{
			return null;
		}
		return highscore;
	}
	
	/**
	 * Write highscores to file
	 */
	public void writeHighscore()
	{
		try
		{
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			for (Score score : scores)
			{
				writer.println(score.name + ":" + score.score);
			}
			writer.close();
		}
		catch(Exception ex)
		{
			System.out.print("Could not write defaults to rdefaults.txt");
		}
	}
	
}
