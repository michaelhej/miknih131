package robots;

import java.io.Serializable;

public class Actor implements Serializable, Comparable<Actor>
{ 
	private static final long serialVersionUID = 9161447611577124669L;
	public int id;
	public boolean dead = false;
	public Point position;
	
	protected String icon()
	{
		return "";
	}

	@Override
	public int compareTo(Actor other)
	{
		return Integer.compare(id, other.id);
	}
	
	@Override
	public int hashCode()
	{
		return id;
	}
	
	@Override
    public boolean equals(Object other)
	{
       if (!(other instanceof Actor))
            return false;
       
        return ((Actor)other).id == this.id;
	}
	
}
