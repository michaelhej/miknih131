package robots;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Collectors;

import robots.GameSettings.RobotChaseType;

public class Rubble extends Actor implements Serializable
{	
	private static final long serialVersionUID = -329548437079445170L;

	public Rubble(int id)
	{
		this(id, 0, 0);
	}
	
	public Rubble(int id, int x, int y)
	{
		this.id = id;
		this.position = new Point(x, y);
	}
	
	@Override
	protected String icon()
	{
		return "dirt";
	};
}
