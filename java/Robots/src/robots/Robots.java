package robots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;
import javafx.scene.layout.GridPane;
import robots.GameSettings.RobotCollisionType;

/**
 * Class containing an instance of a game including game mechanics
 * @author michael
 *
 */
public class Robots
{
	public GameSettings settings;
	private ArrayList<ArrayList<GameCell>> gameCells;

	private GridPane gamePane;
	private HashMap<Integer, Actor> actors = new HashMap<Integer, Actor>();
	private Random rand;
	
	public Robots()
	{
		settings = new GameSettings();
		rand = new Random(System.nanoTime());
	}
	
	/**
	 * Returns the style sheet to be used to the game
	 * @return
	 */
	public String getStyleSheet()
	{
		return getClass().getResource("gamePane.css").toExternalForm();
	}

	/**
	 * Return a list of all players
	 * @return
	 */
	public ArrayList<Actor> getPlayers()
	{
		return actors.values().stream().
				filter(a -> a instanceof Player).
				collect(Collectors.toCollection(ArrayList::new));
	}
	
	/**
	 * Return a list of all robots
	 * @return
	 */
	public ArrayList<Actor> getRobots()
	{
		return actors.values().stream().
				filter(a -> a instanceof Robot).
				collect(Collectors.toCollection(ArrayList::new));
	}
	
	/**
	 * Return a list of all actors
	 * @return
	 */
	public ArrayList<Actor> getActors()
	{
		try
		{
			return new ArrayList<Actor>(actors.values());
		}
		catch(Exception ex)
		{
			return new ArrayList<Actor>();
		}
	}
	
	/**
	 * Returns a random position on the grid
	 * @param safe	returns a safe position, not adjacent to a robot
	 * @return
	 */
	public Point getRandomPosition(boolean safe)
	{
		boolean occupied = false;
		Point position = new Point(0, 0);
		do
		{
			occupied = false;
			position.x = rand.nextInt(settings.gridSize.x);
			position.y = rand.nextInt(settings.gridSize.y);
		
			for (GameCell cell : adjacentCells(position, true, 1))
			{
				if (cell.getActor() != null)
					occupied = true;
			}
		} while (occupied && safe);
		
		return position;
	}
	
	/**
	 * Changes css class of all cells adjacent to given point
	 * @param cssClass
	 * @param position
	 */
	public void setAdjacentClass(String cssClass, Point position)
	{
		for (GameCell cell : adjacentCells(position, false, 1))
		{
			cell.setClass(cssClass);
		}
	}
	
	/**
	 * Changes the css class of a given point
	 * @param x
	 * @param y
	 * @param cssClass
	 */
	public void setCellClass(int x, int y, String cssClass)
	{
		gameCells.get(x).get(y).setClass(cssClass);
	}
	
	/**
	 * Clamp position to game grid
	 * @param position
	 */
	public void clampPosition(Point position)
	{
		position.x = Math.max(0, Math.min(settings.gridSize.x - 1, position.x));
		position.y = Math.max(0, Math.min(settings.gridSize.y - 1, position.y));
	}
	
	/**
	 * Adds a new robot to the game
	 * @param id
	 * @return
	 */
	public Robot addRobot(int id)
	{
		Robot robot = new Robot(id);
		robot.position = getRandomPosition(false);
		updateCell(robot.position, robot);
		actors.put(id, robot);
		robot.setGame(this);
		
		return robot;
	}
	
	/**
	 * Adds new rubble to the game
	 * @param id
	 * @return
	 */
	public Rubble addRubble(int id)
	{
		Rubble dirt = new Rubble(id);
		dirt.position = getRandomPosition(false);
		updateCell(dirt.position, dirt);
		actors.put(id, dirt);
		
		return dirt;
	}
	
	/**
	 * Adds a new actor to the game
	 * @param actor
	 */
	public void addActor(Actor actor)
	{	
		if (actors.containsKey(actor))
			updateCell(actor.position, null);
		
		actors.put(actor.id, actor);
		updateCell(actor.position, actor);
		
		if (actor instanceof Robot)
		{
			Robot robot = (Robot)actor;
			robot.setGame(this);
		}
	}
	
	/**
	 * Removes an actor from the game
	 * @param actor
	 */
	public void rmActor(Actor actor)
	{
		System.out.println("Removing actor: " + actor.id + " at " + actor.position.x + ", " + actor.position.y);
		updateCell(actor.position, null);
		actors.remove(actor.id);
	}
	
	/**
	 * Remove all non-players from the game
	 */
	public void clearNPCs()
	{
		ArrayList<Actor> tmpPlayers = getPlayers();
		
		for (Actor actor : actors.values())
		{
			if (!(actor instanceof Player))
			{
				updateCell(actor.position, null);
			}
		}		
		actors.clear();
		
		for (Actor actor : tmpPlayers)
		{
			actors.put(actor.id, actor);
		}

		if (gameCells == null || settings == null)
			return;
		
		for (int i = 0; i < settings.gridSize.x; i++)
			for (int j = 0; j < settings.gridSize.y; j++)
				gameCells.get(i).get(j).clearNPC();
	}
	
	/**
	 * Adds a new players to the game
	 * @param player
	 */
	public void addPlayer(Player player)
	{
		actors.put(player.id, player);
	}
	
	/**
	 * Returns the game pane for this game instance
	 * @return
	 */
	public GridPane getGamePane()
	{
		if (gamePane == null)
			gamePane = createGrid();

        return gamePane;
	}
	
	/**
	 * Re-adds all actors to their game cell
	 */
	public void resetGrid()
	{
		if (gameCells == null)
			return;
		
		for (int i = 0; i < settings.gridSize.x; i++)
			for (int j = 0; j < settings.gridSize.y; j++)
				gameCells.get(i).get(j).setActor(null);
		
		for(Actor actor : getActors())
		{
			updateCell(actor.position, actor);
		}
	}
	
	/**
	 * Move actor to position
	 * @param id
	 * @param x
	 * @param y
	 */
	public void moveActor(int id, int x, int y)
	{   
		if (gameCells == null)
			return;
		
		if (!actors.containsKey(id))
			return;
		
		//Check bounds
		if (x < 0 || x > settings.gridSize.x - 1 || y < 0 || y > settings.gridSize.y - 1)
			return;
		
		Actor actor = actors.get(id);
		
		Actor actorInCell = gameCells.get(x).get(y).getActor();
		
		if (checkCollision(actor, actorInCell))
			return;
		
		updateCell(actor.position, null);
		actor.position.x = x;
		actor.position.y = y;
		clampPosition(actor.position);
		updateCell(actor.position, actor);
	}
	
	/**
	 * Perform an attack at position
	 * @param position
	 * @return
	 */
	public ArrayList<Actor> attackAt(Point position)
	{
		int killCount = 0;
		ArrayList<Actor> killed = new ArrayList<Actor>();
		for (GameCell cell : adjacentCells(position, false, settings.attackRange))
		{
			Actor actor = cell.getActor();
			if (actor != null && actor instanceof Robot)
			{
				killed.add(actor);
				kill(actor);
				killCount++;
			}
			
			if (killCount >= settings.attackMaxKills)
				break;
		}
		
		return killed;
	}
	
	/**
	 * Resize the current game grid to fit dimensions
	 * @param w
	 * @param h
	 */
	public void resize(double w, double h)
	{
		if (gameCells == null || gamePane == null)
			return;
		
		//w += 15; h += 15;
		gamePane.resize(Math.min(w, h), Math.min(w, h));
		int cellSize = (int)Math.min(w, h) / (Math.max(settings.gridSize.x, settings.gridSize.y)) - 4;
		
		settings.cellSize = cellSize;
		gamePane.setLayoutX(( (w + 20) % ((cellSize + 4) * settings.gridSize.x)) / 2);
		gamePane.setLayoutY(( (h + 20) % ((cellSize + 4) * settings.gridSize.y)) / 2);
		
		for (int i = 0; i < settings.gridSize.x; i++)
			for (int j = 0; j < settings.gridSize.y; j++)
				gameCells.get(i).get(j).resize(cellSize);
            
	}
	
	/**
	 * Generate a new game grid
	 * @return
	 */
	public GridPane createGrid()
	{
		GridPane grid = new GridPane();
        grid.getStyleClass().add("gridView");
        
        grid.setSnapToPixel(false);


        for (int i = 0; i < settings.gridSize.y; i++)
        {
        	grid.addRow(i);
        }
        
        for (int i = 0; i < settings.gridSize.x; i++)
        {
        	grid.addColumn(i);
        }        

        gameCells = addGameCells(grid);

        return grid;
	}

	
	/**
	 * Maps a mouse position to a grid position
	 * @param mouseX
	 * @param mouseY
	 * @return
	 */
	public Point mouseToGrid(int mouseX, int mouseY)
	{
		mouseX -= gamePane.getLayoutX();
		mouseY -= gamePane.getLayoutY();
		
		Point gridPosition = new Point(-1, -1);
		
		if (mouseX > (settings.cellGap + settings.cellSize) * settings.gridSize.x)
			return gridPosition;

		if (mouseY > (settings.cellGap + settings.cellSize) * settings.gridSize.y)
			return gridPosition;

		gridPosition.x = ((mouseX - settings.cellGap) / (settings.cellSize + settings.cellGap));
		gridPosition.y = ((mouseY - settings.cellGap) / (settings.cellSize + settings.cellGap));
		
		clampPosition(gridPosition);
		return gridPosition;
	}

	
	/**
	 * Check type of collision for two actors and kills one of them
	 * @param actor1
	 * @param actor2
	 * @return 
	 */
	private boolean checkCollision(Actor actor1, Actor actor2)
	{
		boolean twoRobots = (actor1 instanceof Robot && actor2 instanceof Robot);
		boolean robotAndPlayer = (actor1 instanceof Robot && actor2 instanceof Player)
							  || (actor2 instanceof Robot && actor1 instanceof Player);
		
		if (robotAndPlayer)
		{
			if (actor1 instanceof Player)
			{
				kill(actor1);
				updateCell(actor2.position, actor2);
				System.out.println("Killed by: " + actor2.id + ", " + actor2.getClass().toString());
			}
			else
			{
				kill(actor2);
				updateCell(actor1.position, actor1);
				System.out.println("Killed by: " + actor1.id + ", " + actor1.getClass().toString());
			}
		}
		
		if (twoRobots)
		{
			if (settings.robotCollision == RobotCollisionType.Merge)
			{
				kill(actor1);
				updateCell(actor2.position, actor2);
			}
			
			if (settings.robotCollision == RobotCollisionType.Rubble)
			{
				kill(actor1);
				kill(actor2);
				//Replace robots with rubble, hijacking ID and position
				Rubble collisionRubble = new Rubble(actor1.id);
				collisionRubble.position = actor1.position;
				actors.put(collisionRubble.id, collisionRubble);
				updateCell(actor2.position, actor1);
			}
		}
		
		if (actor2 instanceof Rubble)
		{
			kill(actor1);
			updateCell(actor2.position, actor2);
			System.out.println("Killed by: " + actor2.id + ", " + actor2.getClass().toString());
		}
		
		return false;
	}
	
	/**
	 * Kill an actor and remove it from the game
	 * @param actor
	 */
	private void kill(Actor actor)
	{
		System.out.println("Killing actor " + actor.id + ", " + actor.getClass());
		actors.remove(actor.id);
		updateCell(actor.position, null);
		actor.dead = true;
	}
	
	/**
	 * Update game cell at position with new actor
	 * @param position
	 * @param actor
	 */
	private void updateCell(Point position, Actor actor)
	{
		if (gameCells == null)
			return;
		
		if (position == null)
			return;
		
		gameCells.get(position.x).get(position.y).setActor(actor);
	}

	/**
	 * Returns an array of cells adjacent to given position
	 * @param position
	 * @return
	 */
	private ArrayList<GameCell> adjacentCells(Point position, boolean includeCenter, int range)
	{
		ArrayList<GameCell> adjacent = new ArrayList<GameCell>();

		if (gameCells == null)
			return adjacent;
		
		Point fromPosition = new Point(position);
		Point toPosition = new Point(position);
		
		fromPosition.x -= range;
		fromPosition.y -= range;

		toPosition.x += range;
		toPosition.y += range;

		clampPosition(fromPosition);
		clampPosition(toPosition);
		
		for (int i = fromPosition.x; i <= toPosition.x; i++)
		{
			for (int j = fromPosition.y; j <= toPosition.y; j++)
			{
				if (i == position.x && j == position.y)
				{
					if (includeCenter)
						adjacent.add(gameCells.get(i).get(j));
				}
				else
				{
					adjacent.add(gameCells.get(i).get(j));
				}
			}
		}
		return adjacent;
	}
	
	/**
	 * 
	 * @param grid
	 * @return
	 */
	private ArrayList<ArrayList<GameCell>> addGameCells(GridPane grid)
	{
		ArrayList<ArrayList<GameCell>> gameCells = new ArrayList<ArrayList<GameCell>>();
		for (int i = 0; i < settings.gridSize.x; i++)
        {
			ArrayList<GameCell> rowCells = new ArrayList<GameCell>();
            for (int j = 0; j < settings.gridSize.y; j++)
            {	
            	GameCell gameCell = new GameCell(i, j);
            	grid.add(gameCell, i, j);
            	rowCells.add(gameCell);
            }
            gameCells.add(rowCells);
        }
		return gameCells;	
	}
	
}
