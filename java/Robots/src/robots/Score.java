package robots;

import java.io.Serializable;

public class Score implements Comparable<Score>, Serializable
{
	public String name;
	public int score;
	
	public Score(String name, int score)
	{
		this.name = name;
		this.score = score;
	}
	
	@Override
    public int compareTo(Score other)
	{
		if (score > other.score)
        {
            return -1;
        }
        else if (score < other.score)
        {
            return +1;
        }
        else
        {
            return 0;
        }
	}
	
	public int compare(Score score1, Score score2)
	{
        if (score1.score > score2.score)
        {
            return -1;
        }
        else if (score1.score < score2.score)
        {
            return +1;
        }
        else
        {
            return 0;
        }
    }
}
