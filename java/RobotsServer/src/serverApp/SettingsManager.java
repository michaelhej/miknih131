package serverApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import robots.GameSettings;

public class SettingsManager
{
	/**
	 * Write settings to file
	 * @param filename
	 * @param gameSettings
	 * @return	success
	 */
	public static boolean writeSettings(String filename, GameSettings gameSettings)
	{
		try
		{
			FileOutputStream fileStream = new FileOutputStream(filename, false);
			ObjectOutputStream writer = new ObjectOutputStream(fileStream);
			writer.writeObject(gameSettings);
			writer.close();
			fileStream.close();
			return true;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Read settings from file
	 * @param filename
	 * @return	new settings object
	 */
	public static GameSettings readSettings(String filename)
	{
		try
		{
			if (!(new File(filename).exists()))
			{
				return null;
			}
			
			FileInputStream fileStream = new FileInputStream(filename);
			ObjectInputStream reader = new ObjectInputStream(fileStream);
			GameSettings gameSettings = (GameSettings)reader.readObject();
			
			reader.close();
			fileStream.close();

			return gameSettings;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
