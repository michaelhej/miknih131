package serverApp;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import robots.GameSettings;
import robots.Highscore;
import robots.Player;
import robots.Score;


public class Main extends Application implements ServerInterface
{
	ListView<Player> listPlayers;
	TextArea log;
	Stage primaryStage;

	public static void main(String[] args)
	{
		launch(args);
	}
	
	/**
	 * Create the server panel and bind ui events
	 */
	@Override
	public void start(Stage primaryStage)
	{
		try 
		{
			this.primaryStage = primaryStage;
			AnchorPane root = FXMLLoader.load(getClass().getResource("ServerGame.fxml"));

			Scene scene = new Scene(root, 800, 600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			//Link objects
			listPlayers = (ListView)scene.lookup("#listPlayers");
			log = (TextArea)scene.lookup("#log");
			
			//Assign server start
			Button btnStart = (Button)scene.lookup("#btnServerToggle");
			btnStart.setOnAction(event->{ startServer(); });

			//Assign kick
			Button btnKick = (Button)scene.lookup("#btnKick");
			btnKick.setOnAction(event->{ kickPlayer(); });

			//Add update on text change for start players
			TextField fieldNoPlayers = (TextField)scene.lookup("#inputStartPlayers");
			fieldNoPlayers.textProperty().addListener(new ChangeListener<String>() {
			    @Override
			    public void changed(ObservableValue<? extends String> observable,
			            String oldValue, String newValue)
			    {
			    	if (!isNumeric(fieldNoPlayers.getText()))
			    			fieldNoPlayers.setText("5");
			    	
			    	Server.waitForPlayers = Integer.parseInt(fieldNoPlayers.getText());
			    	Server.startCheck();
			    }
			});
			
			//Map stage closing to stop server
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				public void handle(WindowEvent we)
				{
					stopServer();
					Platform.exit();
					System.exit(0);
				}
			});

			mapResize(scene);
			primaryStage.setScene(scene);
			primaryStage.show();
	
			loadSettings();
			updateHighscore(Highscore.readHighscore());
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Print current game state to ui
	 */
	public void gameState(String state)
	{
		Platform.runLater(() ->
		{
			Label labelState = (Label)sceneItem("labelGameState");
			labelState.setText(state);
        });
	}
	
	/**
	 * Server is started
	 * Update ui accordingly
	 */
	public void serverStarted()
	{
		gameState("Waiting for players...");
		Platform.runLater(() ->
		{
			Button btnToggle = (Button)sceneItem("btnServerToggle");
			btnToggle.setText("Stop server");
			btnToggle.setOnAction(null);
			btnToggle.setOnAction(event->{ stopServer(); });
			
			AnchorPane settingsPane = sceneItem("settingsPane");
        	settingsPane.setDisable(true);
        	createSettings();
        });
	}
	
	/**
	 * Server stopped
	 * Update ui accordingly
	 */
	public void serverStopped()
	{
		gameState("Server stopped");
		Platform.runLater(() ->
		{
			Button btnToggle = (Button)sceneItem("btnServerToggle");
			btnToggle.setText("Start server");
			btnToggle.setOnAction(null);
			btnToggle.setOnAction(event->{ startServer(); });
			
			AnchorPane settingsPane = sceneItem("settingsPane");
        	settingsPane.setDisable(false);
        });
	}
	
	/**
	 * Generate a new settings object from the settings layout in the UI
	 */
	public GameSettings createSettings()
	{
		GameSettings settings = new GameSettings();
		settings.gridSize.x = parseSetting("GridSizeX", settings.gridSize.x);
		settings.gridSize.y = parseSetting("GridSizeY", settings.gridSize.y);
		settings.initialRubble = parseSetting("InitRubble", settings.initialRubble);
		settings.rubblePerLevel = parseSetting("RubblePerLevel", settings.rubblePerLevel);
		settings.initialRobots = parseSetting("InitRobots", settings.initialRobots);
		settings.robotsPerLevel = parseSetting("RobotsPerLevel", settings.robotsPerLevel);
		settings.teleports = parseSetting("Teleports", settings.teleports);
		settings.safeTeleports = parseSetting("SafeTeleports", settings.teleports);
		settings.attacks = parseSetting("Attacks", settings.attacks);
		settings.attacksPerLevel = parseSetting("AttacksPerLevel", settings.attacksPerLevel);
		settings.attackRange = parseSetting("AttackRange", settings.attackRange);
		settings.roundsPerLevel = parseSetting("RoundsPerLevel", settings.roundsPerLevel);
		settings.teleportsPerKill = parseSetting("TeleportsPerKill", settings.teleportsPerKill);
		settings.safeTeleportsPerKill = parseSetting("SafeTeleportsPerKill", settings.safeTeleportsPerKill);
		settings.attackMaxKills = parseSetting("AttackMaxKills", settings.attackMaxKills);
		
		//Parse choice box values
		ChoiceBox cBox = (ChoiceBox)primaryStage.getScene().lookup("#selectChaseType");
		if (cBox.getValue().toString().equals("Target"))
			settings.chaseType = GameSettings.RobotChaseType.TargetPlayer;
		
		cBox = (ChoiceBox)primaryStage.getScene().lookup("#selectRobotCollision");
		if (cBox.getValue().toString().equals("Merge"))
			settings.robotCollision = GameSettings.RobotCollisionType.Merge;
		
		saveSettings(settings);
		return settings;
	}

	/**
	 * Game is started
	 * Populate ui with gamepane and apply game stylesheet to scene
	 */
	public void gameStarted()
	{
        Platform.runLater(() ->
        {
    		Pane pane = (Pane)primaryStage.getScene().lookup("#gamePane");
        	pane.getChildren().add(Server.getGame().getGamePane());
        	primaryStage.getScene().getStylesheets().add(Server.getGame().getStyleSheet());
        	redraw();
        });
	}
	
	/**
	 * Game is stopped
	 */
	public void gameStopped()
	{
        Platform.runLater(() ->
        {
			Pane pane = (Pane)primaryStage.getScene().lookup("#gamePane");
			pane.getChildren().clear();
        });
	}
	
	/**
	 * Is a function numberic
	 * @param str
	 * @return
	 */
	private boolean isNumeric(String str)
	{
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}

	/**
	 * Retrieve an item from scene
	 * @param itemId	item fx-id
	 * @return
	 */
	private <T> T sceneItem(String itemId)
	{
		if (primaryStage.getScene().lookup("#" + itemId) == null)
		{
			System.out.println("Could not find item in scene: " + itemId);
			return null;
		}
		
		return (T)primaryStage.getScene().lookup("#" + itemId);
	}
	
	private void startServer()
	{
		if (!Server.start(this))
			return;
	}
	
	private void stopServer()
	{
		Server.stop();
	}
	
	/**
	 * Return the value of a given settings field in the ui
	 * @param settingName
	 * @param defaultValue
	 * @return
	 */
	private int parseSetting(String settingName, int defaultValue)
	{
		TextField obj = (TextField)primaryStage.getScene().lookup("#input" + settingName);
		if (obj == null)
			System.out.print("Saknas: " + settingName);
		
		if (isNumeric(obj.getText()))
			return Integer.parseInt(obj.getText());
		else
			return defaultValue;
	}
	
	/**
	 * Kick a player
	 * Bad boy!
	 */
	private void kickPlayer()
	{
		if (listPlayers.getSelectionModel().getSelectedItem() == null)
			return;
		
		Player selectedPlayer = (Player)listPlayers.getSelectionModel().getSelectedItem();
		Server.kickPlayer(selectedPlayer);
	}
	
	/**
	 * Maps redraw function to scene resize
	 * @param scene
	 */
	private void mapResize(Scene scene)
	{
		scene.widthProperty().addListener(new ChangeListener<Number>()
		{
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth)
		    {
		        redraw();
		    }
		});
		
		scene.heightProperty().addListener(new ChangeListener<Number>()
		{
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight)
		    {
		    	redraw();
		    }
		});
	} 

	/**
	 * Redraw the grid to fit window size
	 */
	public void redraw()
	{
		Platform.runLater(() ->
		{
        	Server.getGame().resetGrid();
        	Pane gp = sceneItem("gamePane");
        	Server.getGame().resize(gp.getWidth(), gp.getHeight());
        });
	}

	/**
	 * Add a player to the player list
	 */
	public void addPlayer(Player player)
	{
		if (player.name.equals(""))
			return;
		
        Platform.runLater(() ->
        {
        	if (listPlayers.getItems().contains(player))
        		listPlayers.getItems().remove(player);
        	else
        		statusMessage(player + " joined!");
        	
        	listPlayers.getItems().add(player);
        });
	}
	
	/**
	 * Remove a player from the player list
	 */
	public void removePlayer(Player player)
	{
        Platform.runLater(() ->
        {
        	if (listPlayers.getItems().contains(player))
        		listPlayers.getItems().remove(player);
        });
	}
	
	/**
	 * Send message to status box
	 */
	public void statusMessage(String msg)
	{
        Platform.runLater(() ->
        {
            log.setText(log.getText() + "\n" + msg);
        });
	}
	
	/**
	 * Save settings to file
	 * @param settings
	 */
	private void saveSettings(GameSettings settings)
	{
		SettingsManager.writeSettings("settings.robots", settings);
	}
	
	/**
	 * Read settings from file
	 */
	private void loadSettings()
	{
		GameSettings settings = SettingsManager.readSettings("settings.robots");
		
		if (settings == null)
			return;
	
		populateField("GridSizeX", Integer.toString(settings.gridSize.x));
		populateField("GridSizeY", Integer.toString(settings.gridSize.y));
		populateField("RoundsPerLevel", Integer.toString(settings.roundsPerLevel));
		populateField("RubblePerLevel", Integer.toString(settings.rubblePerLevel));
		populateField("InitRobots", Integer.toString(settings.initialRobots));
		populateField("RobotsPerLevel", Integer.toString(settings.robotsPerLevel));
		populateField("Teleports", Integer.toString(settings.teleports));
		populateField("SafeTeleports", Integer.toString(settings.safeTeleports));
		populateField("Attacks", Integer.toString(settings.attacks));
		populateField("AttacksPerLevel", Integer.toString(settings.attacksPerLevel));
		populateField("AttackRange", Integer.toString(settings.attackRange));
		populateField("AttackMaxKills", Integer.toString(settings.attackMaxKills));
		populateField("TeleportsPerKill", Integer.toString(settings.teleportsPerKill));
		populateField("SafeTeleportsPerKill", Integer.toString(settings.safeTeleportsPerKill));

		ChoiceBox<String> cBox = (ChoiceBox<String>)primaryStage.getScene().lookup("#selectChaseType");
		if (settings.chaseType == GameSettings.RobotChaseType.TargetPlayer)
			cBox.setValue("Target");
		
		cBox = (ChoiceBox<String>)primaryStage.getScene().lookup("#selectRobotCollision");
		if (settings.robotCollision == GameSettings.RobotCollisionType.Merge)
			cBox.setValue("Merge");
	}
	
	/**
	 * Set the settings field in the ui to specified value
	 * @param settingName
	 * @param value
	 */
	private void populateField(String settingName, String value)
	{
		TextField obj = (TextField)primaryStage.getScene().lookup("#input" + settingName);
		if (obj == null)
		{
			System.out.println("Field not found: " + settingName);
			return;
		}
		obj.setText(value);
	}
	
	public void updateHighscore(Highscore highscore)
	{
		if (highscore == null)
			return;
		
		Platform.runLater(() ->
		{
			Pane hsPane = sceneItem("paneHighscore");
			hsPane.getChildren().clear();
			int offset = 1;
			for (Score score : highscore.getScores())
			{
				Label nameLabel = new Label(score.name);
				nameLabel.layoutXProperty().set(20);
				nameLabel.layoutYProperty().set(offset*20);
				
				Label scoreLabel = new Label(score.score + "");
				scoreLabel.layoutXProperty().set(160);
				scoreLabel.layoutYProperty().set(offset*20);
	
				hsPane.getChildren().add(nameLabel);
				hsPane.getChildren().add(scoreLabel);
				offset++;
			}
        });
	}
}
