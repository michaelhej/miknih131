package serverApp;

import robots.GameSettings;
import robots.Highscore;
import robots.Player;

public interface ServerInterface
{
	void statusMessage(String msg);
	void addPlayer(Player player);
	void removePlayer(Player player);
	void serverStarted();
	void serverStopped();
	void gameStarted();
	void gameStopped();
	void gameState(String state);
	void updateHighscore(Highscore highscore);
	void redraw();
	GameSettings createSettings();
}
