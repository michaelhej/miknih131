package serverApp;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import robots.Actor;
import robots.Highscore;
import robots.Player;
import robots.Point;
import robots.Robot;
import robots.Robots;


public class Server
{
	public static int waitForPlayers = 5;
	
	private static Server server;
	
	private ServerInterface ui;
	private ServerSocket serverSocket;
	private CopyOnWriteArrayList<ClientConnection> connections = new CopyOnWriteArrayList<ClientConnection>();
	private ClientConnection currentTurnConnection;
	private Robots game;
	private Highscore highscore;
	private boolean playing = false;
	private boolean listen = true;

	private int acceptedConnections = 0;
	private int numPlayers = 0;
	private int playerTurn = 0;
	private int level = 0;
	private int round = 0;
	private int waitTimer = 10;
	
	/**
	 * Initialize server object
	 * Load highscore
	 * Start countdown thread
	 */
	private Server()
	{
		game = new Robots();
		highscore = Highscore.readHighscore();
		if (highscore == null)
			highscore = new Highscore();
		
		new Thread(() -> waitCountdown()).start();
	}
	
	/**
	 * Start the server listener
	 * @param ui
	 * @return
	 */
	public static boolean start(ServerInterface ui)
	{
		if (server == null)
		{
			server = new Server();
		}
		else
		{
			if (server.serverSocket != null && !server.serverSocket.isClosed())
			{
				log("Server already started!");
				server.stopServer();
				return false;
			}
		}
		server.ui = ui;
		server.game.settings = ui.createSettings();
		
		new Thread(() -> server.listen()).start();
		return true;
	}
	
	/**
	 * Stop server
	 */
	public static void stop()
	{
		if (server == null)
			return;
		server.stopServer();
	}
	
	/**
	 * Save game settings from ui
	 */
	public static void saveSettings()
	{
		server.game.settings = server.ui.createSettings();
	}
	
	/**
	 * Return players in the game
	 * @return
	 */
	public static ArrayList<Player> getPlayers()
	{
		ArrayList<Player> players = new ArrayList<Player>();
		for (ClientConnection connection : server.connections)
		{
			players.add(connection.player);
		}
		return players;
	}
	
	/**
	 * Return actors in the game
	 * @return
	 */
	public static ArrayList<Actor> getActors()
	{
		return server.game.getActors();
	}
	
	/**
	 * Return game object
	 * @return
	 */
	public static Robots getGame()
	{
		if (server.game == null)
			server.game = new Robots();
		return server.game;
	}

	/**
	 * Log a message to the ui
	 * @param msg
	 */
	public static void log(String msg)
	{
		server.ui.statusMessage(msg);
	}
	
	/**
	 * Add a new player to the game
	 * @param newPlayer
	 * @param connection
	 */
	public static void addPlayer(Player newPlayer, ClientConnection newConnection)
	{
		if (server.playing && !server.connections.contains(newConnection))
		{
			newConnection.send("inprogress");
			return;
		}
		server.ui.addPlayer(newPlayer);
		
		for (ClientConnection connection : server.connections)
		{
			if (connection != newConnection)
			{
				connection.sendActor(newPlayer);
			}
		}
		
		if (server.connections.contains(newConnection))
			return;

		server.numPlayers++;

		server.game.addPlayer(newPlayer);
		server.connections.add(newConnection);
				
		Server.startCheck();
	}
	
	/**
	 * Check if connected players are enough to start a game
	 */
	public static void startCheck()
	{
		if (server == null)
			return;
		
		if (!server.playing && server.connections.size() >= waitForPlayers)
		{
			server.startGame();
		}
	}

	
	public static void kickPlayer(Player player)
	{
		for (ClientConnection connection : server.connections)
		{
			if (connection.player == player)
				connection.send("kick");
		}
	}
	
	/**
	 * Move actor and send new position to all connected players
	 * @param actor		Moving robot or player
	 * @param x
	 * @param y
	 */
	public static void movePlayer(ClientConnection movingConnection, int x, int y)
	{
		if (movingConnection != server.currentTurnConnection)
		{
			log("Ignoring move commands from non-turn connection:");
			log("Player: " + movingConnection.player.name);
			return;
		}
		
		Player movingPlayer = movingConnection.player;
		server.game.moveActor(movingPlayer.id, x, y);
		
		for (ClientConnection connection : server.connections)
		{
			Player player = connection.player;
			if (player.id != movingPlayer.id) //Don't send to source player
				connection.sendActorPosition(movingPlayer.id, x, y);
		}
		
		server.nextTurn();
	}
	
	/**
	 * Perform an action
	 * @param connection	connection performing action
	 * @param strAction		action teleport|safeTeleport|attack
	 */
	public static void performAction(ClientConnection connection, String strAction)
	{
		if (connection != server.currentTurnConnection)
		{
			log("Ignoring action received from non-turn connection:");
			log("Player: " + connection.player.name);
			log("Action: " + strAction);
			return;
		}
		
		if (strAction.equals("teleport") || strAction.equals("safeTeleport"))
		{
			Point newPosition;
			if (strAction.equals("safeTeleport"))
				newPosition = server.game.getRandomPosition(true);
			else
				newPosition = server.game.getRandomPosition(false);
			
			//connection.player.position = newPosition;

			movePlayer(connection, newPosition.x, newPosition.y);
			connection.updatePosition();
		}
		
		//Perform attack command and send all killed actors to all players
		if (strAction.equals("attack"))
		{
			for (Actor actor : server.game.attackAt(connection.player.position))
			{
				//Telling player a kill happened
				log(connection.player.name + " killed a robot!");
				connection.send("kill");
				
				for (ClientConnection tConnection : server.connections)
				{
					tConnection.sendActor(actor);
				}
			}
			//Perform wait command for player (move to same position)
			movePlayer(connection, connection.player.position.x, connection.player.position.y);
		}
	}
	 
	
	/**
	 * Remove a player
	 * @param rmConnection
	 */
	public static void removePlayer(ClientConnection rmConnection)
	{
		server.disconnectPlayer(rmConnection);
	}
	
	/**
	 * Removes and disconnects connected player
	 * @param rmConnection
	 */ 
	private void disconnectPlayer(ClientConnection rmConnection)
	{	
		if (!connections.contains(rmConnection))
			return;
		 
		//Remove connection
		connections.remove(rmConnection);
		game.rmActor(rmConnection.player);
		numPlayers--;
		
		//Add player scores
		if (rmConnection.score > 0)
			highscore.add(rmConnection.player.name, rmConnection.score);
		ui.updateHighscore(highscore);
		
		//Remove player for other clients
		rmConnection.player.dead = true;
		for (ClientConnection connection : connections)
		{
			connection.sendActor(rmConnection.player);
			connection.send(highscore);
		}
		
		//If current connection disconnected goto next turn
		if (currentTurnConnection == rmConnection)
		{
			playerTurn--;
			currentTurnConnection = null;
			nextTurn();
		}

		ui.removePlayer(rmConnection.player);
	}
	
	/**
	 * Start the game
	 */
	private void startGame()
	{
		playing = true;
		initLevel(1);
		sendToAll(highscore);
	}
	
	/**
	 * Resets the game and restarts and waits for new connections
	 */
	private void restart()
	{
		ui.gameState("Waiting for players...");
		ui.gameStopped();
		
		game = new Robots();
		currentTurnConnection = null;
		
		playing = false;
		playerTurn = 0;
		level = 0;
		round = 0;
		waitTimer = 10;
	} 
	
	/**
	 * Initialize a new level
	 * @param level
	 */
	private void initLevel(int level)
	{ 
		if (level == 0)
			return;
		
		if (level == 1)
		{
			server.game.settings = ui.createSettings();
			server.game.createGrid();
			ui.gameStarted();
		}
		
		this.level = level;

		
		//Clear robots and rubble
		game.clearNPCs();
		sendToAll("clear");
		sendToAll("level:" + level);
		
		//Instantiate robots
		for (int i = 0; i < game.settings.initialRobots + game.settings.robotsPerLevel*level; i++)
		{
			game.addRobot(1000+i);
		}
		
		//Instantiate dirt
		for (int i = 0; i < game.settings.initialRobots + game.settings.robotsPerLevel*level; i++)
		{
			game.addRubble(2000+i);
		}
		
		//Position players safely
		for (ClientConnection connection : server.connections)
		{
			connection.player.position = game.getRandomPosition(true);
		}
		
		//Send game state to players
		sendGameState();
		
		ui.redraw();
		
		playerTurn = 0;
		round = 1;
		playerTurn = 0;
		sendToAll("round:" + round);
		
		ui.gameState("Level: " + level + ", Round: " + round);
		nextTurn();
	}
	
	/**
	 * Next turn
	 */
	private void nextTurn()
	{	
		
		if (!playing)
			return;
		
		if (connections.size() <= 0)
			numPlayers = 0;
		
		if (numPlayers <= 0)
		{
			restart();
			return;
		}
		
		waitTimer = 10;
		
		//New round (after last players turn)
		if (playerTurn >= numPlayers)
		{
			if (round >= game.settings.roundsPerLevel)
			{
				initLevel(level + 1);
				return;
			}
			
			nextRound();
		}
		
		boolean playerOnline = false;
		int tmpTurn = 0;
		for (ClientConnection connection : connections)
		{
			if (playerTurn == tmpTurn)
			{
				currentTurnConnection = connection;
				connection.send("turn");
				playerOnline = true;
				playerTurn++;
				break;
			}
			tmpTurn++;
		}
		
		if (!playerOnline)
		{
			playerTurn++;
			nextTurn();
		}
	}
	
	/**
	 * Start the next round
	 */
	private void nextRound()
	{
		if (!playing)
			return;
		
		round++;
		
		sendToAll("round:" + round);
		moveRobots();
		sendGameState();
		checkDead();
		playerTurn = 0;
		

		ui.gameState("Level: " + level + ", Round: " + round);
		
		for (ClientConnection connection : connections)
		{
			connection.score++;
			connection.send("score:" + connection.score);
		}
	}
	
	/**
	 * Check for any dead players and disconnect them
	 */
	private void checkDead()
	{
		for (ClientConnection connection : connections)
		{
			if (connection.player.dead == true)
			{
				log(connection.player.name + " died!");
				connection.send("dead");
				disconnectPlayer(connection);
			}
		}
	}
	
	/**
	 * Sends a command to all clients
	 * @param msg
	 */
	private void sendToAll(Object msg)
	{
		for (ClientConnection connection : server.connections)
		{
			connection.send(msg);
		}
	}
	
	private void sendGameState()
	{
		for (ClientConnection connection : server.connections)
		{
			connection.sendState();
		}
	}
	/**
	 * Perform turn for all robots
	 * @param game
	 */
	private void moveRobots()
	{
		for (Actor actor : game.getRobots())
		{
			Robot robot = (Robot)actor;
			Point robotPosition = robot.nextPosition();
			game.moveActor(robot.id, robotPosition.x, robotPosition.y);
		}
	}
	
	/**
	 * Listen for incoming connections
	 */
	private void listen()
	{
		try
		{
			serverSocket = new ServerSocket(12121);
			log("Server listening on port 12121");
			ui.serverStarted();
			Socket clientSocket = null;
			listen = true;
			while (listen && (clientSocket = acceptConnection()) != null)
			{
				connectClient(clientSocket);
			}
			log("Server stopped!");
			ui.serverStopped();
		}
		catch (Exception e)
		{
			log("Could not start server");
			e.printStackTrace();
		}
	}
	
	/**
	 * Counts down the players time left to make a move before performing wait command
	 */
	private void waitCountdown()
	{
		while(true)
		{
			try
			{
				if (playing && currentTurnConnection != null)
				{
					if (waitTimer < 1)
					{
						currentTurnConnection.send("timeout");
						nextTurn();
					}
					else
					{
						currentTurnConnection.send("timer:" + waitTimer);
					}
					waitTimer--;
				}
				Thread.sleep(1000);
			}
			catch (Exception ex)
			{
				/* Ignore */
			}
		}
	}
	
	/**
	 * Accept an incoming connection and apply to a socket
	 * @return
	 */
	private Socket acceptConnection()
	{
		Socket clientSocket;
		try
		{
			clientSocket = serverSocket.accept();
		}
		catch (Exception ex)
		{
			return null;
		}
		return clientSocket;
	}

	/**
	 * Start a new client thread for an incoming connection
	 * @param client
	 * @throws InterruptedException 
	 */
	private void connectClient(Socket client)
	{
		Runnable r = new ClientConnection(client, ++acceptedConnections % 1000); //Robots id start at 1000
		new Thread(r).start();	
	}
	
	/**
	 * Stop the server listener
	 */
	public void stopServer()
	{
		listen = false;
		
		if (highscore != null)
			highscore.writeHighscore();
		
		if (serverSocket != null && !serverSocket.isClosed())
			sendToAll("disconnect");
		
		try
		{
			log("Server going down...");
			serverSocket.close();
		}
		catch (Exception e)
		{
			/* Ignore */
		}
		finally
		{
			serverSocket = null;
			ui.serverStopped();
		}
	}
}
