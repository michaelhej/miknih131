package serverApp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import robots.Actor;
import robots.Player;

public class ClientConnection implements Runnable
{
	public Socket socket;
	public Player player;
	public LinkedBlockingQueue<Object> sendQueue = new LinkedBlockingQueue<Object>();
	public int score = 0;
	
	private ObjectOutputStream objectWriter;
	private ObjectInputStream objectReader;
	
	/**
	 * Initialize the client connection
	 * @param clientSocket
	 * @param id
	 */
	public ClientConnection(Socket clientSocket, int id)
	{
		socket = clientSocket;
		player = new Player(id, "", "");
		player.socket = clientSocket;
	}
	
	/**
	 * - Initializes client streams
	 * - Starts receiving and sending threads
	 */
	public void run()
	{		
		try
		{
			objectWriter = new ObjectOutputStream(socket.getOutputStream());
			objectReader = new ObjectInputStream(socket.getInputStream());
			
			new Thread(() -> sender()).start();
			new Thread(() -> receiver()).start();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			Server.removePlayer(this);
		}

		Server.addPlayer(player, this);
	}
	
	/**
	 * Sender method sending objects from sendQueue
	 */
	private void sender()
	{
		try
		{
			while (socket.isConnected())
			{
				Object obj = sendQueue.poll(1, TimeUnit.SECONDS);
				if (obj != null)
				{
					//System.out.println("Sending object " + obj);
					objectWriter.writeObject(obj);
					objectWriter.flush();
					objectWriter.reset();
				}
			}
		}
		catch (Exception ex)
		{
			//ex.printStackTrace();
		}
		finally
		{
			Server.removePlayer(this);
		}
		System.out.println("[" + player.id + "] Client sender ended for " + player.name);
	}
	
	/**
	 * Receiver method handling incoming client commands
	 */
	private void receiver()
	{
		try
		{
			while (socket.isConnected())
			{
				String line = (String)objectReader.readObject();
				 
				if (line == null) //Client disconnected
					break;
				parseCommand(line);
			}
		}
		catch (Exception ex)
		{
			Server.log(player.name + " disconnected!");
			//ex.printStackTrace();
		}
		finally
		{
			Server.removePlayer(this);
		}
	}

	/**
	 * Close the client connection
	 */
	public void close()
	{
		try
		{
			socket.close();
			socket = null;
		}
		catch (Exception ex)
		{
			/* Ignore */
		}
	}
	
	/**
	 * Sends a string to the client
	 * @param msg
	 */
	public void send(Object msg)
	{
		try
		{
			sendQueue.put(msg);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Sends the current game state to the client
	 */
	public void sendState()
	{
		try
		{
			sendSettings();
			send("id:" + player.id);
			send("pos:" + player.position.x + ":" + player.position.y);
			sendAllActors();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Send current game settings to client
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void sendSettings() throws IOException, InterruptedException
	{
		sendQueue.put(Server.getGame().settings);
	}
	
	/**
	 * Sends all actors (players,robot,rubble) to the player
	 */
	private void sendAllActors()
	{
		for (Actor actor: Server.getActors())
		{
			try
			{
				if (actor != this.player)
				{
					sendActor(actor);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Sends specific actor to client
	 * @param actor
	 * @param add
	 */
	public void sendActor(Actor actor)
	{
		try
		{	
			sendQueue.put(actor);			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Send new position for another player
	 * @param id
	 * @param x
	 * @param y
	 */
	public void sendActorPosition(int id, int x, int y)
	{
		send("move:" + id + ":" + x + ":" + y);
	}
	
	/**
	 * Sends the clients current position
	 */
	public void updatePosition()
	{
		send("pos:" + player.position.x + ":" + player.position.y);
	}
	
	/**
	 * Parse incoming commands from the client
	 * @param cmdStr
	 */
	private void parseCommand(String cmdStr)
	{
		String[] cmd = cmdStr.split(":");
		
		switch (cmd[0])
		{
		case "name":
			player.name = cmd[1];
			Server.addPlayer(player, this);
			break;
		case "icon":
			player.playerIcon = cmd[1];
			Server.addPlayer(player, this);
			break;
		case "move":
			move(cmd);
			break;
		case "action":
			Server.performAction(this, cmd[1]);
			break;
		}
	}
	
	/**
	 * Parse an incoming move command for player associated with this client connection
	 * @param cmd
	 */
	private void move(String[] cmd)
	{
		int x = Integer.parseInt(cmd[1]);
		int y = Integer.parseInt(cmd[2]);
		
		Server.movePlayer(this, x, y);
	}
}

