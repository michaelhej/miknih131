#version 330

//in
in vec4 vertexColor;
in vec4 position;
in vec4 normal;
in vec2 texCoord;

//out
out vec4 fragmentColor;

//uniform
uniform vec4 lightPos;
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform int brdf;


float pi = 3.14159265;

vec4 CookTorrance();
vec4 Phong();
vec4 BlinnPhong();

//Color constants
vec4 Ca, La, Cd, Ld, Cs, Ls;

//Normalized vectors
vec4 N, V, L, H, R;

//Dots
float NdotH, LdotH, NdotV, NdotL, VdotH, RdotL, RdotV;
vec4 lightDir;
vec4 eyeDir;

float time = 0;
void main()
{
    //Colors
    Ca = vertexColor;	Ca.a = 1;
    Cd = vertexColor;	Cd.a = 1;
    Cs = vec4(3.0, 3.0, 3.0, 1) * vertexColor; Cs.a = 1;
    
    La = vec4(0.2, 0.2, 0.2, 1);
    Ld = vec4(0.3, 0.3, 0.3, 1);
    
    //Cd = texture( diffuseTexture, texCoord );
    //Cs = texture( specularTexture, texCoord );
    
    lightDir = normalize(lightPos);
    eyeDir = normalize(-position);
    
    
    //Normalize vectors
    N = normalize(normal);
    L = normalize(lightPos - position);
    L = normalize(lightDir);
    V = normalize(-position);
    H = normalize(lightDir + eyeDir);
    R = reflect(-lightDir, N);//2*(dot(L,N))*N - L; //reflect(L, N);
    
    //Dot products
    NdotH = max(dot(N, H), 0.0);
    NdotV = max(dot(N, V), 0.0);
    NdotL = max(dot(N, L), 0.0);
    VdotH = max(dot(V, H), 0.0);
    RdotL = max(dot(R, L), 0.0);
    RdotV = max(dot(R, V), 0.0);
    LdotH = max(dot(L, H), 0.0);
    
    vec4 specColor;
    if (brdf == 3 || brdf == 4)
        specColor = CookTorrance();
    else if (brdf == 2)
        specColor = BlinnPhong();
    else
        specColor = Phong();
    
    float falloff = length(lightPos - position);
    
    specColor = specColor;// * min(1, 2/falloff);
    
    fragmentColor = Ca*La + Cd*Ld*NdotL + specColor;
    fragmentColor.a = 1;
}

//Get the specular component using the Phong lighting model
vec4 Phong()
{
    float f = 35.0;
    
    float spec = 0;
    if (dot(N, lightDir) > 0)
        spec = pow(RdotV, f);
    
    return Cs*spec;
}

//Get the specular component using the Blinn-Phong lighting model
vec4 BlinnPhong()
{
    float f = 55.0;
    
    float spec = 0;
    if (NdotL > 0)
        spec = pow(NdotH, f);
    
    return Cs*spec;
}

//Get the specular component using the Cook-torrance lighting model
vec4 CookTorrance()
{
    
    lightDir = normalize(lightPos);
    eyeDir = normalize(-position);
    
    vec4 halfVector = normalize(lightDir + eyeDir);
    
    NdotL = max(dot(N, normalize(lightDir)), 0.0);
    
    NdotH = max(dot(N, halfVector), 0.0);
    VdotH = max(dot(eyeDir, halfVector), 0.0);
    
    //Material constants
    float m = 0.25; //
    float rf0 = 0.8; //reflection at normal incidence (θi = 0)....
    float k = 0.2;
    
    //G - microfacet geometric factor
    float g1 = (NdotH * NdotV) / VdotH;
    float g2 = (NdotH * NdotL) / VdotH;
    float G = min(1.0, min(2 * g1, 2 * g2));
    
    //D - amount of microfacets in given direction
    float tangent = tan(acos(NdotV));
    float d1 = pi * (m*m) * pow(NdotH, 4.0);
    float d2 = (NdotH * NdotH - 1) / ( m * m * NdotH * NdotH);//-pow( tangent / m , 2.0);
    float D = (1 / d1) * exp(d2);
    
    //F - function of light angle and reflective indicies
    float F = rf0 + (1.0 - rf0) * pow(1.0 - VdotH, 5.0);
    
    float specColor = 0;
    
    if (NdotL > 0)
        specColor = (F * D * G) / ( pi * (NdotL * NdotV));
    
    if (brdf == 4)
        return Cs * NdotL * (k + specColor * (1.0 - k));
    //return Cs * NdotL * (k + specColor * (1.0 - k));
    
    return Cs * specColor;
}

