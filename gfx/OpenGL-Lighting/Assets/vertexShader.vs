/** vertexShader.**s
    Example vertexShader */

#version 330

//in
layout(location=0) in vec3 inPosition;
layout(location=1) in vec4 inColor;
layout(location=2) in vec4 inNormal;
layout(location=3) in vec2 texPosition;

//out
out vec4 vertexColor;
out vec4 position;
out vec4 normal;
out vec2 texCoord;
out vec4 lPosition;

//uniform
uniform mat4 projectionMatrix;
uniform mat4 transformMatrix;

void main() {
  	gl_Position  = projectionMatrix * vec4(inPosition, 1);
 	vertexColor = inColor;
 	
 	position = transformMatrix * vec4(inPosition, 1);
 	lPosition = projectionMatrix * vec4(inPosition, 1);
	normal = transformMatrix * inNormal;
	texCoord = texPosition;
}