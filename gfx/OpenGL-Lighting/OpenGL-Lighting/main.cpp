/*
    LAB 3
    Computer Graphics
    OpenGL Buffers
    --
    miknin131
*/

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "shader.h"
#include "loadImage.h"
#include "fighter.hpp" //ac3d

//GLM
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

//Cube vertex data
#include "cube.h"
#include <vector>
#include <map>

GLuint programID;
GLFWwindow* window;
GLfloat revolution;

GLuint MatrixID;
GLuint LightID;
GLuint TransformID;
GLuint DiffuseID;
GLuint SpecularID;
GLuint BrdfID;

//Load locations and send vertex- and texture-data to OpenGL
void InitializeProgram()
{
    programID = CreateProgram( "vertexShader.vs", "fragmentShader.fs" );
    glUseProgram(programID);
    
    //Get uniform IDs
    MatrixID = glGetUniformLocation(programID, "projectionMatrix");
    LightID = glGetUniformLocation(programID, "lightPos");
    TransformID = glGetUniformLocation(programID, "transformMatrix");
    DiffuseID = glGetUniformLocation(programID, "diffuseTexture");
    SpecularID = glGetUniformLocation(programID, "specularTexture");
    BrdfID = glGetUniformLocation(programID, "brdf");
    
    
    //Z-buffer
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    
    //Alpha
    glDepthFunc(GL_LESS);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    
    glClearColor(0.25, 0.25, 0.25, 0);
    
    
    //Light position
    glm::vec4 lightPos = glm::vec4(2, 1, 0, 1);
    glUniform4fv(LightID, 1, &lightPos[0]);
    
    return;
    /** CUBE DATA, IGNORING FOR LAB 3 **/
    
    //Buffers
    GLuint vao, vbo[4], textures[2];
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(4, vbo);
    
    //Textures
    glGenTextures(2, textures);
    
    glActiveTexture(GL_TEXTURE0);
    loadTexturePng("texture1.png", textures[0]);
    glUniform1i(DiffuseID, 0);
    
    glActiveTexture(GL_TEXTURE1);
    loadTexturePng("texture2.png", textures[1]);
    glUniform1i(SpecularID, 1);
    
    //Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    
    //Texture coords
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
    
    //Colors
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
    
    //Normal
    glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normal), normal, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, 0);
    
    //Enable attributes
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
}

int main(int argc, const char *argv[])
{
    //One rotation revolution equals to 2 pi
    revolution = 2 * glm::pi<float>();
    
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }
    
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    
    window = glfwCreateWindow( 1024, 768, "OpenGL", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window!\n" );
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    
    // Initialize GLEW
    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW!\n");
        return -1;
    }
    
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    
    
    GLfloat rotation[2] = {0, 0};
    GLboolean spin = true;
    GLboolean keyTrigger = false;
    
    //Set up data and load locations
    InitializeProgram();
    
    Fighter* fighter = new Fighter("fighter-hires.ac");
    
    while( glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 )
    {
        //Reload shaders
        if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
            InitializeProgram();
        
        //Change lighting model
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            glUniform1i(BrdfID, 1); //Phong
        
        else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            glUniform1i(BrdfID, 2); //Blinn-phon
        
        else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            glUniform1i(BrdfID, 3); //Cook torrance
        
        //Start/Stop spin
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        {
            keyTrigger = true;
        }
        else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && keyTrigger)
        {
            spin = !spin;
            keyTrigger = false;
        }
        
        //Rotate cube
        if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
            rotation[1] -= 0.025;
        if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
            rotation[1] += 0.025;
        if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
            rotation[0] -= 0.025;
        if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS)
            rotation[0] += 0.025;
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(programID);
        
        //Rotation
        if (spin)
        {
            rotation[0] += 0.05 * 0.4;          //x
            rotation[1] += 0.05 * 0.4 * 0.4;    //y
        }
        
        if (rotation[0] > revolution)
            rotation[0] -= revolution;
        if (rotation[1] > revolution)
            rotation[1] -= revolution;
        
        
        glm::mat4 matrixRotate =  glm::rotate(rotation[0], glm::vec3(1,0,0))  //Rotate x
                                * glm::rotate(rotation[1], glm::vec3(0,1,0)); //Rotate y
        
        glm::mat4 matrixTranslate = glm::translate(glm::vec3(0, 0, -4));
        
        glm::mat4 matrixPerspective = glm::perspective(45.f, 4/3.f, 0.1f, 10.0f);
        
        //Calculate matrices
        glm::mat4 matrixView = matrixPerspective * matrixTranslate * matrixRotate;
        glm::mat4 matrixTransform = matrixTranslate * matrixRotate;
        
        //Matrix
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &matrixView[0][0]);
        
        //Transform
        glUniformMatrix4fv(TransformID, 1, GL_FALSE, &matrixTransform[0][0]);
        
        //Draw cube
        fighter->reDraw();
        
        //Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    
    // Cleanup VBO and shader
    glDeleteProgram(programID);
    //glDeleteVertexArrays(1, &vao);
    
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    
    return 0;
}