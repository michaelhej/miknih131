#version 330

//in
layout(location=0) in vec4 inPosition;
layout(location=1) in vec4 inColor;
layout(location=2) in vec4 inNormal;
layout(location=3) in vec2 texPosition;

//out
out vec4 vertexColor;
out vec4 position;
out vec4 normal;
out vec2 texCoord;

//uniform
uniform mat4 projectionMatrix;
uniform mat4 transformMatrix;

void main()
{
  	gl_Position  = projectionMatrix * inPosition;
 	vertexColor = inColor;
 	
 	position = transformMatrix * inPosition;
	normal = transformMatrix * inNormal;
	texCoord = texPosition;
}