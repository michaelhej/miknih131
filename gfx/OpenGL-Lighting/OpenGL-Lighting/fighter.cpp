//
//  fighter.cpp
//  OpenGL-Lighting
//
//  Created by Michael Nilsson on 24/11/15.
//  Copyright © 2015 piq. All rights reserved.
//

#include "fighter.hpp"


glm::vec4 computeNormal(glm::vec3 const & a, glm::vec3 const & b, glm::vec3 const & c)
{
    return glm::vec4( glm::normalize(glm::cross(b - a, c - b)), 0);
}

Fighter::Fighter(const char *filename)
{
    offset = 0;
    
    //Read model from file
    AC3DObjectFactory myFactory;
    fighter = myFactory.loadAC3D(filename);
    
    traverseModel(fighter, false);
    makeBuffer();
}

//Traverses the model tree
void Fighter::traverseModel(AC3DObject* node, bool draw = false)
{
    if (node->nChildren > 0)
    {
        for (int i = 0; i < node->nChildren; i++)
        {
            traverseModel(node->children[i], draw);
        }
    }
    else
    {
        if (draw)
            drawModel(node);
        else
            processModel(node);
    }
}

//Bind buffer and draw fighter
void Fighter::reDraw()
{
    glBindVertexArray(vao);
    offset = 0;
    traverseModel(fighter, true);
}

//Initialize buffers for vertex- and index-data
void Fighter::makeBuffer()
{
    GLulong vertexSize = vVertices.size() * 4 * sizeof(GLfloat);
    GLulong normalSize = vNormals.size() * 4 * sizeof(GLfloat);
    GLulong indexSize = vIndicies.size() * sizeof(GLuint);
    
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
 
    //Set indicies
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize, vIndicies.data(), GL_STATIC_DRAW);
    
    //Bind single buffer
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertexSize + normalSize, NULL, GL_STATIC_DRAW);
    
    //Vertices
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertexSize, vVertices.data());
    
    //Normals
    glBufferSubData(GL_ARRAY_BUFFER, vertexSize, normalSize, vNormals.data());
    
    //Set vertex attribute
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (const GLvoid *)(0));
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (const GLvoid *)(vertexSize));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(2);
    
    //Free up space by clearing the vectors
    vVertices.clear();
    vNormals.clear();
    vIndicies.clear();
}

void Fighter::drawModel(AC3DObject* node)
{
    //Interate all model surfaces drawing either traingles or quads (triangle_fan)
    for (int i = 0; i < node->nSurfaces; i++)
    {
        if (node->surfaces[i].nVertices == 3)
            glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (const GLvoid *)(offset * sizeof(GLuint)));
        else
            glDrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, (const GLvoid *)(offset * sizeof(GLuint)));

        offset += node->surfaces[i].nVertices;
    }
}

//Process model, add vertices, add indicies, create normals
void Fighter::processModel(AC3DObject* node)
{
    //Add all vertices for model to vector
    for (int i = 0; i < node->nVertices * 4; i+=4)
    {
        vVertices.push_back(glm::vec4(node->vertices[i],
                                      node->vertices[i+1],
                                      node->vertices[i+2],
                                      node->vertices[i+3]));
        vNormals.push_back(glm::vec4(0,0,0,0));
    }
    
    //Add model indices
    for (int i = 0; i < node->nSurfaces; i++)
    {
        AC3DSurface surface = node->surfaces[i];
        for (int j = 0; j < surface.nVertices; j++)
        {
            vIndicies.push_back(surface.vert[j].index + offset);
        }
    }
    
    //Create normals
    for (int i = 0; i < node->nSurfaces; i++)
    {
        AC3DSurface surface = node->surfaces[i];
        glm::vec3 v1 = glm::vec3(vVertices[surface.vert[0].index + offset]);
        glm::vec3 v2 = glm::vec3(vVertices[surface.vert[1].index + offset]);
        glm::vec3 v3 = glm::vec3(vVertices[surface.vert[2].index + offset]);
        
        glm::vec4 normal = computeNormal(v1, v2, v3);
        
        //Add computed normal for all vertices on this surface
        for (int j = 0; j < surface.nVertices; j++)
        {
            vNormals[surface.vert[j].index + offset] += normal;
        }
        
    }
    
    //Normalize all normals
    for (int i = 0; i < node->nVertices; i++)
    {
        vNormals[i + offset] = glm::normalize(vNormals[i + offset]);
    }
    
    offset += node->nVertices;
}
