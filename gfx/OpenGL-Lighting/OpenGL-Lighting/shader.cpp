#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include "shader.h"

using namespace std;

//Read a shader from file and compile it
GLuint CreateShader(const char* filename, GLenum shaderType)
{
    GLuint shaderID = glCreateShader(shaderType);
    
    std::string shaderCode;
    std::ifstream fileStream(filename, std::ios::in);
    
    //Read shader from tile
    if(fileStream.is_open())
    {
        std::string Line = "";
        while(getline(fileStream, Line))
            shaderCode += "\n" + Line;
        fileStream.close();
    }
    else
    {
        printf("Error opening %s!\n", filename);
        getchar();
        return -1;
    }
    
    //Compile
    char const * sourcePointer = shaderCode.c_str();
    glShaderSource(shaderID, 1, &sourcePointer , NULL);
    glCompileShader(shaderID);
    
    GLint result = GL_FALSE;
    int infoLogLength;
    
    //Check status
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if ( infoLogLength > 0 )
    {
        std::vector<char> shaderErrorMessage(infoLogLength+1);
        glGetShaderInfoLog(shaderID, infoLogLength, NULL, &shaderErrorMessage[0]);
        printf("%s\n", &shaderErrorMessage[0]);
    }
    
    return shaderID;
}

//Create an OpenGL program
GLuint CreateProgram(const char* vertexPath, const char* fragmentPath)
{
    //Create the shaders
    GLuint VertexShaderID = CreateShader(vertexPath, GL_VERTEX_SHADER);
    GLuint FragmentShaderID = CreateShader(fragmentPath, GL_FRAGMENT_SHADER);
    
    GLuint ProgramID = glCreateProgram();
    
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);
    
    GLint result = GL_FALSE;
    int infoLogLength;
    
    //Check status
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if ( infoLogLength > 0 )
    {
        std::vector<char> ProgramErrorMessage(infoLogLength+1);
        glGetProgramInfoLog(ProgramID, infoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
    
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
    
    return ProgramID;
}

