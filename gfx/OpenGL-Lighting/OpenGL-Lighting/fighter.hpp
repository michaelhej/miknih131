//
//  fighter.hpp
//  OpenGL-Lighting
//
//  Created by Michael Nilsson on 24/11/15.
//  Copyright © 2015 piq. All rights reserved.
//

#ifndef fighter_hpp
#define fighter_hpp

#include <stdio.h>
#include "glUtil.h" //ac3d
#include <vector>
#include <map>

class Fighter
{

public:
    Fighter(const char *filename);
    void reDraw();
    
private:
    AC3DObject* fighter;
    GLuint ebo, vao, vbo, textures[2];
    GLuint offset;

    std::vector<glm::vec4> vVertices;
    std::vector<GLuint> vIndicies;
    std::vector<glm::vec4> vNormals;
    std::vector<glm::vec4> vColors;
    std::map<GLuint, int> vOffsetVertex;
    
    void traverseModel(AC3DObject* node, bool draw);
    void processModel(AC3DObject* node);
    void drawModel(AC3DObject* node);
    void makeBuffer();
};


#endif /* fighter_hpp */
