//
//  shader.h
//  OpenGL-Lighting
//
//  Created by Michael Nilsson on 08/11/15.
//  Copyright (c) 2015 piq. All rights reserved.
//

#ifndef __OpenGL_Lighting__shader__
#define __OpenGL_Lighting__shader__

GLuint CreateShader(const char* filename, GLenum shaderType);
GLuint CreateProgram(const char* vertexPath, const char* fragmentPath);

#endif /* defined(__OpenGL_Lighting__shader__) */
