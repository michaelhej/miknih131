#version 330

//in
in vec4 vertexColor;
in vec4 position;
in vec4 normal;
in vec2 texCoord;

//out
out vec4 fragmentColor;

//uniform
uniform vec4 lightPos;
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform int brdf;

float pi = 3.14159265;

vec4 Phong();
vec4 BlinnPhong();
vec4 CookTorrance();

//Color constants
vec4 Ca, La, Cd, Ld, Cs, Ls;

//Normalized vectors
vec4 N, V, L, H, R;

//Dots
float NdotH, NdotV, NdotL, VdotH, RdotV;
void main()
{

    fragmentColor = abs(position);
 
    vec4 mega = position / normal;
    //Colors
    Ca = vec4(1,1,0,1) + abs(position);
    Cd = vec4(1,1,0,1) * abs(position);
    Cs = vec4(3.0, 3.0, 3.0, 1);
    
    La = vec4(0.2, 0.2, 0.2, 1);
    Ld = vec4(0.3, 0.3, 0.3, 1);
    
    //Apply texture to vertex
    //Cd = texture(diffuseTexture, texCoord);
    //Cs = texture(specularTexture, texCoord);
    
    //Normalize vectors
    N = normalize(normal);
    L = normalize(lightPos - position);
    V = normalize(-position);
    H = normalize(L + V);
    R = reflect(-L, N); // 2 * (max(dot(V, N), 0)) * N - V

    //Dot products
    NdotH = max(dot(N, H), 0.0);
    NdotV = max(dot(N, V), 0.0);
    NdotL = max(dot(N, L), 0.0);
    VdotH = max(dot(V, H), 0.0);
    RdotV = max(dot(R, V), 0.0);
    
    vec4 specColor;
    if (brdf == 3)
        specColor = CookTorrance();
    else if (brdf == 2)
        specColor = BlinnPhong();
    else
        specColor = Phong();
    
    fragmentColor = Ca*La + Cd*Ld*NdotL + specColor;
    fragmentColor.a = 1;
}

//Get the specular component using the Phong lighting model
vec4 Phong()
{
    float f = 20.0;
    
    float spec = 0;
    if (dot(N, L) > 0)
        spec = pow(RdotV, f);
    
    return Cs*spec;
}

//Get the specular component using the Blinn-Phong lighting model
vec4 BlinnPhong()
{
    float f = 40.0;
    
    float spec = 0;
    if (NdotL > 0)
        spec = pow(NdotH, f);
    
    return Cs*spec;
}

//Get the specular component using the Cook-torrance lighting model
vec4 CookTorrance()
{   
    //Material constants
    float m = 0.55;  //roughness
    float rf0 = 0.7; //reflection at normal incidence (θi = 0)...
    
    //G - geometric attenuation, microfacet shadowing factor
    float g1 = (NdotH * NdotV) / VdotH;
    float g2 = (NdotH * NdotL) / VdotH;
    float G = min(1.0, min(2 * g1, 2 * g2));
    
    //D - amount of microfacets in given direction
    float tangent = tan(acos(NdotV));
    float d1 = pi * (m*m) * pow(NdotH, 4.0);
    float d2 = (NdotH * NdotH - 1) / ( m * m * NdotH * NdotH);//-pow( tangent / m , 2.0);
    float D = (1 / d1) * exp(d2);
    
    //F - function of light angle and reflective indicies
    float F = rf0 + (1.0 - rf0) * pow(1.0 - VdotH, 5.0);
    
    float specColor = 0;
    
    if (NdotL > 0)
        specColor = (F * D * G) / ( pi * NdotL * NdotV);

    specColor = min(max(0, specColor), 20);

    return Cs * specColor;
}

