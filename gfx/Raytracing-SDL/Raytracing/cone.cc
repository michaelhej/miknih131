/** \file sphere.cc
\brief Implements all methods for the Cone class.
*/
/* Made by Mathias Broxvall

Copyright (C) 2007  Mathias Broxvall

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/


//
//
// Exercise 4.2 CONE
//
//


#include "../lab-4/general.h"
#include "cone.h"

Cone::Cone(double radius) {
	this->radius = radius;
}
Cone::~Cone() {}

double Cone::lineTest(double O[3], double dir[3], double maxDistance)
{
	double Ox = O[0];
	double Oy = O[1];
	double Oz = O[2];

	double Dx = dir[0];
	double Dy = dir[1];
	double Dz = dir[2];

	double a = (Dx*Dx + Dy*Dy - Dz*Dz);
	double b = (2*Ox*Dx + 2*Oy*Dy - 2*Oz*Dz);
	double c = (Ox*Ox + Oy*Oy - Oz*Oz);

	double s = b * b - 4*a*c;

	if(s < 0)
		return MAX_DISTANCE;

	s = sqrt(s);

	double sol1 = (-b - s)/(2*a);

	if(sol1 > 0 && (Oz + sol1*Dz > 0))
		return sol1;

	double sol2 = (-b + s)/(2*a);

	if (sol2 > 0 && (Oz + sol2*Dz > 0)) 
		return sol2;

	return MAX_DISTANCE;
}

void Cone::getNormal(double point[3], double normal[3])
{
	double N[3];

	N[0] = 2 * point[0];
	N[1] = 2 * point[1];
	N[2] = -2 * point[2];
	
	normalize(N);
	assign(N, normal);
}

bool Cone::isInside(double point[3])
{
	if (point[2] < 0)
		return false;

	return pow(point[0], 2) + pow(point[1], 2) - pow(point[2], 2) <= 0;
}


