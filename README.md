# School-related projects #

## Artificial Intelligence ##

**- Lab 1:** Search. Use different search algorithms to find a route through a map.

**- Lab 2:** Constraint Satisfaction Problem

**- Lab 3:** Decision Tree Learning

**- Lab 4:** K-means clustering (Java)

## Computer graphics ##

Computer graphics in OpenGL

** - Lab 1:** Setting up SDL, vertex drawing, color interpolation.

** - Lab 2:** Adding textures and comparing different lighting algorithms.

** - Lab 3:** Extended raytracing with transparency and light refraction

(Decided to skip SDL here since we're using deprecated version of both SDL and OpenGL)

## Compilers and Interpreters ##

### Hejsan! ###
*Lite programmering på svenska*

Small programming language using swedish syntax.

Options for interpreting or compiling to c++.

Including 2 example programs (snake and tic-tac-toe).

Written using bison/flex/c++.


Example files located in ./hejfiler

** Usage **

Compile to c++:

./hejsan -i <infile.hej> -o <outfile.c>


Interpret using syntax tree:

./hejsan -i <infile.hej>